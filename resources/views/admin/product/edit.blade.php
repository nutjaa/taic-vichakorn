@extends('layouts.admin')
@section('content') 
<div class="breadcrumbs">
    <ol class="breadcrumb">
        <li><a href="/"><i class="fa fa-home"></i>Home</a></li>
        <li><a href="/admin">Admin</a></li>
        <li><a href="/products">Products</a></li>
        <li class="active">Edit product - {{ $product->name }}</li>
    </ol>
</div>

<div class="innerLR">
    <div class="row manage-header">
        <div class="col-lg-7">
        	<h2>Edit product - {{ $product->name }}</h2>
        </div>
 	</div>
    
    <div class="widget">
        {!! Notification::showAll() !!}
        {!! Form::model($product, array('url' => 'products/'. $product->id , 'method' => 'PUT'  , 'class'=>'form-horizontal' , 'role' => 'form'  , 'files'=>true )) !!}
        	 @include('admin/product/form')  
        {!! Form::close() !!}
    </div>
</div>

@stop

@section('footerjs') 
	{!! HTML::script('js/ckeditor/ckeditor.js'); !!}  
    {!! HTML::script('js/page/admin/product.js'); !!}  
@stop