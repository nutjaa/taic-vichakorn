@extends('layouts.master')

@section('content')
<div class="row">
	<ol class="breadcrumb">
	  <li><a href="/">Home</a></li> 
	  <li class="active">ข้อมูลส่วนตัว</li>
	</ol>
</div>
<div class="row">
    <div class="col-md-2">
        <aside class="widget">
            <div class="widget-inner">
                <h3 class="widget-title" style="background-position: 0px -11px;">
                    <span>ข้อมูลส่วนตัว</span>
                </h3> 
                <img class="img-responsive img-rounded" src="/images/{{ Auth::user()->avatarpath() }}" />
                <ul class="left-menu list-unstyled">
                    <li class="active"><a href="/home">ข้อมูลส่วนตัว</a></li>
                    <li><a href="/orders">ประวัติการสั่งซื้อ</a></li>
                    <li><a href="/historical-point">ประวัติแต้มสะสม</a></li>
                </ul>
            </div>
        </aside>
    </div>
    <div class="col-md-10">
        <h2>ข้อมูลส่วนตัว <a href="{{ url('profile/edit') }}"><span class="glyphicon glyphicon-edit"></span> <small>(คลิกเพื่อแก้ไขข้อมูลส่วนตัว)</small></a></h2>
        {!! Notification::showAll() !!} 
        <table class="table table-striped">
            <tr>
                <td class="{{$table1}}">Username</td>
                <td class="{{$table2}}">{{ Auth::user()->username }}</td>
            </tr>
            <tr>
                <td class="{{$table1}}">Email</td>
                <td class="{{$table2}}">{{ Auth::user()->email }}</td>
            </tr>
            <tr>
                <td class="{{$table1}}">แต้มสะสม</td>
                <td class="{{$table2}}">{{ Auth::user()->point }}</td>
            </tr>
            <tr>
                <td class="{{$table1}}">ชื่อนามสกุล</td>
                <td class="{{$table2}}">{{ Auth::user()->fullname }}</td>
            </tr>
            <tr>
                <td class="{{$table1}}">เบอร์โทร</td>
                <td class="{{$table2}}">{{ Auth::user()->phone }}</td>
            </tr>
            <tr>
                <td class="{{$table1}}">เพศ</td>
                <td class="{{$table2}}">{{ Auth::user()->gender }}</td>
            </tr>
            <tr>
                <td class="{{$table1}}">วันเกิด</td>
                <td class="{{$table2}}">{{ Auth::user()->birthDateFormatted }}</td>
            </tr>
            <tr>
                <td class="{{$table1}}">ที่อยู่</td>
                <td class="{{$table2}}">{!! Auth::user()->addressHtml !!}</td>
            </tr>
            <tr>
                <td class="{{$table1}}">Facebook</td>
                <td class="{{$table2}}">{!! Auth::user()->facebookname !!}</td>
            </tr>
            <tr>
                <td class="{{$table1}}">Line ID</td>
                <td class="{{$table2}}">{!! Auth::user()->lineid !!}</td>
            </tr>
            <tr>
                <td class="{{$table1}}">รับข่าวสารทางอีเมล์</td>
                <td class="{{$table2}}">
                    @if(Auth::user()->is_subscribe)
                        ใช่
                    @else
                        ไม่
                    @endif
                </td>
            </tr>
            
        </table>
    </div>
</div>
@stop