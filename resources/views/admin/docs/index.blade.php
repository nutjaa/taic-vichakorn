@extends('layouts.admin')
@section('content') 
<div class="breadcrumbs">
    <ol class="breadcrumb">
        <li><a href="/"><i class="fa fa-home"></i>Home</a></li>
        <li><a href="/admin">Admin</a></li>
        <li class="active">Documents</li>
    </ol>
</div>
<div class="innerLR">
    <div class="row manage-header">
        <div class="col-lg-10">
        	<h2>Documents</h2>
        </div> 
        {!! Form::open(array( 'method' => 'get')) !!} 
        <div class="col-lg-2"> 
        	<input type="text" name="q" placeholder="search" class="span3 pull-right form-control" value="{{ $q }}" /> 
        </div>
        {!! Form::close() !!}
    </div>
    <div class="widget">
        {!! Notification::showAll() !!}
        <div class="table-responsive">
            <table class="table">
                <thead>
    			<tr>
    				<th width="45">ID</th>
    				<th>Title</th> 
    				<th>Order</th> 
    				<th>Updated</th> 
    				<th>Created By</th>
    				<th>Actions</th>
    			</tr>
    		</thead>
    		<tbody>
    		@foreach($pages as $page)
    			<tr>
    				<td>{{ $page->id }}</td>
    				<td>{{ $page->title }}
                    @if($page->status == 0 )
                        <span class="label label-danger">&nbsp;</span>
                    @else
                        <span class="label label-success">&nbsp;</span>
                    @endif
                    </td> 
					<td>
						{{ $page->order }}
					</td> 
    				<td>{{ $page->updated_at }}</td>
    				<td>{{ $page->user->shortname() }}</td>
    				<td>
    					<a class="btn btn-small btn-info" href="{{ URL::to('admindocs/' . $page->id . '/' . $page->sefu) }}">View</a> 
    				</td>
    			</tr>
    		@endforeach
    		</tbody>
    	</table> 
        </div>
    </div>
</div>  
@stop