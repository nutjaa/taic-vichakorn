<?php namespace App;

use LaravelBook\Ardent\Ardent;
use App\Termdata ; 
use Session;

class Product extends Ardent  {
	 
	protected $table = 'products';
  protected $fillable = array('name', 'sefu', 'sku' , 'description' );
    
  public static $rules = array(
  	'name' => 'required',
  	'sefu' => 'required|unique:products',  
  	'sku' => 'required|unique:products' , 
	);
	/*--- comments ---*/
	public static $relationsData = array(  
    'category' => array(self::BELONGS_TO, 'App\Termdata', 'foreignKey' => 'category_id') , 
    'images' => array(self::HAS_MANY, 'App\ProductImage' , 'product_id') ,     
    'defaultimage' => array(self::BELONGS_TO, 'App\ProductImage', 'foreignKey' => 'product_image_id') ,  
  ) ; 
  
  public function comments()
  {
    return $this->morphMany('\App\Comment', 'commentable')->orderBy('updated_at', 'desc');
  }
    
  public function scopeActive($query)
  {
    return $query->where('status', '=', 1);
  }
  
  public function scopeSefu($query , $q)
  {
  	return $query->where('sefu', '=', $q);
  }
  
  public function scopeSku($query , $q)
  {
  	return $query->where('sku', '=', $q);
  }
  
  public function scopeSearch($query,$q){
  	if(trim($q) == '')
  		return $query ;   
 		return $query->where(function($query) use ($q){ 
 			$query->where('name', 'LIKE' , '%'.$q.'%')
 				->orWhere('description', 'LIKE' , '%'.$q.'%') 
	   		->orWhere('sku', 'LIKE' , '%'.$q.'%') ; 
 		}) ; 
  } 
  
  public function scopeHasCategory($query , $id){
  	if($id == '' || $id == 0)
  		return $query ; 
 		$term = Termdata::find($id);
 		if(is_null($term))
 			return $query ; 
 		return $query->where('category_id','=',$id) ; 
  }
  
  public function scopeCategory($query,$catename){
  	$term = Termdata::name($catename)->first() ; 
  	return $query->where('category_id','=',$term->id) ; 
  }
  
  public function imageDefault($type){ 
      if( is_null($this->defaultimage))
          return '' ; 
  	return $this->defaultimage->imagePath($type) ;
  }
 
    
    /*--- Attribute ----*/
 	public function getNameFirstAttribute()
  { 
   	$names = explode('<br/>',$this->name) ;  
   	return $names[0] ;
  }
  
  public function getRouteAttribute(){
  	return '/product/' . $this->sefu ; 
  }
  
  public function getSessionQtyAttribute(){
  	$products = Session::get('products') ; 
  	if(!isset($products[$this->id]['selected_quantity']))
  		return 0 ; 
  	return $products[$this->id]['selected_quantity'] ; 
  }
  
  public function getSessionTotalPriceAttribute(){
  	return $this->sale_price * $this->sessionQty ; 
  }
    
    /*--- To Array --- */ 
  public function toArray(){
    $array = parent::toArray();
    $array['nameFirst'] = $this->NameFirst ; 
    $array['selected_quantity'] = 0 ; 
    $array['selected_product'] = false ; 
    $array['out_of_stock'] = intval($array['out_of_stock']) ; 
    $array['image_default_thumb'] = $this->imageDefault('thumb') ; 
    $products = Session::get('products') ; 
    if( isset($products[$this->id]['selected_quantity']))
    	$array['selected_quantity'] = intval($products[$this->id]['selected_quantity']) ; 
   	if( isset($products[$this->id]['selected_product']))
    	$array['selected_product'] = (bool)$products[$this->id]['selected_product'] ; 
    return $array ; 
  }
 }