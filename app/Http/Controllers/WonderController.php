<?php namespace App\Http\Controllers;

use App\Page;
use App\Pagecategory;
use App\Vocabulary ; 
use App\Termdata ; 

use SEOMeta;
use OpenGraph;

class WonderController extends Controller { 
 	public $vocab  ; 
 	public $termdata;
 	
 	public function __construct()
	{
		$this->vocab = Vocabulary::Page()->first() ; 
		$this->termdata = Termdata::where('name' , '=' , 'wonders-of-use')->where('vid' , '=' , $this->vocab->id )->first() ; 
	 
	}

	/**
	 * Show the application dashboard to the user.
	 *
	 * @return Response
	 */
	public function index()
	{ 
 
		$page = Page::join('page_categories',function($join){
			$join->on('page_categories.page_id','=','pages.id') ; 
		})->where('page_categories.term_id','=',$this->termdata->id)->active()->orderBy('pages.order', 'ASC')->first()  ; 
 
		return redirect('wonders-of-use/'.$page->id.'/'.$page->sefu) ;
	}
	
	public function view($id , $sefu){
		$page = Page::active()->find($id)  ; 
		if(! is_object($page)){
			return Redirect('wonders-of-use') ; 
		}
    
    SEOMeta::setTitle('มหัศจรรย์แห่งการใช้ผลิตภัณฑ์ Luminance Cocktail ครบเซ็ต');
    SEOMeta::setDescription($page->title);
    
    OpenGraph::setDescription($page->title);
    OpenGraph::setTitle('มหัศจรรย์แห่งการใช้ผลิตภัณฑ์ Luminance Cocktail ครบเซ็ต');
    OpenGraph::setUrl(url('/wonders-of-use/'.$page->id.'/'.$sefu)); 
    
    
		$pages = Page::join('page_categories',function($join){
			$join->on('page_categories.page_id','=','pages.id') ; 
		})->where('page_categories.term_id','=',$this->termdata->id)->active()->orderBy('pages.order', 'ASC')->get()  ;  
		return view('wonder')->with('page',$page)->with('pages',$pages) ; 
	}

}
