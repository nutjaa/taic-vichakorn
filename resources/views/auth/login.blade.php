@extends('layouts.master')

@section('content') 
	<div class="row">
		<div class="col-md-8 col-md-offset-2">
			<br />
			<div class="panel panel-default">
				<div class="panel-heading">เข้าสู่ระบบ</div>
				<div class="panel-body">
					{!! Notification::showAll() !!}
					<form class="form-horizontal" role="form" method="POST" action="{{ url('/auth/login') }}">
						<input type="hidden" name="_token" value="{{ csrf_token() }}">

						<div class="form-group">
							<label class="col-md-4 control-label">อีเมล์เพื่อเข้าสู่ระบบ</label>
							<div class="col-md-6">
								<input type="email" class="form-control" name="email" value="{{ old('email') }}">
							</div>
						</div>

						<div class="form-group">
							<label class="col-md-4 control-label">รหัสผ่าน</label>
							<div class="col-md-6">
								<input type="password" class="form-control" name="password">
							</div>
						</div>

						<div class="form-group">
							<div class="col-md-6 col-md-offset-4">
								<div class="checkbox">
									<label>
										<input type="checkbox" name="remember">จดจำ
									</label>
								</div>
							</div>
						</div>

						<div class="form-group">
							<div class="col-md-6 col-md-offset-4">
								<button type="submit" class="btn btn-primary">เข้าสู่ระบบ</button>
								<br /> 
								ลืมรหัสผ่าน?<a class="btn btn-link" href="{{ url('/password/email') }}">คลิ๊กที่นี่</a> <br />
								ยังไม่ได้เป็นสมาชิก<a class="btn btn-link" href="{{ url('/auth/register') }}">สมัครสมาชิกคลิ๊กที่นี่</a> 
							</div>
						</div>
						<!--
						<hr />
						
						<div class="row">
							<div class="col-md-6 col-md-offset-3">
								<a href="#"><img class="img-responsive" src="/images/facebook-sign-in.png" /></a>
							</div>
						</div>
						-->
					</form>
				</div>
			</div>
		</div>
	</div> 
@endsection
