<?php namespace App;

use LaravelBook\Ardent\Ardent;
use Carbon ; 
use Auth;

class HistoricalPoint extends Ardent{
  protected $table = 'historical_points'; 
  
  public static $relationsData = array(  
    'order' => array(self::BELONGS_TO, 'App\Order', 'foreignKey' => 'order_id') ,   
    'user' => array(self::BELONGS_TO,'App\User', 'foreignKey' => 'user_id')
  ) ; 
 
  
  /*--- Atribute ---*/
  
  public function getCreatedAtFormatAttribute(){ 
  	$date = Carbon::createFromFormat('Y-m-d H:i:s', $this->created_at );
  	$date->setTimezone('Asia/Bangkok');
  	return $date->format('d/m/Y H:i');
  }
 
}