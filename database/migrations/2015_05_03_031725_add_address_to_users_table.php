<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddAddressToUsersTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('users', function(Blueprint $table)
		{
			$table->string('lastname');
			$table->string('username');
			$table->text('address'); 
			$table->date('birthdate');
			$table->string('lineid');
			$table->string('facebookname');
			$table->tinyInteger('is_subscribe');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('users', function(Blueprint $table)
		{
			$table->dropColumn('lastname');
			$table->dropColumn('username');
			$table->dropColumn('address');
			$table->dropColumn('birthdate');
			$table->dropColumn('lineid');
			$table->dropColumn('facebookname');
			$table->dropColumn('is_subscribe');
		});
	}

}
