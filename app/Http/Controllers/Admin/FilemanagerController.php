<?php namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use App\Http\Controllers\Controller; 

class FilemanagerController extends Controller {
	protected $package = 'elfinder';
	
	public function __construct()
	{
		$this->middleware('auth');
	}
	
	public function showIndex()
    {
        $dir = 'packages/barryvdh/' . $this->package;  
        return view('admin.filemanager.index')->with(compact('dir'));
    }
    
    public function showConnector()
    {
        $dir = Config::get($this->package . '::dir');
        $roots = Config::get($this->package . '::roots');

        if (!$roots)
        {
            $roots = array(
                array(
                    'driver' => 'LocalFileSystem', // driver for accessing file system (REQUIRED)
                    'path' => public_path() . DIRECTORY_SEPARATOR . $dir, // path to files (REQUIRED)
                    'URL' => asset($dir), // URL to files (REQUIRED)
                    'accessControl' => Config::get($this->package . '::access') // filter callback (OPTIONAL)
                )
            );
        }

        // Documentation for connector options:
        // https://github.com/Studio-42/elFinder/wiki/Connector-configuration-options
        $opts = array(
            'roots' => $roots
        );

        // run elFinder
        $connector = new \elFinderConnector(new \elFinder($opts));
        $connector->run();
    }
}