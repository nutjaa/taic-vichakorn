<?php namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Vocabulary;

use Krucas\Notification\Facades\Notification;

class VocabularyController extends Controller {
	/**
	 * Create a new controller instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		$this->middleware('auth');
	}
	
	public function index(Request $requests)
	{ 
		$q = $requests->input('q','') ;  
		$vocabularies = Vocabulary::search($q)->orderBy('name','ASC')->paginate(15)  ;   
		return view('admin.vocabulary.index')->with('vocabularies' , $vocabularies)->with('q',$q) ; 
	}
    
    public function create(){
		return view('admin.vocabulary.create');
	}
	
    public function store(Request $requests){
		$vocabulary = new Vocabulary() ;
		$vocabulary->name = $requests->input('name');
		$vocabulary->description = $requests->input('description'); 
		 
		if ($vocabulary->save()) {
			Notification::success('Successfull create vocabulary <a href="'.url('vocabulary/' . $vocabulary->id . '/edit').'">'.$vocabulary->name.'</a>');
			return redirect('vocabulary') ; 
		}else{ 
			Notification::error( $vocabulary->errors()->All());
			return redirect('vocabulary/create')->withInput() ; 
		}
	}
	
	public function edit($id)
	{ 
		$vocabulary = Vocabulary::find($id);
 
		return view('admin.vocabulary.edit')
			->with('vocabulary', $vocabulary);
	}
	
	public function update(Request $requests, $id)
	{
		$vocabulary = Vocabulary::find($id);
		$vocabulary->name = $requests->input('name');
		$vocabulary->description = $requests->input('description'); 
		 
		if ($vocabulary->updateUniques()) {
			Notification::success('Successfull update vocabulary <a href="'.url('vocabulary/' . $vocabulary->id . '/edit').'">'.$vocabulary->name.'</a>');
			return redirect('vocabulary') ; 
		}else{ 
			Notification::error( $vocabulary->errors()->All());
			return \redirect('vocabulary/' . $vocabulary->id . '/edit')->withInput() ; 
		} 
	}
}