<div class="form-group">
	{!! Form::label('name', 'Name*' , array('class'=>'col-sm-2 control-label')) !!}
	<div class="col-sm-8">
		{!! Form::text('name', Input::old('name'), array('class' => 'form-control')) !!}
	</div>
</div>	
<div class="form-group">
	{!! Form::label('sefu', 'Sefu*' , array('class'=>'col-sm-2 control-label')) !!}
	<div class="col-sm-8">
		{!! Form::text('sefu', Input::old('sefu'), array('class' => 'form-control')) !!}
	</div>
</div> 
<div class="form-group">
	{!! Form::label('sku', 'SKU*' , array('class'=>'col-sm-2 control-label')) !!}
	<div class="col-sm-8">
		{!! Form::text('sku', Input::old('sku'), array('class' => 'form-control')) !!}
	</div>
</div> 
<div class="form-group">
	{!! Form::label('price', 'Price' , array('class'=>'col-sm-2 control-label')) !!}
	<div class="col-sm-4">
		{!! Form::text('price', Input::old('price'), array('class' => 'form-control' , 'placeholder' => 'Price')) !!}
	</div>
	<div class="col-sm-4">
		{!! Form::text('sale_price', Input::old('sale_price'), array('class' => 'form-control' , 'placeholder' => 'Sale Price')) !!}
	</div>
</div> 
<div class="form-group">
	{!! Form::label('quantity', 'Quantity' , array('class'=>'col-sm-2 control-label')) !!}
	<div class="col-sm-8">
		{!! Form::text('quantity', Input::old('quantity'), array('class' => 'form-control')) !!}
	</div>
</div> 
<div class="form-group">
    <div class="col-sm-offset-2 col-sm-8">
      <div class="checkbox">
        <label>
          {!! Form::checkbox('out_of_stock' , 1) !!} Out of stock
        </label>
      </div>
    </div>
</div> 
<div class="form-group">
	{!! Form::label('description', 'Description' , array('class'=>'col-sm-2 control-label')) !!}
	<div class="col-sm-8">
		{!! Form::textarea('description', Input::old('description'), array('class' => 'form-control ckeditor')) !!}
	</div>
</div>
<div class="form-group">
    <div class="col-sm-offset-2 col-sm-8">
      <div class="checkbox">
        <label>
          {!! Form::checkbox('status' , 1) !!} Status
        </label>
      </div>
    </div>
</div> 
<div class="form-group">
	{!! Form::label('weight', 'Weight' , array('class'=>'col-sm-2 control-label')) !!}
	<div class="col-sm-8">
		{!! Form::text('weight', Input::old('weight'), array('class' => 'form-control')) !!}
	</div>
</div> 

<div class="form-group">
	{!! Form::label('category_id', 'Category' , array('class'=>'col-sm-2 control-label')) !!}
	<div class="col-sm-8">
		{!! Form::select('category_id',$category_options,Input::old('category_id'), array('class' => 'form-control')) !!}
	</div> 
</div>

<div class="form-group">
	{!! Form::label('images', 'Image' , array('class'=>'col-sm-2 control-label')) !!}
	<div class="col-sm-8">
		<div style="position:relative;">
	        <a class='btn btn-primary' href='javascript:;'>
	            Choose File...
	            <input multiple type="file" style='position:absolute;z-index:2;top:0;left:0;filter: alpha(opacity=0);-ms-filter:"progid:DXImageTransform.Microsoft.Alpha(Opacity=0)";opacity:0;background-color:transparent;color:transparent;' name="images[]" size="40"   onmouseout="$('#upload-file-info').html(''); for (var i = 0; i < this.files.length; i++) $('#upload-file-info').append('<span class=\'label label-info\'>'+this.files[i].name+'</span>&nbsp;&nbsp;');">
	        </a>
	        &nbsp;
	        <div id="upload-file-info"> 
	        </div>
		</div> 
	</div> 
</div>

@if ( isset($product) && $product->images->count() )
<div class="form-group">
	<div class="col-sm-2"></div>
	<div class="col-sm-8">
        <?php foreach($product->images as $image){ ?>
            <div class="product-image-widget {{ ($image->id == $product->product_image_id)?'default':'' }} " data-id="{{ $image->id }}"> 
                <img src="/images/products/{{ $image->id }}/thumb/{{ $image->filename }}"  />
                <a class="delete" href="#"><i class="fa fa-trash-o"></i></a>
            </div>
        <?php } ?>
        {!! Form::hidden('product_image_id',$product->product_image_id, array('class' => 'form-control' , 'id' => 'product_image_id')) !!}
	</div>
</div>
@endif

<div class="form-group">
    <div class="col-sm-offset-2 col-sm-8">
		{!! Form::submit('Save', array('class' => 'btn btn-primary')) !!}
		<a href="{{ URL::to('products/') }}">&nbsp;Cancel</a>
	</div>
</div>