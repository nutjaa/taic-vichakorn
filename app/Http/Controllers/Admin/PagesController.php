<?php namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use App\Http\Controllers\Controller; 
use Auth;

use App\Vocabulary ; 
use App\Termdata ; 
use App\Page;
use App\Pagecategory;
use App\Comment;

use Krucas\Notification\Facades\Notification;

class PagesController extends Controller {
	
	public function __construct()
	{
		$this->middleware('auth');
	}
	
	public function index(Request $requests)
	{ 
		$q = $requests->input('q') ;  
    $c = $requests->input('c') ;  
		$pages = Page::with('categories','categories.term')->hasCategory($c)->search($q)->orderBy('updated_at','DESC')->paginate(15)  ;   
    
    $vocab = Vocabulary::Page()->first() ; 
    $category_options = Termdata::GenerateOptionByVid($vocab->id , 0) ; 
      
		if ($requests->ajax()){ 
			return response()->json($pages) ;   	
		}
    return view('admin.page.index')->with('pages' , $pages)->with('q',$q)->with('c',$c)->with('categoryOptions',$category_options) ; 
	}
	
	public function create(){ 
    $vocab = Vocabulary::Page()->first() ; 
    $category_options = Termdata::GenerateOptionByVid($vocab->id , 0) ; 
    
    $vocab = Vocabulary::Name('lang')->first() ;  
    $lang_options = Termdata::NameValueOption($vocab->id  ) ; 
		return view('admin.page.create')->with('category_options',$category_options)->with('lang_options' , $lang_options);
	}
	
	public function store(Request $requests){ 
		$page = new Page() ;
		$page->title = $requests->input('title'); 
		$page->sefu = $requests->input('sefu');  
		$page->body = $requests->input('body');  
		$page->status = $requests->input('status',0); 
		$page->has_comments = $requests->input('has_comments',0); 
		$page->order = $requests->input('order',0); 
		$page->createby = Auth::user()->id ;  
		$page->lang = $requests->input('lang','th');  
    $categories = $requests->input('category');  
   
		if ($page->save()) {
      foreach($categories as $category){
        if($category == 0)
          continue ; 
        $pageCategory = new Pagecategory() ; 
        $pageCategory->page_id = $page->id ; 
        $pageCategory->term_id = $category ; 
        if(! $pageCategory->isAdded()){
          $pageCategory->save() ; 
        }
      }
			Notification::success('Successfull create page <a href="'.url('pages/' . $page->id . '/edit').'">'.$page->title.'</a>');
			return redirect('pages?c='.$page->categories->first()->term_id) ; 
		}else{ 
			Notification::error( $page->errors()->All());
			return redirect('pages/create')->withInput() ; 
		}
	}
	
	public function edit(Request $requests , $id)
	{  
		$page = Page::find($id); 
 		$vocab = Vocabulary::Page()->first() ; 
    $category_options = Termdata::GenerateOptionByVid($vocab->id , 0) ; 
    $vocab = Vocabulary::Name('lang')->first() ;  
    $lang_options = Termdata::NameValueOption($vocab->id  ) ; 
		return view('admin.page.edit')
			->with('page', $page) 
			->with('category_options',$category_options)
			->with('lang_options' , $lang_options);
	}
	
	public function update(Request $requests , $id)
	{ 
		$page = Page::find($id);
		$page->title = $requests->input('title');  
		$page->sefu = $requests->input('sefu'); 
		$page->body = $requests->input('body');  
		$page->status = $requests->input('status',0); 
		$page->has_comments = $requests->input('has_comments',0); 
    $categories = $requests->input('category'); 
		$page->lang = $requests->input('lang','th');    
		$page->order = $requests->input('order',0); 
		 
		if ($page->updateUniques()) {
	    foreach($categories as $category){
        if($category == 0)
          continue ; 
        $pageCategory = new Pagecategory() ; 
        $pageCategory->page_id = $page->id ; 
        $pageCategory->term_id = $category ; 
        if(! $pageCategory->isAdded()){
          $pageCategory->save() ; 
        }
      }
      Pagecategory::where('page_id','=',$page->id)->whereNotIn('term_id',array_values($categories))->delete() ;  
      Notification::success('Successfull update page <a href="'.url('pages/' . $page->id . '/edit').'">'.$page->title.'</a>');
      return redirect('pages?c='.$page->categories->first()->term_id) ; 
		}else{ 
			Notification::error( $page->errors()->All());
			return redirect('pages/' . $page->id . '/edit')->withInput() ; 
		} 
	}
	
	public function comments(Request $requests , $id){
    $page = Page::find($id); 
    return view('admin.page.comments.index')
			->with('page', $page)   ;
  }
  
  public function commentedit(Request $requests , $id , $cid ){
    $page = Page::find($id) ; 
    $comment = Comment::find($cid) ; 
    return view('admin.page.comments.edit')
			->with('page', $page)->with('comment' , $comment)   ;
  }
}