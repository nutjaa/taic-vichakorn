<?php namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Page; 
use Krucas\Notification\Facades\Notification;

use Mail;
use Config;

use SEOMeta;
use OpenGraph;
use Twitter;

class ContactController extends Controller {
	
	public function index(){ 
    SEOMeta::setTitle('ติดต่อเรา');
    SEOMeta::setDescription('ติดต่อและสอบถามข้อมูลต่างๆกับทางทีมงาน Luminance Cocktail');
    
    OpenGraph::setDescription('ติดต่อและสอบถามข้อมูลต่างๆกับทางทีมงาน Luminance Cocktail');
    OpenGraph::setTitle('ติดต่อเรา');
    OpenGraph::setUrl(url('/contactus')); 
    
        
        
		$page = Page::active()->sefu('contact-us')->first() ; 
		return view('contact')->with('page',$page) ; 
	}
	
	public function submit(Request $request){
		$sendTo = Config::get('mail.contact_email');  
		
		$this->validate($request, [
			'email' => 'required|email', 'description' => 'required','captcha' => 'required|captcha'
		]);
		
		$email = $request->input('email') ; 
		$description = $request->input('description');
		
		Mail::send('emails.contactus', ['email' => $email , 'description' => $description ], function($message)use ($email,$sendTo)
		{ 
			$message->from($email, $email);
		    $message->to($sendTo,$sendTo)->subject('ข้อความจาก contact us ');
		});
		
		Notification::success('ทางเราได้รับข้อความของท่านแล้ว');
		return redirect('contactus') ; 
	}
}