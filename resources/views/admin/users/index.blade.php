@extends('layouts.admin')
@section('content') 
<div class="breadcrumbs">
    <ol class="breadcrumb">
        <li><a href="/"><i class="fa fa-home"></i>Home</a></li>
        <li><a href="/admin">Admin</a></li>
        <li class="active">Users</li>
    </ol>
</div>
<div class="innerLR">
    <div class="row manage-header">
        <div class="col-lg-7">
        	<h2>Users Management</h2>
        </div>
        <div class="col-lg-2">
        	<a class="btn btn-small btn-primary" href="{{ URL::to('users/create') }}">Create new user</a>
        </div>
        <div class="col-lg-3">
            {!! Form::open(array( 'method' => 'get')) !!}
                 <input type="text" name="q" placeholder="search" class="span3 pull-right form-control" value="{{ $q }}" />
            {!! Form::close() !!}
        </div>
    </div>
    <div class="widget">
        {!! Notification::showAll() !!}
        <div class="table-responsive">
            <table class="table">
                <thead>
                    <tr>
                        <th>#</th>
        				<th>Email</th>
        				<th>Name</th>  
                        <th>Role</th>
        				<th>Actions</th>
                    </tr>
                </thead>
                <tbody>
                @foreach($users as $user)
        			<tr>
        				<td>{{ $user->id }}</td>
                <td>{{ $user->email }} @if($user->is_vip)<i class="fa fa-star"></i>@endif
                  @if($user->status == 0 )
                      <span class="label label-danger">&nbsp;</span>
                  @else
                      <span class="label label-success">&nbsp;</span>
                  @endif
                </td>
        				<td>{{ $user->name }}</td>  
        				<td>{{ $user->role->label }}</td>
        				<td>
        					<a   href="{{ URL::to('users/' . $user->id . '/edit') }}">Edit</a>
        				</td>
        			</tr>
        		@endforeach
                </tbody>
            </table>
        </div>
    </div>
    
</div>
@stop

@section('footerjs')  
@stop

@section('headercss')   
@stop