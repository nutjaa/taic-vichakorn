@extends('layouts.master')

@section('content')
<div class="row">
	<ol class="breadcrumb">
	  <li><a href="/">Home</a></li> 
	  <li class="active">สิทธิพิเศษสมาชิก</li>
	</ol>
</div>
<div class="row"> 
	<div class="col-md-9">
		{!! $page->body !!}
		<br />
		@if(Auth::guest())
		<div class="row">
			<div class="col-sm-offset-4 col-sm-4 col-xs-offset-3 col-xs-6">
			<a href="/auth/register" class="btn btn-primary form-control">สมัครสมาชิก คลิ๊กที่นี่</a>
			</div>
		 </div>
		@endif
	</div>
	<div class="col-xs-3">
		<br /><br />
		<div class="fb-page" data-href="https://www.facebook.com/luminancecocktail" data-width="100%" data-hide-cover="false" data-show-facepile="true" data-show-posts="false"><div class="fb-xfbml-parse-ignore"><blockquote cite="https://www.facebook.com/luminancecocktail"><a href="https://www.facebook.com/luminancecocktail">Luminance Cocktail</a></blockquote></div></div>
	</div>
</div>
@stop