<?php namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Product;
use App\Termdata;
use App\Vocabulary;
use App\ProductImage;
use App\Comment;
use Auth ; 
use DB ; 

use Krucas\Notification\Facades\Notification;

class ProductsController extends Controller {
	public function __construct()
	{
		$this->middleware('auth');
	}
	
	public function index(Request $requests)
	{ 
		$q = $requests->input('q') ; 
    $c = $requests->input('c') ;  
    
    $vocab = Vocabulary::Product()->first() ; 
    $category_options = Termdata::GenerateOptionByVid($vocab->id , 0) ; 
     
		$products = Product::search($q)->hasCategory($c)->orderBy('weight','ASC')->paginate(15)  ; 
    return view('admin.product.index')->with('products' , $products)->with('q',$q)->with('c',$c)->with('categoryOptions',$category_options)  ; 
	}
	
	
	public function create(){ 
    $vocab = Vocabulary::Product()->first() ; 
    $category_options = Termdata::GenerateOptionByVid($vocab->id , 0) ; 
      
    $lang_options = Termdata::NameValueOption($vocab->id  ) ; 
		return view('admin.product.create')->with('category_options',$category_options) ;
	}
	
	public function store(Request $requests){ 
		$product = new Product() ;
		$product->name = $requests->input('name'); 
		$product->sefu = $requests->input('sefu');  
		$product->sku = $requests->input('sku');  
		$product->description = $requests->input('description');  
		$product->category_id = $requests->input('category_id');  
		$product->price = $requests->input('price',0); 
		$product->sale_price = $requests->input('sale_price',0); 
		$product->status = $requests->input('status',0);  
		$product->quantity = $requests->input('quantity');   
    $product->out_of_stock = $requests->input('out_of_stock',0); 
    $product->weight = $requests->input('weight',0);  
		if ($product->save()) { 
			
			$images = $requests->file('images'); 
			foreach($images as $file){
				$productImage = new ProductImage() ; 
				$productImage->product_id = $product->id ; 
				$productImage->filename = $file->getClientOriginalName() ; 
				$productImage->save() ;  
				$file->move('images/products/' . $productImage->id . '/' , $file->getClientOriginalName()   ); 
        $productImage->processImage() ; 
                 
			} 
      if( $product->images->count() > 0){ 
        if(is_null( $product->default_image) || $product->product_image_id == 0){ 
          $product->product_image_id = $product->images->first()->id  ;       
          $product->updateUniques() ;     
        }
      } 
      
      #process relate weight ; 
      DB::table('products')->where('weight', '>=' , $product->weight)->where('id','<>',$product->id)->increment('weight');
      
			Notification::success('Successfull create product <a href="'.url('products/' . $product->id . '/edit').'">'.$product->name.'</a>');
			return redirect('products') ; 
		}else{ 
			Notification::error( $product->errors()->All());
			return redirect('products/create')->withInput() ; 
		}
	}
	
	public function edit(Request $requests , $id)
	{  
		$product = Product::find($id); 
 		$vocab = Vocabulary::Product()->first() ; 
	    $category_options = Termdata::GenerateOptionByVid($vocab->id , 0) ;  
		return view('admin.product.edit')
			->with('product', $product) 
			->with('category_options',$category_options) ;
	}
	
	public function update(Request $requests , $id)
	{  
		$product = Product::find($id);
    $oldWeight = $product->weight ;
		$product->name = $requests->input('name'); 
		$product->sefu = $requests->input('sefu');  
		$product->sku = $requests->input('sku');  
		$product->description = $requests->input('description');  
		$product->category_id = $requests->input('category_id');  
		$product->price = $requests->input('price',0); 
		$product->sale_price = $requests->input('sale_price',0); 
		$product->status = $requests->input('status',0);  
		$product->quantity = $requests->input('quantity');    
    $product->out_of_stock = $requests->input('out_of_stock',0); 
		$product->product_image_id = $requests->input('product_image_id',0);
		$product->weight = $requests->input('weight',0);  
		if ($requests->hasFile('images')){
			$images = $requests->file('images'); 
			foreach($images as $file){
				$productImage = new ProductImage() ; 
				$productImage->product_id = $product->id ; 
				$productImage->filename = $file->getClientOriginalName() ; 
				$productImage->save() ;  
				$file->move('images/products/' . $productImage->id . '/' , $file->getClientOriginalName()   ); 
        $productImage->processImage() ; 
                 
			} 
      if( $product->images->count() > 0){ 
        if(is_null( $product->default_image)){
          $product->product_image_id = $product->images->first()->id  ;           
        }
      }
		}
  
		if ($product->updateUniques()) { 
		  #process relate weight ; 
      if($oldWeight != $product->weight){ 
        DB::table('products')->where('weight', '>=' , $product->weight)->where('id','<>',$product->id)->increment('weight');
      }
			Notification::success('Successfull update page <a href="'.url('products/' . $product->id . '/edit').'">'.$product->name.'</a>');
			return redirect('products') ; 
		}else{ 
			Notification::error( $product->errors()->All());
			return redirect('products/' . $product->id . '/edit')->withInput() ; 
		} 
	}
  
  public function comments(Request $requests , $id){
    $product = Product::find($id); 
    return view('admin.product.comments.index')
			->with('product', $product)   ;
  }
  
  public function commentedit(Request $requests , $id , $cid ){
    $product = Product::find($id) ; 
    $comment = Comment::find($cid) ; 
    return view('admin.product.comments.edit')
			->with('product', $product)->with('comment' , $comment)   ;
  }
	
}