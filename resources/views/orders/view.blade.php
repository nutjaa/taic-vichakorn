@extends('layouts.master')

@section('content')
<div class="row">
	<ol class="breadcrumb">
	  <li><a href="/">Home</a></li> 
	  <li><a href="{{ url('orders') }}">ประวัติการสั่งซื้อ</a></li> 
	  <li class="active">#{{$order->orderNumber}}</li>
	</ol>
</div>
<div class="row"> 
    <div class="col-md-9">
    	 <h2>ใบสั่งซื้อเลขที่ #{{$order->orderNumber}}</h2>
		 {!! Notification::showAll() !!} 
    	 <table class="table table-striped">
    	 	<thead>
				<tr>
					<th></th>
					<th>รหัส</th>
					<th>ชื่อ</th>
					<th class="text-right">ราคา</th>
					<th class="text-right">จำนวน</th>
					<th class="text-right">รวม</th>
				</tr> 
			</thead>
			<tbody>
				@foreach($order->items as $item)
				<tr>
					<td><img src="{{$item->product->imageDefault('thumb')}}" title="{{$item->product->name}}" /></td>
					<td>{{$item->product->sku}}</td>
					<td>{{$item->product->nameFirst}}</td>
					<td class="text-right">{{number_format($item->price,2)}}</td>
					<td class="text-right">{{$item->quantity}}</td>
					<td class="text-right">{{$item->total}}</td>
				</tr>
				@endforeach
				<tr>
					<td colspan="5" class="text-right">รวม</td>
					<td class="text-right">{{number_format($order->total_price,2)}}</td>
				</tr>
        @if( $order->discount > 0 )
        <tr>
					<td colspan="5" class="text-right">ส่วนลด VIP 10%</td>
					<td class="text-right">{{number_format($order->discount,2)}}</td>
				</tr>
        @endif
				<tr>
					<td colspan="5" class="text-right">ค่าจัดส่ง</td>
					<td class="text-right">{{$order->shipping_price}}</td>
				</tr>
        @if( $order->used_point > 0 )
        <tr>
					<td colspan="5" class="text-right">ใช้แต้มสะสม</td>
					<td class="text-right">{{number_format($order->used_point,2)}}</td>
				</tr>
        @endif
				<tr>
					<td colspan="5" class="text-right">รวมทั้งสิ้น</td>
					<td class="text-right">{{$order->grandTotal}}</td>
				</tr>
			</tbody>
    	 </table>
    	 <h2>ประวัติการเปลี่ยนแปลงสถานะ</h2>
    	 <table class="table table-striped">
    	 	<thead>
    	 		<tr>
					<th>สถานะ</th>
					<th class="text-right">วันที่</th> 
		 		</tr>
    	 	</thead>
    	 	<tbody>
				@foreach($order->statuses as $status)
				<tr>
					<td >{{$status->status->label}}</td>
					<td class="text-right">{{$status->createdAtFormat}}</td>
				</tr>
				@endforeach 
		 	</tbody>
   	 	</table>
   	 	@if(count($order->confirmtransfers)>0)
   	 	<h3>แจ้งโอนเงิน</h3>
   	 	<table class="table table-striped">
   	 		<thead>
				<tr> 
					<th>โอนจากธนาคาร</th>
					<th>โอนเข้าธนาคาร</th>
					<th>จำนวนเงิน</th>
					<th class="text-right">วันที่ทำการโอน</th>
				</tr>	
				@foreach($order->confirmtransfers as $transfer)
				<tr> 
					<td>{{$transfer->frombank}}</td>
					<td>{{$transfer->tobank}}</td>
					<td>{{$transfer->amount}}</td>
					<td  class="text-right">{{$transfer->transferedAtFormat}}</td>
				</tr>
				@endforeach 
			</thead>
   	 	</table>
   	 	@endif
    </div>
    <div class="col-md-3">
    	<br />
		<div class="panel panel-primary"> 
			<div class="panel-heading">
				สถานะ {{$order->status->label}}
			</div>
			<div class="panel-body">
				<h4><span class="label label-large {{$order->labelStatus}}">{{$order->status->label}}</span></h4> 
				@if( $order->isShipping  )
        <br />
        <strong>EMS</strong> : {{$order->ship_no}}  
        <a class="btn btn-success form-control ems-checking" href="http://emsbot.com/#/?s={{$order->ship_no}}" target="_blank">ตรวจสอบ EMS</a> 
        <br /><br />
        
        @endif 
			</div>
		</div>
		<div class="panel panel-primary"> 
			<div class="panel-heading">
				ชื่อและที่อยู่
			</div>
			<div class="panel-body">
				{{$order->firstname}} {{$order->lastname}}<br />
				{!!nl2br($order->address)!!}
			</div>
		</div>
		<a href="http://track.thailandpost.co.th/tracking/default.aspx" target="_blank"><img src="/images/TrackTrace.jpg" class="img-responsive" /></a>
	</div>
</div>

@include('blocks.emsmodal')
@stop

@section('footerjs')  
    <script type="text/javascript" src="/js/emsModal.js"></script>
@stop