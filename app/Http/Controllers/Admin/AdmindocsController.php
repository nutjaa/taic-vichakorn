<?php namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller; 

use Carbon;
use Krucas\Notification\Facades\Notification;

use App\Page;
use App\Pagecategory;
use App\Vocabulary ; 
use App\Termdata ; 


class AdmindocsController extends Controller {
  public $vocab  ; 
 	public $termdata;
  
	public function __construct()
	{
		$this->middleware('auth');
    
    $this->vocab = Vocabulary::Page()->first() ; 
		$this->termdata = Termdata::where('name' , '=' , 'admin-doc')->where('vid' , '=' , $this->vocab->id )->first() ; 
	}
  
  public function index(Request $requests){
    $q = $requests->input('q') ;  
    $pages = Page::join('page_categories',function($join){
			$join->on('page_categories.page_id','=','pages.id') ; 
		})->where('page_categories.term_id','=',$this->termdata->id)->search($q)->active()->orderBy('pages.order', 'ASC')->get()  ; 
    
    return view('admin.docs.index')->with('pages',$pages)->with('q',$q) ;
  }
  
  public function view($id , $sefu){
    $page = Page::find($id)  ; 
    return view('admin.docs.view')->with('page',$page) ; 
  }
}