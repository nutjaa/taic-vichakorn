<?php namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Product;
use App\Termdata;
use App\Vocabulary;
use App\ProductImage;

use Krucas\Notification\Facades\Notification;

class ProductImageController extends Controller {
    public function destroy($id){
        $productimage = ProductImage::find($id); 
        $productimage->delete();
        return response()->json(['success' => true, 'id' => $id]);
    }
}