<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePagesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('pages', function(Blueprint $table)
		{
			$table->increments('id');
			$table->timestamps();
            
      $table->string('title',256);
      $table->string('sefu',256);
      $table->text('body');
      $table->softDeletes();
      
      $table->tinyInteger('status');
      $table->text('meta');
      $table->integer('createby');
      $table->string('lang',2);
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('pages');
	}

}
