@extends('layouts.admin')
@section('content') 
<div class="breadcrumbs">
  <ol class="breadcrumb">
    <li><a href="/"><i class="fa fa-home"></i>Home</a></li>
    <li><a href="/admin">Admin</a></li>
    <li class="active">Vocabulary</li>
  </ol>
</div>
<div class="innerLR">
    <div class="row manage-header">
        <div class="col-lg-7">
        	<h2>Vocabulary Management</h2>
        </div>
        <div class="col-lg-2">
        	<a class="btn btn-small btn-primary" href="{{ URL::to('vocabulary/create') }}">Create new vocabulary</a>
        </div>
        <div class="col-lg-3">
            {!! Form::open(array( 'method' => 'get')) !!}
                <input type="text" name="q" placeholder="search" class="span3 pull-right form-control" value="{{ $q }}" />
            {!! Form::close() !!}
        </div>
    </div>
    <div class="widget">
        {!! Notification::showAll() !!}
        <div class="table-responsive">
            <table class="table">
                <thead>
    			<tr>
    				<th width="45">ID</th>
    				<th>Name</th>
    				<th>Description</th> 
                    <th>Actions</th>
    			</tr>
    		</thead>
    		<tbody>
    		@foreach($vocabularies as $vocabulary)
    			<tr>
    				<td>{{ $vocabulary->id }}</td>
    				<td>{{ $vocabulary->name }}</td>
    				<td>{{ $vocabulary->description }}</td> 
                    <td>
    					<a href="{{ URL::to('vocabulary/' . $vocabulary->id . '/edit') }}">Edit</a>
    				</td>
    			</tr>
    		@endforeach
    		</tbody>
    	</table>
        </div>
    </div>
</div>  
@stop