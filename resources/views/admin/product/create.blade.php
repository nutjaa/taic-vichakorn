@extends('layouts.admin')
@section('content') 
<div class="breadcrumbs">
    <ol class="breadcrumb">
        <li><a href="/"><i class="fa fa-home"></i>Home</a></li>
        <li><a href="/admin">Admin</a></li>
        <li><a href="/products">Product</a></li>
        <li class="active">Create new product</li>
    </ol>
</div>

<div class="innerLR">
    <div class="row manage-header">
        <div class="col-lg-7">
        	<h2>Create new product</h2>
        </div>
 	</div>
 	
 	<div class="widget">
        {!! Notification::showAll() !!}
 	
     	{!! Form::open(array('url' => 'products' , 'class'=>'form-horizontal' , 'role' => 'form' , 'files'=>true )) !!}
            @include('admin/product/form')  
     	{!! Form::close() !!}
 	</div>
</div>

@stop

@section('footerjs') 
	{!! HTML::script('js/ckeditor/ckeditor.js'); !!}  
@stop