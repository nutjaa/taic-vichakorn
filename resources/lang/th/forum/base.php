<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Base forum language strings
    |--------------------------------------------------------------------------
    */

    // General
    'home_title'        => "Forum",
    'index'             => "เว็บบอร์ดแจ้งข่าว พูดคุยและสอบถาม",
    'author'            => "ผู้เขียน",
    'last_5_thread'     => "5 กระทู้ล่าสุด" ,
    'thread_and_creatorname' => 'ชื่อกระทู้/ผู้ตั้งกระทู้' , 
    'title'             => "ชื่อกระทู้",
    'subject'           => "Subject",
    'edit'              => "Edit",
    'delete'            => "Delete",
    'reply'             => "ตอบกลับ",
    'replies'           => "ข้อความ",
    'new_reply'         => "New reply",
    'quick_reply'       => "Quick reply",
    'reply_added'       => "Reply added",
    'send'              => "Send",
    'posting_into'      => "You're posting into",
    'you_are_editing'   => "You're editing",
    'posted_at'         => "สร้างเมื่อ",
    'last_update'       => "แก้ไขล่าสุด",
    'generic_confirm'   => "Are you sure?",
    'actions'           => "Actions",
    'cancel'            => "ยกเลิก",
    'unread'            => "New",
    'updated'           => "Updated",
    'mark_read'         => "Mark as read",

    // Categories
    'category'          => "Category",
    'no_categories'     => "No categories found",

    // Threads
    'threads'           => "กระทู้",
    'first_thread'      => "สร้างกระทู้แรก!",
    'new_thread'        => "สร้างกระทู้ใหม่",
    'thread_created'    => "Thread created",
    'pin_thread'        => "Pin/unpin this thread",
    'pinned'            => "Pinned",
    'lock_thread'       => "Lock/unlock this thread",
    'locked'            => "Locked",
    'thread_updated'    => "Thread updated",
    'delete_thread'     => "Delete this thread",
    'thread_deleted'    => "Thread deleted",
    'no_threads'        => "ไม่พบกระทู้",
    'newest_thread'     => "Newest thread",
    'new_threads'       => "New & updated threads",
    'marked_read'       => "All threads have been marked as read",
    'total_thread_view' => 'เปิดอ่าน	' , 

    // Posts
    'posts'             => "Posts",
    'post'              => "ข้อความ",
    'your_post'         => "Your post",
    'latest_posts'      => "ข้อความล่าสุด",
    'edit_post'         => "Edit post",
    'post_updated'      => "Post updated",
    'post_deleted'      => "Post deleted",
    'last_post'         => "ข้อความล่าสุด",
    'view_post'         => "View post",
    'total_posts'		=> 'ข้อความ' , 
    
    'by'				=> 'โดย' ,
	'room'				=> 'ห้อง'
];
