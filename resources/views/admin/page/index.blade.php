@extends('layouts.admin')
@section('content') 
<div class="breadcrumbs">
    <ol class="breadcrumb">
        <li><a href="/"><i class="fa fa-home"></i>Home</a></li>
        <li><a href="/admin">Admin</a></li>
        <li class="active">Pages</li>
    </ol>
</div>
<div class="innerLR">
    <div class="row manage-header">
        <div class="col-lg-6">
        	<h2>Pages Management</h2>
        </div>
        <div class="col-lg-2">
        	<a class="btn btn-small btn-primary" href="{{ URL::to('pages/create') }}">Create new page</a>
        </div>
        {!! Form::open(array( 'method' => 'get')) !!}
        <div class="col-lg-2"> 
        	{!! Form::select('c',$categoryOptions,$c, array('class' => 'form-control', 'onchange' => "this.form.submit()" ) ) !!}
        </div>
        <div class="col-lg-2"> 
        	<input type="text" name="q" placeholder="search" class="span3 pull-right form-control" value="{{ $q }}" /> 
        </div>
        {!! Form::close() !!}
    </div>
    <div class="widget">
        {!! Notification::showAll() !!}
        <div class="table-responsive">
            <table class="table">
                <thead>
    			<tr>
    				<th width="45">ID</th>
    				<th>Title</th>
    				<th>Lang</th>
    				<th>Order</th>
                    <th>Tags</th>
    				<th>Updated</th> 
    				<th>Created By</th>
    				<th>Actions</th>
    			</tr>
    		</thead>
    		<tbody>
    		@foreach($pages as $page)
    			<tr>
    				<td>{{ $page->id }}</td>
    				<td>{{ $page->title }}
                    @if($page->status == 0 )
                        <span class="label label-danger">&nbsp;</span>
                    @else
                        <span class="label label-success">&nbsp;</span>
                    @endif
                    </td>
                    <td>
						{{ $page->lang }}
					</td>
					<td>
						{{ $page->order }}
					</td>
                    <td>
                    @foreach($page->categories as $category)
                        <a href="/pages?c={{ $category->term->id }}"><span class="badge">{{$category->term->label}}</span></a>
                    @endforeach
                    </td>
    				<td>{{ $page->updated_at }}</td>
    				<td>{{ $page->user->shortname() }}</td>
    				<td>
    					<a class="btn btn-small btn-info" href="{{ URL::to('pages/' . $page->id . '/edit') }}">Edit</a>
              &nbsp;
              <a class="btn btn-small btn-info" href="{{ URL::to('pages/' . $page->id . '/comments') }}">Comments({{$page->comments->count()}})</a>
    				</td>
    			</tr>
    		@endforeach
    		</tbody>
    	</table>
        <div class="text-center">
    		<?php echo $pages->appends(array('q' => $q))->render(); ?>
    	</div>
        </div>
    </div>
</div>  
@stop