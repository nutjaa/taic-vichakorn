<?php namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Page;
use App\Product;
use App\Order;
use App\OrderStatus;
use App\OrderItem;
use App\Vocabulary;
use App\Termdata;
use App\Invoice;
use App\HistoricalPoint;
use Session ; 
use Auth;
use Mail;
use Config ; 

use Krucas\Notification\Facades\Notification;

class OrderController extends Controller {
	
	public function index(){  
		$products = Product::active()->orderBy('weight','ASC')->get() ; 
		$page1 = Page::active()->sefu('order-1')->first() ; 
		$page2 = Page::active()->sefu('order-2')->first() ; 
		$page3 = Page::active()->sefu('order-3')->first() ; 
		return view('order.index')->with('products' , $products)->with('page1',$page1)->with('page2',$page2)->with('page3',$page3); 
	}
 	
 	public function doOrder(Request $requests){
 	  $products = $requests->input('products') ; 
    $sess_products = array() ; 
    forEach($products as $k => $product){
      if($product['selected_quantity'] > 0){
        $sess_products[$k] = $product ; 
      }
    }
 		Session::put('products', $sess_products );
    Session::put('point',0) ;
 		return redirect('order/comfirm-and-shipping')  ; 
 	}
 	
 	private function processSessionCart(){
 		$products = Session::get('products') ;  
 		$ps = []  ;
 		$totalPrice = 0 ; 
 		foreach($products as $k => $product){
 			if(isset($product['selected_product'])){
 				$p = Product::find($k) ; 
 				$totalPrice += $p->sessionTotalPrice ; 
 				$ps[] = $p ;
 			}
 		}
 		
 		$shipping = 0 ; 
 		if($totalPrice < 1000)
 			$shipping = 50 ; 
    
    $discount = 0 ;   
    if(!Auth::guest()){
      if(Auth::user()->is_vip){
        $discount = $totalPrice * 0.1 ; 
      }
    }
    
    $point = Session::get('point') ; 
 			
		$grandTotal = $totalPrice + $shipping - $discount - $point ; 
		
		return ['products' => $ps , 'totalPrice' => $totalPrice , 'shipping' => $shipping , 'grandTotal' => $grandTotal , 'discount' => $discount , 'point' => $point ] ; 
 	}
 	
 	private function clearCartSession(){
 		Session::forget('products');
 		Session::forget('shipping');
    Session::forget('point');
 	}
 	
 	public function confirm(){
 		$products = Session::get('products') ;  
 	 	if( count($products) == 0 )
 	 		return redirect('order')  ; 
 	 		
		$data = $this->processSessionCart() ; 
 		
 		return view('order.confirm')->with('products',$data['products'])->with('totalPrice' , $data['totalPrice'])->with('shipping',$data['shipping'])->with('grandTotal',$data['grandTotal'])->with('discount',$data['discount'])->with('point',$data['point']) ; 
 	}
  
  public function addPoint(Request $requests){
    $point = (int)$requests->input('point',0) ;
    $data = $this->processSessionCart() ; 
    
    $possiblePoint = $data['totalPrice'] + $data['shipping'] - $data['discount'] ; 
    $point = ($point > $possiblePoint)?  $possiblePoint : $point ;
    
    $point = ($point > Auth::user()->point)? Auth::user()->point : $point ; 
    
    Session::put('point',$point) ; 
    
    Notification::success('ใช้แต้มสะสมเพื่อเป็นส่วนลด จำนวน ' . $point . ' แต้ม'); 
    return redirect('order/comfirm-and-shipping')  ;  
  }
    
  public function doConfirm(Request $requests){
    Session::put('shipping.name' , $requests->input('name')) ; 
    Session::put('shipping.lastname' , $requests->input('lastname')) ; 
    Session::put('shipping.email' , $requests->input('email')) ; 
    Session::put('shipping.phone' , $requests->input('phone')) ; 
    Session::put('shipping.address' , $requests->input('address')) ; 
    return redirect('order/payment')  ;  
  } 
    
  public function payment(){
    $products = Session::get('products') ;  
 	 	if( count($products) == 0 )
 	 		return redirect('order')  ; 
 	 		
		$page1 = Page::sefu('order-payment-credit-card')->first() ; 
		$page2 = Page::sefu('order-payment-atm')->first() ; 
    
    #create invoice ; 
    $invoice = new Invoice() ; 
    $data = Session::get('shipping') ;  
    $data['user_id'] = Auth::user()->id ;  
    $data['point'] = Session::get('point') ;  
    $invoice->data = $data ; 
    $invoice->save() ; 
 	 		
    $data = $this->processSessionCart() ; 
    return view('order.payment')->with('products',$data['products'])->with('totalPrice' , $data['totalPrice'])->with('shipping',$data['shipping'])->with('grandTotal',$data['grandTotal'])->with('discount',$data['discount'])->with('page1',$page1)->with('page2',$page2)->with('invoice' , $invoice )->with('point',$data['point']) ; 
  }
    
  public function doPayment(Request $requests){
    $products = Session::get('products') ;  
 	 	if( count($products) == 0 )
 	 		return redirect('order')  ;
			  
		if($requests->input('paymentoption',2) == 2){
			#process atm transfer
			$data = $this->processSessionCart() ; 
	    	#Create order ;
			$orderVocab = Vocabulary::name('order')->first() ;  
    	$firstStatus = Termdata::vid($orderVocab->id)->name('ordered')->first() ; 
    	
    	$order = new Order() ; 
    	$order->user_id = Auth::user()->id ; 
    	$order->status_id = $firstStatus->id ; 
    	$order->firstname = Auth::user()->shipping('name');
    	$order->lastname = Auth::user()->shipping('lastname');
    	$order->phone = Auth::user()->shipping('phone');
    	$order->email = Auth::user()->shipping('email');
    	$order->address = Auth::user()->shipping('address');
    	$order->shipping_price = $data['shipping'] ; 
      $order->discount = $data['discount'] ; 
    	$order->total_price = $data['totalPrice'] ; 
    	$order->payment_type_id	 = $requests->input('paymentoption',2) ; 
      $order->used_point = $data['point'] ; 
    	$order->save() ; 
      
      $this->processUsePoint($order) ; 
       
    	
    	$orderStatus = new OrderStatus() ; 
    	$orderStatus->status_id = $firstStatus->id ; 
    	$orderStatus->order_id = $order->id ;  
    	$orderStatus->save() ; 
    	
    	foreach($data['products'] as $product){
    		$orderItem = new OrderItem() ; 
    		$orderItem->product_id = $product->id ; 
    		$orderItem->price = $product->sale_price ; 
    		$orderItem->quantity = $product->sessionQty ; 
    		$orderItem->order_id = $order->id ;  
    		$orderItem->save() ; 
    	}
	    	
      $this->clearCartSession() ; 
	    	
	    	#send mail order information
      Mail::send('emails.order', ['order' => $order ], function($message)use ($order)
			{ 
			    $message->to($order->email,$order->fullname)->subject('[luminancecocktail.com] รายการสั่งซื้อ #'.$order->orderNumber);
			});
			
			if($order->isBankTransfer){
				Mail::send('emails.waiting_transfer', ['order' => $order ], function($message)use ($order)
				{ 
				    $message->to($order->email,$order->fullname)->subject('[luminancecocktail.com] รอการโอนเงิน #'.$order->orderNumber);
				});
			}
    	Notification::success('เราได้รับรายการสั่งซื้อสินค้าของคุณสมบูรณ์แล้ว ขอบคุณเป็นอย่างยิ่งที่ได้ซื้อสินค้าจากเรา'); 
    	return redirect('orders/' . $order->id) ; 
		}else{
			#process credit card 
			$data = $this->processSessionCart() ;
			
			try{ 
				$charge = \OmiseCharge::create(array(
				  'amount' => $data['grandTotal']*100,
				  'currency' => 'thb',
				  'card' => $requests->input('omise_token') ,  
				));
				
				$orderVocab = Vocabulary::name('order')->first() ;  
    		$firstStatus = Termdata::vid($orderVocab->id)->name('ordered')->first() ; 
    		$confirmTransferStatus = Termdata::vid($orderVocab->id)->name('confirm')->first() ; 
	    	
	    	$order = new Order() ; 
	    	$order->user_id = Auth::user()->id ; 
	    	$order->status_id = $confirmTransferStatus->id ; 
	    	$order->firstname = Auth::user()->shipping('name');
	    	$order->lastname = Auth::user()->shipping('lastname');
	    	$order->phone = Auth::user()->shipping('phone');
	    	$order->email = Auth::user()->shipping('email');
	    	$order->address = Auth::user()->shipping('address');
	    	$order->shipping_price = $data['shipping'] ; 
        $order->discount = $data['discount'] ; 
	    	$order->total_price = $data['totalPrice'] ; 
	    	$order->payment_type_id	 = $requests->input('paymentoption',2) ; 
        $order->used_point = $data['point'] ; 
	    	$order->save() ; 
        
        $this->processUsePoint($order) ; 
	    	
	    	$orderStatus = new OrderStatus() ; 
	    	$orderStatus->status_id = $firstStatus->id ; 
	    	$orderStatus->order_id = $order->id ;  
	    	$orderStatus->save() ; 
	    	
	    	$orderStatus = new OrderStatus() ; 
	    	$orderStatus->status_id = $confirmTransferStatus->id ; 
	    	$orderStatus->order_id = $order->id ;  
	    	$orderStatus->save() ; 
	    	
	    	foreach($data['products'] as $product){
	    		$orderItem = new OrderItem() ; 
	    		$orderItem->product_id = $product->id ; 
	    		$orderItem->price = $product->sale_price ; 
	    		$orderItem->quantity = $product->sessionQty ; 
	    		$orderItem->order_id = $order->id ;  
	    		$orderItem->save() ; 
	    	}
		    	 
        $this->clearCartSession() ; 
		    	
        #send mail order information
        Mail::send('emails.order', ['order' => $order ], function($message)use ($order)
				{ 
				    $message->to($order->email,$order->fullname)->subject('[luminancecocktail.com] รายการสั่งซื้อ #'.$order->orderNumber);
				}); 
				Mail::send('emails.confirmpayment', ['order' => $order ], function($message)use ($order)
        		{ 
        		    $message->to($order->email,$order->fullname)->subject('[luminancecocktail.com] ได้รับเงินโอนแล้ว‏ #'.$order->orderNumber);
        		});
        		
        		$charge->update(array(
				  'description' => '#' . $order->orderNumber . ' : '. Auth::user()->shipping('email') . ' ชำระเงิน ' . $data['grandTotal'] . ' บาท'
				)); 
				Notification::success('เราได้รับรายการสั่งซื้อสินค้าของคุณสมบูรณ์แล้ว ขอบคุณเป็นอย่างยิ่งที่ได้ซื้อสินค้าจากเรา'); 
	    	return redirect('orders/' . $order->id) ; 
				
			}catch(\OmiseException $e){
				Notification::error($e->getMessage());
				return redirect('order/payment') ;  
			} 
		} 
  }
  
  public function cancel_paypal(){
    Notification::error('คุณได้ยกเลิกคำส่งซื้อจาก Paypal');
    return redirect('order/payment') ;  
  }
  
  public function complete_paypal(){
    #get lastest order id ; 
    $this->clearCartSession() ; 
    $order = Order::orderBy('created_at','DESC')->first() ; 
    Notification::success('เราได้รับรายการสั่งซื้อสินค้าของคุณสมบูรณ์แล้ว ขอบคุณเป็นอย่างยิ่งที่ได้ซื้อสินค้าจากเรา'); 
    return redirect('orders/' . $order->id) ; 
  }
  
  public function ipn_paypal(Request $requests){
    $listener = new \WadeShuler\PhpPaypalIpn\IpnListener();
    if( Config::get('services.paypal.sandbox'))
      $listener->use_sandbox = true;
    
    if ($verified = $listener->processIpn()){
      $transactionData = $listener->getPostData();      
      
      $postData = array() ; 
      foreach($transactionData as $index => $data){
        list($key,$val) = explode('=',$data) ; 
        $postData[$key] = $val ; 
      } 
      
      file_put_contents(storage_path('/logs/ipn_success-'.date('Y-m-d').'-'.$postData['invoice'].'.log'), print_r($postData, true) . PHP_EOL, LOCK_EX | FILE_APPEND);
      
      #find invoice ; 
      $invoice = Invoice::find(intval($postData['invoice'])) ; 
      
      if($invoice == null){
        return ; 
      }
      
      if($invoice->status == 0){
        #create new order; 
        $data = $invoice->data ; 
        
        $orderVocab = Vocabulary::name('order')->first() ;  
        $firstStatus = Termdata::vid($orderVocab->id)->name('paypal-pending')->first() ; 
        
        $order = new Order() ; 
	    	$order->user_id = $data['user_id'] ; 
	    	$order->status_id = $firstStatus->id ; 
	    	$order->firstname = $data['name'];
	    	$order->lastname = $data['lastname'];
	    	$order->phone = $data['phone'];
	    	$order->email = $data['email'];
	    	$order->address = $data['address'];
	    	$order->shipping_price = $postData['mc_shipping1'] ; 
        $order->discount = $postData['discount'] ; 
	    	$order->total_price = $postData['mc_gross'] - $postData['mc_shipping1'] +  $postData['discount'] ; 
	    	$order->payment_type_id	 = 3 ;
        if(isset($data['point'])){ 
          $order->used_point = $data['point'] ;  
        }else{ 
          $order->used_point = 0 ;  
        }
	    	$order->save() ; 
        
        $this->processUsePoint($order) ; 
        
        $orderStatus = new OrderStatus() ; 
      	$orderStatus->status_id = $firstStatus->id ; 
      	$orderStatus->order_id = $order->id ;  
        $orderStatus->user_id = $data['user_id'] ; 
      	$orderStatus->save() ; 
        
        for($i = 1 ; $i <= $postData['num_cart_items'] ; $i++ ){ 
      		$orderItem = new OrderItem() ; 
          $product = Product::sku($postData['item_number'.$i])->first() ; 
      		$orderItem->product_id = $product->id ; 
      		$orderItem->price = $product->sale_price ; 
      		$orderItem->quantity = $postData['quantity'.$i] ; 
      		$orderItem->order_id = $order->id ;  
      		$orderItem->save() ; 
      	}
        
        $invoice->status = 1 ; 
        $invoice->order_id = $order->id ; 
        $invoice->save() ; 
        
        #send mail order information
        Mail::send('emails.order', ['order' => $order ], function($message)use ($order)
				{ 
				    $message->to($order->email,$order->fullname)->subject('[luminancecocktail.com] รายการสั่งซื้อ #'.$order->orderNumber);
				});
      }
      
      $order = $invoice->order ; 
      
      if($postData['payment_status'] == 'Completed'){
        $orderVocab = Vocabulary::name('order')->first() ;   
    		$confirmTransferStatus = Termdata::vid($orderVocab->id)->name('confirm')->first() ; 
        
        $orderStatus = new OrderStatus() ; 
	    	$orderStatus->status_id = $confirmTransferStatus->id ; 
        $orderStatus->user_id = $data['user_id'] ; 
	    	$orderStatus->order_id = $order->id ;  
	    	$orderStatus->save() ; 
        
        $order->status_id = $confirmTransferStatus->id ; 
        $order->save() ;
        
        Mail::send('emails.confirmpayment', ['order' => $order ], function($message)use ($order)
    		{ 
    		  $message->to($order->email,$order->fullname)->subject('[luminancecocktail.com] ได้รับเงินโอนแล้ว‏ #'.$order->orderNumber);
    		});
        		
      }
      
      #update order status if it complete ; 
      
    }else{
      $errors = $listener->getErrors();
      file_put_contents(storage_path('/logs/ipn_error-'.date('Y-m-d').'.log'), print_r($errors, true) . PHP_EOL, LOCK_EX | FILE_APPEND);
    }
    
    die();
  }
  
  private function processUsePoint($order){
    if($order->used_point > 0){
      $historicalPoint = new HistoricalPoint() ; 
      $historicalPoint->order_id = $order->id ; 
      $historicalPoint->point = -1 * $order->used_point ; 
      $historicalPoint->user_id = $order->user->id ; 
      $historicalPoint->remark = "ใช้แต้มสะสมจากการซื้อของ" ; 
      $historicalPoint->save() ; 
      
      $user = $order->user ; 
      $user->point = $user->point - $order->used_point ; 
      $user->updateUniques() ; 
    }
  }
}