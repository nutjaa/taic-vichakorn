<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTermDataTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('term_datas', function(Blueprint $table)
		{
			$table->increments('id');
			$table->timestamps();
			
			$table->integer('vid');
            $table->string('label',512);
            $table->string('name',512);
            $table->string('description',512);
            $table->integer('weight');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('term_datas');
	}

}
