@extends('layouts.master')

@section('content') 
	<div class="row">
		<div class="col-md-12">
			<br /><br />
			@if( is_null($user))
				<div class="alert alert-error">
					<p class="text-center">
						<strong>เกิดข้อผิดพลาด</strong><br/>
						Token ที่คุณใช้ไม่พบในระบบ กรุณาตรวจสอบอีกครั้งค่ะ
					</p>
				</div>
			@else
				<div class="alert alert-success">
					<p class="text-center">
						<strong>คุณได้ทำการยืนยันอีเมล์เรียบร้อยแล้ว</strong><br/>
						สามารถใช้บัญชีดังกล่าวล๊อกอินเข้าสู่ระบบ ได้เลยค่ะ 
					</p>
				</div>
			@endif
		</div>
	</div>
@endsection