<?php namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;

use App\Order ; 
use App\User ;
use App\Post ; 

class IndexController extends Controller {
	/**
	 * Create a new controller instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		$this->middleware('auth');
	}
	
	public function index()
    {
        $orders = Order::orderBy('updated_at','DESC')->paginate(8)  ;  
        $users = User::orderBy('updated_at','DESC')->paginate(8);
        $posts = Post::orderBy('updated_at','DESC')->paginate(8);
        return   view('admin.home')->with('orders',$orders)->with('users',$users)->with('posts',$posts) ; 
    }
}