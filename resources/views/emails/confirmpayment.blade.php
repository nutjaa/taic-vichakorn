@extends('layouts.email') 

@section('content') 

<table width="100%" bgcolor="#e8eaed" cellpadding="0" cellspacing="0" border="0" id="backgroundTable">
   <tbody>
      <tr>
         <td>
            <table width="600" cellpadding="0" cellspacing="0" border="0" align="center" class="devicewidth">
               <tbody>
                  <tr>
                     <td width="100%">
                        <table bgcolor="#ffffff" width="600" cellpadding="0" cellspacing="0" border="0" align="center" class="devicewidth">
                           <tbody>
                              <!-- Spacing -->
                              <tr>
                                 <td width="100%" height="20"></td>
                              </tr>
                              <!-- Spacing -->
                              <tr>
                                 <td>
                                    <table width="560" align="center" cellpadding="0" cellspacing="0" border="0" class="devicewidthinner">
                                       <tbody>
                                          <!-- Title -->
                                          <tr><td align="left" style="font-family: Helvetica, arial, sans-serif; font-size: 14px;">สวัสดีค่ะ คุณ<strong style="color:#FFA70C;">{{$order->fullName}}</strong></tr>
                                          <tr>
                                             <td height="15"></td>
                                          </tr> 
                                          <tr>
                             				<td align="left" style="background-color:#271E15;color:#FFF;font-size:13px;font-weight:bold;padding:0.5em 1em;">รายละเอียดการสั่งซื้อ #{{$order->orderNumber}}</td>
										  </tr>
                                          <tr>
                                             <td height="15"></td>
                                          </tr>  
										  <tr>
                                             <td height="15"></td>
                                          </tr> 
										  <tr>
											<td align="left" style="font-family: Helvetica, arial, sans-serif; font-size: 14px;">
												 เราได้รับการชำระเงินของคุณแล้วค่ะ สินค้าสำเร็จรูปจะถูกจัดส่งให้ภายใน 1-2วันทำการ 
											</td>
										  </tr> 
										  <tr>
                                             <td height="15"></td>
                                          </tr> 
										  <tr>
                                             <td height="15"></td>
                                          </tr> 
                                          <tr>
											<td align="left" style="font-family: Helvetica, arial, sans-serif; font-size: 14px;">
												คุณสามารถตรวจสอบรายการสั่งซื้อ ได้ที่ <a target="_blank" style="color:#FFA70C;font-weight:bold;text-decoration:none;" href="{{Config::get('app.url')}}orders/{{$order->id}}">"Order history"</a> บน Website
											</td>
										  </tr>
                                       </tbody>
                                    </table>
                                 </td>
                              </tr>
                              <tr>
                                 <td width="100%" height="20"></td>
                              </tr>
                           </tbody>
                        </table>
                     </td>
                  </tr>
               </tbody>
            </table>
         </td>
      </tr>
   </tbody>
</table>
@endsection