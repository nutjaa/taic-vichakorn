<?php namespace App;

use LaravelBook\Ardent\Ardent;

class OrderItem extends Ardent{
    protected $table = 'order_items'; 
    
    public static $relationsData = array(  
        'order' => array(self::BELONGS_TO, 'App\Order', 'foreignKey' => 'order_id') ,  
        'product' => array(self::BELONGS_TO, 'App\Product', 'foreignKey' => 'product_id') ,  
    ) ; 
    
    /*--- Attribute --- */
    public function getTotalAttribute(){
    	return number_format( $this->price * $this->quantity , 2) ; 
    }
}