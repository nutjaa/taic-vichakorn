<?php

return [

	/*
	|--------------------------------------------------------------------------
	| Password Reminder Language Lines
	|--------------------------------------------------------------------------
	|
	| The following language lines are the default lines which match reasons
	| that are given by the password broker for a password update attempt
	| has failed, such as for an invalid token or invalid new password.
	|
	*/

	"password" => "รหัสผ่านจะต้องมีอักษรอย่างน้อย 6 ตัวอักษร และ สอดคล้องกับค่าจากช่องยืนยันรหัสผ่าน",
	"user" => "เราไม่พบผู้ใช้งานตาม อีเมล์ที่ได้ระบุ",
	"token" => "Token ไม่ถูกต้อง",
	"sent" => "ทางเราได้ส่ง ลิงค์สำหรับแก้ไขรหัสผ่านไปยังอีเมล์ของท่าน",
	"reset" => "รหัสผ่านของท่านได้ทำการแก้ไขเรียบร้อย!",

];
