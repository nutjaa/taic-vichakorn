<?php namespace App;

use LaravelBook\Ardent\Ardent;

class Vocabulary extends Ardent {

    protected $table = 'vocabularies';
    protected $fillable = array('name', 'description' );
    
    public function scopeSearch($query,$q){
    	if(trim($q) == '')
    		return $query ;   
   		return $query->where(function($query) use ($q){ 
   			$query->where('name', 'LIKE' , '%'.$q.'%')
   				->orWhere('description', 'LIKE' , '%'.$q.'%')  ; 
   		}) ; 
    }
    
    /*---- Scopre ----*/
    public function scopeName($query,$name){
        return $query->where('name','=',$name);
    }
    
    #fixed
    public function scopeIngradient($query){
        return $query->where('name','=','ingradient');
    }
    public function scopePage($query){
        return $query->where('name','=','page') ; 
    } 
    public function scopeProduct($query){
    	return $query->where('name','=','product');
    }
    
    /*---- Attribute ----*/
}