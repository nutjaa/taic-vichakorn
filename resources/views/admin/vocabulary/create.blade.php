@extends('layouts.admin')
@section('content') 
<div class="breadcrumbs">
    <ol class="breadcrumb">
        <li><a href="/"><i class="fa fa-home"></i>Home</a></li>
        <li><a href="/admin">Admin</a></li>
        <li><a href="/vocabulary">Vocabulary</a></li>
        <li class="active">Create new vocabulary</li>
    </ol>
</div>

<div class="innerLR">
    <div class="row manage-header">
        <div class="col-lg-7">
        	<h2>Create new vocabulary</h2>
        </div>
 	</div>
    <div class="widget">
        {!! Notification::showAll() !!}
 	
     	{!! Form::open(array('url' => 'vocabulary' , 'class'=>'form-horizontal' , 'role' => 'form' )) !!}
     		<div class="form-group">
    			{!! Form::label('name', 'Name' , array('class'=>'col-sm-2 control-label')) !!}
    			<div class="col-sm-8">
    				{!! Form::text('name', Input::old('name'), array('class' => 'form-control')) !!}
    			</div>
    		</div>
    		<div class="form-group">
    			{!! Form::label('description', 'Description' , array('class'=>'col-sm-2 control-label')) !!}
    			<div class="col-sm-8">
    				{!! Form::textarea('description', Input::old('description'), array('class' => 'form-control')) !!}
    			</div>
    		</div> 
    		<div class="form-group">
    		    <div class="col-sm-offset-2 col-sm-8">
    				{!! Form::submit('Save', array('class' => 'btn btn-primary')) !!}
    				<a href="{{ URL::to('vocabulary/') }}">&nbsp;Cancel</a>
    			</div>
    		</div>
     	{!! Form::close() !!}
    </div>
</div>

@stop