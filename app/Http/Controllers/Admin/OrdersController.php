<?php namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Order;
use App\Termdata; 
use App\Vocabulary;
use App\OrderStatus;
use App\HistoricalPoint ;

use Krucas\Notification\Facades\Notification;
use Mail ;

class OrdersController extends Controller {
	public function __construct()
	{
		$this->middleware('auth');
	}
    
  public function index(Request $requests){ 
		$q = $requests->input('q');
		$s = $requests->input('s');
		
		
		$vocab = Vocabulary::Name('order')->first() ;  
    $options = Termdata::NameValueOption($vocab->id  ) ; 
    $options = array_merge(['all'=>'All'],$options);
 
		  
		$orders = Order::hasStatus($s)->search($q)->orderBy('updated_at','DESC')->paginate(15)  ;  
    return view('admin.order.index')->with('orders' , $orders)->with('q',$q)->with('s',$s)->with('statusOptions',$options) ; 
	}
    
  public function show(Request $requests , $id){
    $order = Order::find($id) ;
    if(is_null($order)){
      Notification::error('Order not found.');
      return redirect('orders') ; 
    }
    return view('admin.order.show')->with('order',$order);
  }
    
    public function changestatus(Request $requests , $id){
        $order = Order::find($id) ;
        $status = $requests->input('status') ; 
        
        $vocab = Vocabulary::Name('order')->first() ; 
        $term = Termdata::Name($status)->Vid($vocab->id)->first() ; 
        
        if(is_null($term)){
            Notification::error('Status not found.');
            return redirect('orders/'.$id);
        }
        
        switch ($status){ 
        	case 'canceled':
                if(!$order->canCanceled){
                    Notification::error('Fail cannot cancel this order.');
                    return redirect('orders/'.$id);
                }
                $status = new OrderStatus() ; 
                $status->status_id = $term->id ; 
                $status->description = $requests->input('description');
                $status->order_id = $order->id ; 
                $status->save() ; 
                
                $order->status_id = $term->id ; 
                $order->save() ; 
                
                #send cancel mail
                
                Notification::success('Success cancel order.');
                return  redirect('orders/'.$id);
        	break;
            case 'confirm' :
                if(!$order->canConfirm){
                    Notification::error('Fail cannot confirm this order.');
                    return redirect('orders/'.$id);
                }
                $status = new OrderStatus() ; 
                $status->status_id = $term->id ; 
                $status->description = $requests->input('description');
                $status->order_id = $order->id ; 
                $status->save() ; 
                
                $order->status_id = $term->id ; 
                $order->save() ; 
                
                Mail::send('emails.confirmpayment', ['order' => $order ], function($message)use ($order)
        		{ 
        		    $message->to($order->email,$order->fullname)->subject('[luminancecocktail.com] ได้รับเงินโอนแล้ว‏ #'.$order->orderNumber);
        		});
                
                Notification::success('Success confirm order.');
                return  redirect('orders/'.$id);
            break ;
            
            case 'shipping' : 
                $this->validate($requests, [
        			'ship_no' => 'required' ,
        		]);
            
                if(!$order->canShipping && !$order->isShipping){
                    Notification::error('Fail cannot shipping this order.');
                    return redirect('orders/'.$id);
                }
                
                $status = new OrderStatus() ; 
                $status->status_id = $term->id ; 
                $status->description = $requests->input('description');
                $status->order_id = $order->id ; 
                $status->save() ; 
                
                $order->status_id = $term->id ; 
                $order->ship_no = $requests->input('ship_no') ; 
                $order->save() ; 
                
                $this->processGotPoint($order) ; 
                
                Mail::send('emails.confirmshipping', ['order' => $order ], function($message)use ($order)
        		{ 
        		    $message->to($order->email,$order->fullname)->subject('[luminancecocktail.com] จัดส่งสินค้าแล้ว‏ #'.$order->orderNumber);
        		});
                
                Notification::success('Success ship order.');
                return  redirect('orders/'.$id);
            break ; 
          
        	default :
        }
        
         
    }
    
  public function processGotPoint($order){ 
    if( $order->got_point == 0){
      $order->got_point = $order->currentPoint ; 
      $order->save() ; 
      
      $historicalPoint = new HistoricalPoint() ; 
      $historicalPoint->order_id = $order->id ; 
      $historicalPoint->user_id = $order->user->id ; 
      $historicalPoint->point = $order->got_point ; 
      $historicalPoint->remark = "ได้รับแต้มสะสมจากการซื้อของ" ; 
      $historicalPoint->save() ; 
      
      $user = $order->user ; 
      $user->point = $user->point + $order->got_point ; 
      $user->updateUniques() ; 
    }
  }
}