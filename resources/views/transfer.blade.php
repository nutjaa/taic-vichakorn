@extends('layouts.master')

@section('content')
<div class="row">
    <ol class="breadcrumb">
      <li><a href="/">Home</a></li>  
      <li class="active">แจ้งโอนเงิน </li>
    </ol>
</div>
<div ng-app="vichakorn"><div ng-controller="vichakornController"  > 
@if(!Auth::guest())
<div ng-init='orders={!!$orders->toJson();!!}'></div> 
@endif
<div class="row">
	<div class="col-md-12">
		<h2 class="text-center">แจ้งโอนเงิน สำหรับท่านที่ชำระเงินโดยการโอนเงินเข้าบัญชีธนาคารของลูมิแนนซ์ ค็อกเทลค่ะ</h2>
		@if(Auth::guest())
		<div class="row">
			<br />
			<br />
			<div class="col-md-4 col-md-offset-4"><a href="/auth/login?redirectTo=/moneytransfer" class="btn btn-primary form-control">กรุณา Login เพื่อทำรายการ</a></div>
		</div>
        @elseif( count($orderList) == 0 )
        <div class="row">
			<br />
			<br />
            <div class="col-md-12 text-center">
                <h3>ไม่พบข้อมูลการสั่งซื้อ</h3>
            </div>
        </div>
		@else
		<br />
		{!! Notification::showAll() !!} 
		<form class="form-horizontal" role="form"  method="POST" action="{{ url('/moneytransfer') }}" id="transfer-form">
			<input type="hidden" name="_token" value="{{ csrf_token() }}">
			<div class="form-group">
				<label class="col-md-3 control-label">หมายเลขการสั่งซื้อ </label>
				<div class="col-md-6"> 
					{!! Form::select('order_id', $orderList,Input::old('order_id'), array('class' => 'form-control' , 'ng-model' => 'order_id' , 'ng-change' => 'order_changed()' , 'id' => 'order_id', 'placeholder' => 'กรุณาเลือกหมายเลขสั่งซื้อสินค้า')) !!}
				</div>  
			</div>
			<div class="form-group">
				<label class="col-md-3 control-label">ชื่อ</label>
				<div class="col-md-6">
					<input type="text" class="form-control" required  name="firstname" ng-model="order.firstname" >
				</div> 
				<div class="col-md-3">
					<label for="firstname" class="error inline"></label>
				</div>
			</div>
			<div class="form-group">
				<label class="col-md-3 control-label">นามสกุล</label>
				<div class="col-md-6">
					<input type="text" class="form-control" required  name="lastname" ng-model="order.lastname" >
				</div> 
				<div class="col-md-3">
					<label for="lastname" class="error inline"></label>
				</div>
			</div>
			<div class="form-group">
				<label class="col-md-3 control-label">ที่อยู่จัดส่ง</label>
				<div class="col-md-6">
					<textarea class="form-control" required name="address" ng-model="order.address"></textarea>
				</div> 
				<div class="col-md-3">
					<label for="address" class="error inline"></label>
				</div>
			</div>
			<div class="form-group">
				<label class="col-md-3 control-label">อีเมล์</label>
				<div class="col-md-6">
					<input type="email" class="form-control" required  name="email" ng-model="order.email">
				</div> 
				<div class="col-md-3">
					<label for="email" class="error inline"></label>
				</div>
			</div>
			<div class="form-group">
				<label class="col-md-3 control-label">โทรศัพท์</label>
				<div class="col-md-6">
					<input type="text" class="form-control"  required name="phone" ng-model="order.phone">
				</div> 
				<div class="col-md-3">
					<label for="phone" class="error inline"></label>
				</div>
			</div>
			<div class="form-group">
				<label class="col-md-3 control-label">จำนวนเงิน</label>
				<div class="col-md-2">
					<input type="text" class="form-control"  required name="amount" id="amount" value="{{Input::old('amount')}}">
				</div> 
				<div class="col-md-3">
					<label for="amount" class="error inline"></label>
				</div>
			</div> 
			<div class="form-group">
				<label class="col-md-3 control-label">โอนจากธนาคาร</label>
				<div class="col-md-6">
					<label class="radio-inline">
						<input type="radio"  name="from_bank" value="bbl" required  />กรุงเทพ
					</label> 
					<label class="radio-inline">
						<input type="radio"  name="from_bank" value="krungsri" />กรุงศรี
					</label>
					<label class="radio-inline">
						<input type="radio"  name="from_bank" value="ktb" />กรุงไทย
					</label>
					<label class="radio-inline">
						<input type="radio"  name="from_bank" value="scb" />ไทยพาณิชย์
					</label>
					<label class="radio-inline">
						<input type="radio"  name="from_bank" value="kbank" />กสิกร
					</label>
				</div> 
				<div class="col-md-3">
					<label for="from_bank" class="error inline"></label>
				</div>
			</div>
			<div class="form-group">
				<label class="col-md-3 control-label">เข้าบัญชีลูมิแนนซ์ ค็อกเทล ธนาคาร</label>
				<div class="col-md-6">
					<label class="radio-inline">
						<input type="radio"  name="to_bank" value="bbl"  required />กรุงเทพ
					</label> 
					<label class="radio-inline">
						<input type="radio"  name="to_bank" value="krungsri" />กรุงศรี
					</label>
					<label class="radio-inline">
						<input type="radio"  name="to_bank" value="ktb" />กรุงไทย
					</label>
					<label class="radio-inline">
						<input type="radio"  name="to_bank" value="scb" />ไทยพาณิชย์
					</label>
					<label class="radio-inline">
						<input type="radio"  name="to_bank" value="kbank" />กสิกร
					</label>
				</div>
				<div class="col-md-3">
					<label for="to_bank" class="error inline"></label>
				</div> 
			</div>
			<div class="form-group">
				<label class="col-md-3 control-label">วันที่ทำการโอน </label>
				<div class="col-md-3"> 
	                <div class='input-group date' id='transferdate'>
	                    <input type='text' class="form-control" name="transferdate" required value="{{ Input::old('transferdate') }}" />
	                    <span class="input-group-addon">
	                        <span class="glyphicon glyphicon-calendar"></span>
	                    </span>
	                </div> 
				</div>
				<div class="col-md-3">
					<label for="transferdate" class="error inline"></label>
				</div> 
			</div>
			<div class="form-group">
				<label class="col-md-3 control-label">เวลาที่ทำการโอน </label>
				<div class="col-md-3"> 
	                <div class='input-group date' id='transfertime'>
	                    <input type='text' class="form-control" name="transfertime" required value="{{ Input::old('transfertime') }}" />
	                    <span class="input-group-addon">
	                        <span class="glyphicon glyphicon-time"></span>
	                    </span>
	                </div> 
				</div>
				<div class="col-md-3">
					<label for="transfertime" class="error inline"></label>
				</div> 
			</div>
			<!--
			<div class="form-group">
				<label class="col-md-3 control-label">วันที่ทำการโอน </label>
				<div class="col-md-2">
					<div class="row">
						<div class="col-md-4">
							<label class="control-label">วันที</label>่
						</div>
						<div class="col-md-8"> 
							{!! Form::select('day', $day_list,Input::old('day'), array('class' => 'form-control')) !!}
						</div>
					</div> 
				</div>
				<div class="col-md-2">
					<div class="row">
						<div class="col-md-4">
							<label class="control-label">เดือน</label>่
						</div>
						<div class="col-md-8">
							{!! Form::select('month', $month_list,Input::old('month'), array('class' => 'form-control')) !!}
						</div>
					</div> 
				</div>
				<div class="col-md-2">
					<div class="row">
						<div class="col-md-2">
							<label class="control-label">ปี</label>่
						</div>
						<div class="col-md-8">
							{!! Form::select('year', $year_list,Input::old('year'), array('class' => 'form-control')) !!}
						</div>
					</div> 
				</div>
			</div>
			<div class="form-group">
				<label class="col-md-3 control-label">เวลาที่ทำการโอน   </label>
				<div class="col-md-2">
					<div class="row"> 
						<div class="col-xs-10"> 
							{!! Form::select('hour', $hour_list,Input::old('hour'), array('class' => 'form-control')) !!}
						</div>
						<div class="col-xs-2">
							<label class="control-label">.</label>่
						</div>
					</div> 
				</div>
				<div class="col-md-2">
					<div class="row"> 
						<div class="col-xs-10"> 
							{!! Form::select('minute', $minute_list ,Input::old('minute'), array('class' => 'form-control')) !!}
						</div>
						<div class="col-xs-2">
							<label class="control-label">น.</label>่
						</div>
					</div>  
				</div>  
			</div>
			-->
			<div class="form-group">
				<label class="col-md-3 control-label">หมายเหตุ</label>
				<div class="col-md-7">
					<textarea class="form-control" name="remark">{{Input::old('remark')}}</textarea>
				</div> 
			</div>
			<div class="form-group">
				<label class="col-md-4 col-md-offset-3 control-label">กรุณาตรวจสอบข้อมูล ก่อนกดยืนยันการแจ้งโอนเงินค่ะ</label>
			</div>
			<div class="form-group">
				<div class="col-md-4 col-md-offset-3">
					<input type="submit" class="form-control btn btn-primary" value="ยืนยันการแจ้งโอนเงิน" />
				</div>
			</div>
		</form>
		@endif
	</div>
</div>
</div></div>
@stop

@section('headerjs')
    <link href="/css/bootstrap-datetimepicker.css" rel="stylesheet" type="text/css" /> 
    <script type="text/javascript" src="/js/angular.min.js"></script>
@stop

@section('footerjs')
	
	<script type="text/javascript" src="/js/jquery.validate.min.js"></script>
	<script type="text/javascript" src="/js/messages_th.js"></script>
    <script type="text/javascript" src="/js/moment-with-locales.js"></script>
    <script type="text/javascript" src="/js/bootstrap-datetimepicker.js"></script>
    <script type="text/javascript" src="/js/page/order.transferconfirm.js"></script>
@stop