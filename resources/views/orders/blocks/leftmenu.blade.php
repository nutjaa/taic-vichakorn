<aside class="widget">
    <div class="widget-inner">
        <h3 class="widget-title" style="background-position: 0px -11px;">
            <span>ข้อมูลส่วนตัว</span>
        </h3>
        <img class="img-responsive img-rounded" src="/images/{{ Auth::user()->avatarpath() }}" />
        <ul class="left-menu list-unstyled">
            <li><a href="/home">ข้อมูลส่วนตัว</a></li>
            <li class="active"><a href="/orders">ประวัติการสั่งซื้อ</a></li>
            <li><a href="/historical-point">ประวัติแต้มสะสม</a></li>
        </ul>
    </div>
</aside>