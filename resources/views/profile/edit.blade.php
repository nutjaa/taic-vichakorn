@extends('layouts.master')

@section('content')
<div class="row">
	<ol class="breadcrumb">
	  <li><a href="/">Home</a></li> 
      <li><a href="/home">ข้อมูลส่วนตัว</a></li> 
	  <li class="active">แก้ไข</li>
	</ol>
</div>
<div class="row">
    <div class="col-md-2">
        <aside class="widget">
            <div class="widget-inner">
                <h3 class="widget-title" style="background-position: 0px -11px;">
                    <span>ข้อมูลส่วนตัว</span>
                </h3>
                <img class="img-responsive img-rounded" src="/images/{{ Auth::user()->avatarpath() }}" />
                <ul class="left-menu list-unstyled">
                    <li><a href="/home">ข้อมูลส่วนตัว</a></li>
                    <li><a href="/orders">ประวัติการสั่งซื้อ</a></li>
                    <li><a href="/historical-point">ประวัติแต้มสะสม</a></li>
                </ul>
            </div>
        </aside>
    </div>
    <div class="col-md-10">
        <h2>แก้ไขข้อมูล</h2>
        {!! Notification::showAll() !!} 
        <form class="form-horizontal" role="form" method="POST" action="{{ url('/profile/edit') }}" accept-charset="UTF-8" enctype="multipart/form-data">
            <input type="hidden" name="_token" value="{{ csrf_token() }}"/>
            <div class="form-group">
				<label class="col-md-3 control-label">ชื่อผู้ใช้ที่จะปรากฏในเว็บนี้ <span class="require">*</span></label>
				<div class="col-md-6">
					<input type="text" class="form-control" name="username" value="{{ Auth::user()->username }}">
				</div>
			</div>
            <div class="form-group">
				<label class="col-md-3 control-label">ชื่อ <span class="require">*</span></label>
				<div class="col-md-6">
					<input type="text" class="form-control" name="name" value="{{ Auth::user()->name }}">
				</div>
			</div>
            <div class="form-group">
				<label class="col-md-3 control-label">นามสกุล <span class="require">*</span></label>
				<div class="col-md-6">
					<input type="text" class="form-control" name="lastname" value="{{ Auth::user()->lastname }}">
				</div>
			</div>
            <div class="form-group">
				<label class="col-md-3 control-label">อีเมล์<span class="require">*</span></label>
				<div class="col-md-6">
					<input type="email" class="form-control" name="email" value="{{ Auth::user()->email }}">
				</div>
			</div>
            <div class="form-group">
				<label class="col-md-3 control-label">รหัสผ่าน</label>
				<div class="col-md-6">
					<input type="password" class="form-control" name="password" value="">
				</div>
			</div>
            <div class="form-group">
				<label class="col-md-3 control-label">ยืนยันรหัสผ่าน</label>
				<div class="col-md-6">
					<input type="password" class="form-control" name="repassword" value="">
				</div>
			</div>
            <div class="form-group">
				<label class="col-md-3 control-label">เพศ <span class="require">*</span></label>
				<div class="col-md-9">
					<label class="radio-inline">
						<input type="radio"  name="gender" value="M" required id="gender-m" @if(Auth::user()->gender == 'M') checked="" @endif/>ชาย
					</label> 
					<label class="radio-inline">
						<input type="radio"  name="gender" value="F" id="gender-f" @if(Auth::user()->gender == 'F') checked="" @endif/>หญิง
					</label> 
					<label class="radio-inline">
						<input type="radio"  name="gender" value="N" id="gender-n" @if(Auth::user()->gender == 'N') checked="" @endif/>ไม่ประสงค์จะระบุ
					</label>
					<label for="gender" class="error radio-inline"></label>
				</div>
			</div>
            <div class="form-group">
            	{!! Form::label('birthdate', 'วันเกิด' , array('class'=>'col-sm-3 control-label')) !!}
            	<div class="col-md-3"> 
                    <div class='input-group date' id='birthdate'>
                        <input type='text' class="form-control" name="birthdate" required value="{{ Auth::user()->birthDateFormatted }}" /> 
                        <span class="input-group-addon">
                            <span class="glyphicon glyphicon-calendar"></span>
                        </span>
                    </div> 
            	</div>
            </div>
            <div class="form-group">
				<label class="col-md-3 control-label">เบอร์โทร </label>
				<div class="col-md-6">
					<input type="text" class="form-control" name="phone" value="{{ Auth::user()->phone }}">
				</div>
			</div>
            <div class="form-group">
				<label class="col-md-3 control-label">ที่อยู่ <span class="require">*</span></label>
				<div class="col-md-6">
					<textarea required name="address" class="form-control">{{ Auth::user()->address }}</textarea>
				</div>
			</div>
            <div class="form-group">
				<label class="col-md-3 control-label">ไอดีไลน์ (line ID) </label>
				<div class="col-md-6">
					<input type="text" class="form-control" name="lineid" value="{{ Auth::user()->lineid }}">
				</div>
			</div>
            <div class="form-group">
				<label class="col-md-3 control-label">ชื่อที่ใช้ในเฟสบุ๊ค </label>
				<div class="col-md-6">
					<input type="text" class="form-control" name="facebookname" value="{{ Auth::user()->facebookname }}">
				</div>
			</div>
            <div class="form-group">
				<label class="col-md-3 control-label">รับข่าวสารทางอีเมล์หรือไม่<span class="require">*</span></label>
				<div class="col-md-6">
					<label class="radio-inline">
						<input type="radio"  name="is_subscribe" value="1" required @if(Auth::user()->is_subscribe) checked="" @endif/>ต้องการ
					</label> 
					<label class="radio-inline">
						<input type="radio"  name="is_subscribe" value="0" @if(! Auth::user()->is_subscribe) checked="" @endif/>ไม่ต้องการ
					</label>
					<label for="is_subscribe" class="error radio-inline"></label>
				</div> 
			</div>
      <div class="form-group">
      	{!! Form::label('avatar', 'Avatar' , array('class'=>'col-sm-3 control-label')) !!}
      	<div class="col-sm-8">
      		<div style="position:relative;">
      	        <a class='btn btn-primary' href='javascript:;'>
      	            Choose File...
      	            <input type="file" style='position:absolute;z-index:2;top:0;left:0;filter: alpha(opacity=0);-ms-filter:"progid:DXImageTransform.Microsoft.Alpha(Opacity=0)";opacity:0;background-color:transparent;color:transparent;' name="avatar" size="40"  onchange='$("#upload-file-info").html($(this).val());'>
      	        </a>
      	        &nbsp;
      	        <span class='label label-info' id="upload-file-info"></span>
      		</div> 
      	</div> 
      </div>
            <div class="form-group">
                <div class="col-sm-offset-3 col-sm-8">
            		{!! Form::submit('บันทึกข้อมูล', array('class' => 'btn btn-primary')) !!}
            		<a href="{{ URL::to('home') }}">&nbsp;ยกเลิก</a>
            	</div>
            </div>
        </form>
    </div>
</div>
@stop

@section('headerjs')
    <link href="/css/bootstrap-datetimepicker.css" rel="stylesheet" type="text/css" />  
@stop

@section('footerjs')
    <script type="text/javascript" src="/js/moment-with-locales.js"></script>
    <script type="text/javascript" src="/js/bootstrap-datetimepicker.js"></script>
    <script type="text/javascript" src="/js/page/admin/user.form.js"></script>
@stop