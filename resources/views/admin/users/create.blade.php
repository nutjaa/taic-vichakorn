@extends('layouts.admin')
@section('content') 
<div class="breadcrumbs">
    <ol class="breadcrumb">
        <li><a href="/"><i class="fa fa-home"></i>Home</a></li>
        <li><a href="/admin">Admin</a></li>
        <li><a href="/users">Users</a></li>
        <li class="active">Create new user</li>
    </ol>
</div>

<div class="innerLR">
    <div class="row manage-header">
        <div class="col-lg-7">
        	<h2>Create new user</h2>
        </div>
 	</div>
 	{!! Notification::showAll() !!}
    <div class="widget">
     {!! Form::open(array('url' => 'users' , 'class'=>'form-horizontal' , 'role' => 'form'  , 'files'=>true )) !!}
        @include('admin/users/form')  
    {!! Form::close() !!}
    </div>
</div>

@stop