<?php namespace App\Http\Controllers;

use App\Page;
use App\Pagecategory;
use App\Vocabulary ; 
use App\Termdata ; 

use SEOMeta;
use OpenGraph;

class TricksController extends Controller { 
 	public $vocab  ; 
 	public $termdata;
 	
 	public function __construct()
	{
		$this->vocab = Vocabulary::Page()->first() ; 
		$this->termdata = Termdata::where('name' , '=' , 'tricks')->where('vid' , '=' , $this->vocab->id )->first() ; 
	 
	}

	/**
	 * Show the application dashboard to the user.
	 *
	 * @return Response
	 */
	public function index()
	{ 
 
		$page = Page::join('page_categories',function($join){
			$join->on('page_categories.page_id','=','pages.id') ; 
		})->where('page_categories.term_id','=',$this->termdata->id)->active()->orderBy('pages.order', 'ASC')->first()  ; 
    
    if(! is_object($page)){
      return Redirect('/') ; 
    }
 
		return redirect('/tricks-and-data-insight/'.$page->id.'/'.$page->sefu) ;
	}
	
	public function view($id , $sefu){
		$page = Page::active()->find($id)  ; 
		if(! is_object($page)){
			return Redirect('/tricks-and-data-insight') ; 
		}
    
    SEOMeta::setTitle('มหัศจรรย์แห่งการใช้ผลิตภัณฑ์ Luminance Cocktail ครบเซ็ต');
    SEOMeta::setDescription($page->title);
    
    OpenGraph::setDescription($page->title);
    OpenGraph::setTitle('มหัศจรรย์แห่งการใช้ผลิตภัณฑ์ Luminance Cocktail ครบเซ็ต');
    OpenGraph::setUrl(url('/tricks-and-data-insight/'.$page->id.'/'.$sefu)); 
    
    
		$pages = Page::join('page_categories',function($join){
			$join->on('page_categories.page_id','=','pages.id') ; 
		})->where('page_categories.term_id','=',$this->termdata->id)->active()->orderBy('pages.order', 'ASC')->get()  ;  
		return view('tricks')->with('page',$page)->with('pages',$pages) ; 
	}

}
