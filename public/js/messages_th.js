/*
 * Translated default messages for the jQuery validation plugin.
 * Locale: EN (Englisg; eng)
 */
(function($) {
	$.extend($.validator.messages, {
		required: "กรุณาระบุข้อมูลช่องนี้",
		maxlength: $.validator.format("Please enter no more than {0} characters."),
		minlength: $.validator.format("กรุณาระบุขั้นต่ำ {0} ตัวอักษร"),
		rangelength: $.validator.format("Please enter at least {0} and a maximum of {1} characters."),
		email: "กรุณาระบุอีเมล์ให้ถูกต้อง",
		url: "Please enter a valid URL.",
		date: "Please enter a valid date.",
		number: "กรุณาระบุตัวเลข",
		digits: "Enter only digits.",
		equalTo: "กรุณาระบุรหัสให้ตรงกัน",
		range: $.validator.format("Please enter a value between {0} and {1}."),
		max: $.validator.format("Enter a value no more than {0}."),
		min: $.validator.format("Enter a value that is at least {0}."),
		creditcard: "Please enter a valid credit card number."
	});
}(jQuery));
