angular.module('vichakorn', []).controller('vichakornController', function($scope) {  
  $scope.total = 0 ; 
  
  $scope.discount = 0 ; 
  
  $scope.$watch('products', function() {
    var cartTotal = 0;

    $scope.products.forEach(function(product) {
      if(  product.selected_product)
        cartTotal += product.sale_price * product.selected_quantity ;
    });
    
    if($scope.is_vip){
      $scope.discount = cartTotal * 0.1 ; 
    }

    $scope.total = cartTotal - $scope.discount;
  }, true);
});
