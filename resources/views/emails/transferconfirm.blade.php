@extends('layouts.email') 

@section('content') 

<table width="100%" bgcolor="#e8eaed" cellpadding="0" cellspacing="0" border="0" id="backgroundTable">
   <tbody>
      <tr>
         <td>
            <table width="600" cellpadding="0" cellspacing="0" border="0" align="center" class="devicewidth">
               <tbody>
                  <tr>
                     <td width="100%">
                        <table bgcolor="#ffffff" width="600" cellpadding="0" cellspacing="0" border="0" align="center" class="devicewidth">
                           <tbody>
                              <!-- Spacing -->
                              <tr>
                                 <td width="100%" height="20"></td>
                              </tr>
                              <!-- Spacing -->
                              <tr>
                                 <td>
                                    <table width="560" align="center" cellpadding="0" cellspacing="0" border="0" class="devicewidthinner">
                                       <tbody>
                                          <!-- Title -->
                                          <tr><td align="left" style="font-family: Helvetica, arial, sans-serif; font-size: 14px;">ระบบได้รับการแจ้งโอนเงิน สำหรับ order หมายเลข {{$order_confirmtransfer->order->orderNumber}}</tr>
                                          <tr>
                                             <td height="15"></td>
                                          </tr> 
                                          <tr>
                             				<td align="left" style="background-color:#271E15;color:#FFF;font-size:13px;font-weight:bold;padding:0.5em 1em;">รายละเอียด</td>
										  </tr>
										  <tr>
                                             <td height="15"></td>
                                          </tr> 
										  <tr>
											<td align="left" style="font-family: Helvetica, arial, sans-serif; font-size: 14px;">
												โอนโดย: <strong>{{$order_confirmtransfer->method}}</strong>
												<br>โอนจากธนาคาร: <strong>{{$order_confirmtransfer->frombank}}</strong>
												<br>เข้าบัญชีลูมิแนนซ์ ค็อกเทล ธนาคาร: <strong>{{$order_confirmtransfer->tobank}}</strong>
												<br>จำนวนเงิน: <strong>{{$order_confirmtransfer->amount}}</strong>
												<br/>วันที่ทำการโอน: <strong>{{$order_confirmtransfer->transferedAtFormat}}</strong>
												<br/>หมายเหตุ: <strong>{!!nl2br($order_confirmtransfer->remark)!!}</strong>
											</td>
										  </tr> 
										  <tr>
                                             <td height="15"></td>
                                          </tr> 
										  <tr>
                                             <td height="15"></td>
                                          </tr> 
                                          <tr>
											<td align="left" style="font-family: Helvetica, arial, sans-serif; font-size: 14px;">
												ตรวจสอบ order ได้ที่ <a target="_blank" style="color:#FFA70C;font-weight:bold;text-decoration:none;" href="{{Config::get('app.admin_url')}}orders/{{$order_confirmtransfer->order_id}}">"Order Edit"</a> บน Website
											</td>
										  </tr>
                                       </tbody>
                                    </table>
                                 </td>
                              </tr>
                              <tr>
                                 <td width="100%" height="20"></td>
                              </tr>
                           </tbody>
                        </table>
                     </td>
                  </tr>
               </tbody>
            </table>
         </td>
      </tr>
   </tbody>
</table>
@endsection