<?php namespace App\Http\Controllers;

use App\Product; 
use Krucas\Notification\Facades\Notification;

use SEOMeta;
use OpenGraph;

class ProductsController extends Controller {
	
	public function index(){  
    SEOMeta::setTitle('รายละเอียดผลิตภัณฑ์');
    SEOMeta::setDescription('รายละเอียดผลิตภัณฑ์');
    
    OpenGraph::setDescription('รายละเอียดผลิตภัณฑ์');
    OpenGraph::setTitle('รายละเอียดผลิตภัณฑ์');
    OpenGraph::setUrl(url('/products'));   
    
		$products = Product::active()->category('none')->orderBy('weight','ASC')->get() ;
		return view('products.list')->with('products',$products) ; 
	}
	
	public function view($sefu){
		$products = Product::active()->category('none')->orderBy('weight','ASC')->get() ;
		$product = Product::active()->sefu($sefu)->first() ; 
    
    SEOMeta::setTitle($product->name);
    #SEOMeta::setDescription(substr($product->description,0,300)); 
    #OpenGraph::setDescription(substr($product->description,0,300));
    OpenGraph::setTitle($product->name);
    
    
		return view('products.view')->with('products',$products)->with('product',$product) ; 
	}
 
}