<?php namespace App;

use LaravelBook\Ardent\Ardent;
use Carbon ; 

class OrderConfirmtransfer extends Ardent{
    protected $table = 'order_confirmtransfers'; 
    
    public static $relationsData = array(  
        'order' => array(self::BELONGS_TO, 'App\Order', 'foreignKey' => 'order_id') ,  
    ) ; 
    
    public static $rules = array(
    	'order_id' => 'required|exists:orders,id', 
    	'frombank' => 'required' , 
    	'tobank' => 'required' , 
    	'amount' => 'required|numeric' , 
    	'transfered_at' => 'required' , 
	);
	
	/*---- Attribute ---- */
	
	public function getTransferedAtFormatAttribute(){
		$date = Carbon::createFromFormat('Y-m-d H:i:s', $this->	transfered_at );
		$date->setTimezone('Asia/Bangkok');
		return $date->format('d/m/Y H:i');
	}
	
}