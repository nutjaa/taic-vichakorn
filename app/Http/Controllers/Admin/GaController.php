<?php namespace App\Http\Controllers\Admin;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use LaravelAnalytics;

class GaController extends Controller { 
  public function getInfo(){
    $lastWeekPageViews = LaravelAnalytics::getVisitorsAndPageViews(7);
    $mostVisitPages = LaravelAnalytics::getMostVisitedPages() ;  
    $topKeyword = LaravelAnalytics::getTopKeywords() ;  
    $activeUsers = LaravelAnalytics::getActiveUsers() ;  
    $topReferrers = LaravelAnalytics::getTopReferrers() ; 
    return   view('admin.ga.info')->with('lastWeekPageViews',$lastWeekPageViews)->with('mostVisitPages' , $mostVisitPages )->with('topKeyword',$topKeyword)->with('activeUsers' , $activeUsers )->with('topReferrers' , $topReferrers ) ; 
  }
}