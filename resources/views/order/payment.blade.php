@extends('layouts.master')

@section('content')
<div class="row">
	<ol class="breadcrumb">
	  <li><a href="/">Home</a></li> 
	  <li><a href="{{ url('order') }}">สั่งซื้อผลิตภัณฑ์</a></li>
	  <li><a href="{{ url('order/comfirm-and-shipping') }}">สรุปค่าใช้จ่าย</a></li>
	  <li class="active">ชำระเงิน </li>
	</ol>
</div>


<div class="row">
	<div class="col-md-4">
		<br />
		<div class="panel panel-primary">
			<div class="panel-heading">
				วิธีการชำระเงิน 
			</div>
			<div class="panel-body"> 
					<ul class="list-unstyled" id="payment-options"> 
						<li>
							<input type="radio" name="paymentoption" value="1" id="paymentoption1"/>&nbsp;&nbsp;1.	ชำระเงินทางบัตรเครดิต <br />&nbsp;&nbsp;&nbsp;<img src="/images/visa.png" />
						</li> 
						<li>
							<input type="radio" name="paymentoption" value="2" id="paymentoption2"/>&nbsp;&nbsp;2.	ชำระเงินโดยการโอนเงินผ่านธนาคารหรือตู้ ATM<br />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<img src="/images/thai_bank.png" />
						</li>  
            <li>
							<input type="radio" name="paymentoption" value="3" id="paymentoption3"/>&nbsp;&nbsp;3.	ชำระเงินทาง Paypal<br />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<img src="https://www.paypal.com/en_US/i/logo/PayPal_mark_37x23.gif" style="margin-right: 7px;" /><span>The safer, easier way to pay.</span>
						</li> 
            <!--
            <li>
							<input type="radio" name="paymentoption" value="4" id="paymentoption4"/>&nbsp;&nbsp;4.	ชำระเงินทาง Paysbuy<br />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<img src="/images/PAYSBUY(S).gif" style="margin-right: 7px;" />
						</li>  
            -->
					</ul>
					
				
				    
			</div>
		</div>
		 
		<div class="panel panel-primary">
			<div class="panel-heading">
				รายละเอียด คำสั่งซื้อ
			</div>
			<div class="panel-body">
				<table class="table table-borderless">
					<tbody>
						@foreach($products as $product)
              @if($product->sessionQty > 0 )
							<tr>
								<td><small>{!!$product->name!!} x {{$product->sessionQty}}</small></td>
								<td class="text-right">{{$product->sessionTotalPrice}}</td>
							</tr>
              @endif 
						@endforeach
						<tr>
							<td class="text-right">รวม</td>
							<td class="text-right">{{$totalPrice}}</td>
						</tr>
            @if($discount > 0 )
						<tr>
							<td class="text-right">ส่วนลด VIP 10%</td>
							<td class="text-right">{{$discount}}</td>
						</tr>
            @endif
            <tr>
							<td class="text-right">ค่าจัดส่ง</td>
							<td class="text-right">{{$shipping}}</td>
						</tr>
            @if($point > 0 )
						<tr>
							<td class="text-right">ใช้แต้มสะสม</td>
							<td class="text-right">{{$point}}</td>
						</tr>
            @endif
						<tr>
							<td class="text-right">รวมทั้งหมด</td>
							<td class="text-right">{{$grandTotal}}</td>
						</tr>
					</tbody>
				</table>
			</div>
		</div>
		
		<div class="panel panel-primary">
			<div class="panel-heading">
				ชื่อและสถานที่ผู้จัดส่ง
			</div>
			<div class="panel-body">
				{{Auth::user()->shipping('name')}} {{Auth::user()->shipping('lastname')}}<br />
				{!!nl2br(Auth::user()->shipping('address'))!!}
			</div>
		</div>	
		
	</div>
	<div class="col-md-8">
		<div id="payment-description-1"  class="desc" >
      <form class="form-horizontal" role="form" method="POST" action="{{ url('/order/payment') }}" id="checkout">
			{!! Notification::showAll() !!}
			<h2>{{$page1->title}}</h2>
			{!!$page1->body!!}
			<div class="panel" id="omise-card">
				<div class="panel-header">
					<img src="/images/visa.png" />
				</div>
				<div class="panel-body">
					<div class="alert alert-danger" id="token_errors" style="display: none;"></div>
					<input type="hidden" name="omise_token">
					<div class="form-group"> 
							<div class="col-md-6">
								<strong>Name <span class="require">*</span></strong>
								<input type="text" class="form-control" value="" placeholder="Name" data-omise="holder_name" />
							</div>
							<div class="col-md-6">
								<strong>Card Number <span class="require">*</span></strong>
								<input type="text" class="form-control" value="" placeholder="Card number" data-omise="number" />
							</div> 
					</div>
					<div class="form-group"> 
							<div class="col-md-4">
								<strong>Expiration month <span class="require">*</span></strong>
								<input type="text" class="form-control" value="" placeholder="MM" data-omise="expiration_month" size="4" />
							</div>
							<div class="col-md-4">
								<strong>Expiration year <span class="require">*</span></strong>
								<input type="text" class="form-control" value="" placeholder="YYY" data-omise="expiration_year" size="8" />
							</div>
							<div class="col-md-4">
								<strong>Security Code <span class="require">*</span></strong>
								<input type="text" class="form-control" value="" placeholder="CVC" data-omise="security_code" size="8" />
							</div> 
					</div>
					<div class="form-group">
						<div class="row">
							<div class="col-md-4 col-md-offset-4">
								<input type="submit" value="ชำระค่าสินค้าด้วยบัตรเครดิต / Credit Card" class="btn btn-primary " />
							</div>
						</div>
					</div>
				</div>
			</div>
      <input type="hidden" name="paymentoption" value="1" />
      <input type="hidden" name="_token" value="{{ csrf_token() }}">
      </form>
		</div>
		
		<div id="payment-description-2" class="desc" style="display: none;">
      <form class="form-horizontal" role="form" method="POST" action="{{ url('/order/payment') }}" id="checkout">
			<h2>{{$page2->title}}</h2>
			{!!$page2->body!!}
      <div class="form-group">
				<div class="row">
					<div class="col-md-4 col-md-offset-4">
						<input type="submit" class="btn btn-primary form-control" value="ทำการชำระเงิน" /> 
					</div>
				</div>
			</div>
      <input type="hidden" name="paymentoption" value="2" />
      <input type="hidden" name="_token" value="{{ csrf_token() }}">
      </form>
		</div>
    
    <div id="payment-description-3" class="desc" style="display: none;">
			<h2>3. ชำระเงินทาง Paypal</h2> 
      <div class="form-group">
				<div class="row">
					<div class="col-md-4 col-md-offset-4">
            <br /><br />
            <form action="https://www.paypal.com/cgi-bin/webscr" method="post" accept-charset="utf-8">
              <input type="hidden" name="cmd" value="_cart"> 
              <input type="hidden" name="business" value="{{ config('services.paypal.email') }}">
              <!-- Begin Item -->
              <?php $i = 1 ; ?>
              @foreach($products as $product)
              <input type="hidden" name="quantity_{{ $i }}" value="{{$product->sessionQty}}">
              <input type="hidden" name="item_name_{{ $i }}" value="{!! str_replace('<br/>',"\n",$product->name) !!}">
              <input type="hidden" name="item_number_{{ $i }}" value="{{ $product->sku }}">
              <input type="hidden" name="amount_{{ $i }}" value="{{ $product->sale_price }}"> 
              <?php $i++ ; ?>
              @endforeach
              <!-- End Item -->
              <input type="hidden" name="shipping_1" value="{{ $shipping }}">  
              <input type="hidden" name="currency_code" value="THB">
              <input type="hidden" name="discount_amount_cart" value="{{$discount}}">
              
              <input type="hidden" name="email" value="{{Auth::user()->shipping('email')}}">
              <input type="hidden" name="first_name" value="{{Auth::user()->shipping('name')}}">
              <input type="hidden" name="last_name" value="{{Auth::user()->shipping('lastname')}}">
              <input type="hidden" name="address1" value="{{Auth::user()->shipping('address')}}">
              <input type="hidden" name="charset" value="utf-8">
              
              <input type="hidden" name="notify_url" value="{{ url('/order/ipn_paypal') }}"/>
              <input type="hidden" name="return" value="{{ url('/order/complete_paypal') }}"/>
              <input type="hidden" name="cancel_return" value="{{ url('/order/cancel_paypal') }}"/> 
              <input type="hidden" name="invoice" value="{{ $invoice->id }}"/>
              <!--<input type="hidden" name="image_url" value="https://gator86.hostgator.com/~nutjaa/_taic-vichakorn/taic-vichakorn/public/images/logo-s.gif" />-->
              <input type="image" src="https://www.paypalobjects.com/en_US/i/btn/x-click-but6.gif" border="0" name="upload" alt="Make payments with PayPal - it's fast, free and secure!" />
              </form>
					</div>
				</div>
			</div>
      
		</div>
    <div id="payment-description-4" class="desc" style="display: none;">
      <h2>4. ชำระเงินทาง Paysbuy</h2> 
      <div class="form-group">
				<div class="row">
					<div class="col-md-4 col-md-offset-4">
            <br /><br />
            <form action="https://demo.PAYSBUY.com/paynow.aspx" method="post" accept-charset="utf-8">
              <input type="hidden" name="psb" value="psb"/> 
              <input type="hidden" name="biz" value="nutjaa@msn.com"/> 
              <input type="hidden" name="inv" value="{{ $invoice->id }}"/>
              <input type="hidden" name="itm" value="item"/>  
              <input type="hidden" name="amt" value="{{ $grandTotal }}"/>  
              
              <input type="hidden" name="reqURL" value="{{ url('/order/req_paysbuy') }}"/>
              <input type="hidden" name="postURL" value="{{ url('/order/post_paysbuy') }}"/> 
               
              <input type="image" src="https://www.PAYSBUY.com/images/p_click2pay.gif" border="0" name="submit" alt="Make payments with PAYSBUY - it's fast, free and secure!">
              </form>
					</div>
				</div>
			</div>
    </div>
	</div>
</div>
</form>
@stop

@section('headerjs')  
@stop

@section('footerjs') 
    
    <script src="https://cdn.omise.co/omise.js.gz"></script>
    <script>
	  Omise.setPublicKey("{{ config('services.omise.public_key') }}");
	</script>
	<script type="text/javascript" src="/js/page/order.payment.js"></script>
@stop