<?php namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Comment; 

use Carbon;
use Krucas\Notification\Facades\Notification;
 

class CommentsController extends Controller {
	public function __construct()
	{
		$this->middleware('auth');
	}
  
  public function update(Request $requests , $id)
	{  
    $comment = Comment::find($id);
		$comment->body = $requests->input('body'); 
    
    $date = Carbon::createFromFormat('d/m/Y G:i:s', $requests->input('updated_at'), 'Asia/Bangkok'); 
		$date->setTimezone('UTC');
    
		$comment->updated_at = $date->toDateTimeString() ;  
    
    $redirect_url = $requests->input('redirect_url');   
  
		if ($comment->updateUniques()) {  
			Notification::success('Successfull update comments <a href="'.url($redirect_url . '/' . $comment->id).'">'.$comment->id.'</a>');
			return redirect($redirect_url) ; 
		}else{ 
			Notification::error( $comment->errors()->All());
			return redirect($redirect_url . '/' . $comment->id)->withInput() ; 
		}
  }
  
  public function destroy(Request $requests , $id){
    Comment::destroy($id) ; 
    $redirect_url = $requests->input('redirect_url');   
    Notification::success('Successfull remove comments');
    return redirect($redirect_url) ; 
  }
}