@extends('layouts.admin')
@section('content') 
<div class="breadcrumbs">
    <ol class="breadcrumb">
        <li><a href="/"><i class="fa fa-home"></i>Home</a></li>
        <li><a href="/admin">Admin</a></li>
        <li><a href="/pages">Pages</a></li>
        <li class="active">Edit page - {{ $page->title }}</li>
    </ol>
</div>

<div class="innerLR">
    <div class="row manage-header">
        <div class="col-lg-7">
        	<h2>Edit page - {{ $page->title }}</h2>
        </div>
 	</div>
    
    <div class="widget">
        {!! Notification::showAll() !!}
        {!! Form::model($page, array('url' => 'pages/'. $page->id , 'method' => 'PUT'  , 'class'=>'form-horizontal' , 'role' => 'form' )) !!}
        	 @include('admin/page/form')  
        {!! Form::close() !!}
    </div>
</div>

@stop

@section('footerjs') 
	{!! HTML::script('js/ckeditor/ckeditor.js'); !!}  
@stop