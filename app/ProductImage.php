<?php namespace App;

use LaravelBook\Ardent\Ardent;
use File;
use Image;

class ProductImage extends Ardent  {
    
    protected $table = 'product_images';
    
    protected $attachment = array( 
        'thumb' => array('w' => 100 , 'h' => 100 )
    ) ; 
    
    public static $rules = array(
    	'product_id' => 'exists:products,id', 
	);
    
    public static $relationsData = array(
        'product' => array(self::BELONGS_TO, 'App\Product', 'foreignKey' => 'product_id')   
    );
    
    public function processImage(){
    	foreach($this->attachment as $key => $attach){ 
    		if(! File::exists(public_path() . '/images/products/' . $this->id . '/' . $key . '/'))
            	File::makeDirectory(public_path() . '/images/products/' . $this->id . '/' . $key . '/');
            $image = Image::make( public_path() . '/images/products/' . $this->id . '/' . $this->filename )->fit($attach['w'],$attach['h'])->save( public_path() . '/images/products/' . $this->id . '/' . $key . '/' . $this->filename);
        }
    }
    
    public function imagePath($type){
    	if ($type == 'original' || $type == null)
    		return '/images/products/' . $this->id .'/' .  $this->filename 	 ;
        return '/images/products/' . $this->id . '/'.$type.'/' .  $this->filename 	 ;
    }
    
}