 
$(function () {
    $('#transferdate').datetimepicker({
        format: 'D/M/YYYY'
    });
    $('#transfertime').datetimepicker({
        format: 'LT'
    });
    
    $("#transfer-form").validate({
	  rules: { 
	  	amount:{
	  		required: true,
	  		number : true
	  	},
	  	order_id:{
	  		required: true
	  	}
  	  },
	  submitHandler: function(form) {
	    form.submit();
	  }
	});
});
    
angular.module('vichakorn', []).controller('vichakornController', function($scope) {  
    $scope.order_id = null  ;
	$scope.order = null ; 
 	$scope.orders = [] ; 
 	$scope.order_changed = function(){
 		for(var i = 0 ; i < $scope.orders.length ; i++){
 			if( $scope.order_id == $scope.orders[i].id){ 
 				$scope.order = $scope.orders[i] ; 
 				break ; 
			}
 		}
 	}
 
});
