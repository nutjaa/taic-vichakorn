@extends('layouts.master')

@section('content')
<div class="row">
	<ol class="breadcrumb">
	  <li><a href="/">Home</a></li> 
	  <li class="active">รายละเอียดผลิตภัณฑ์</li>
	</ol>
</div>
<h2>รายละเอียดผลิตภัณฑ์</h2>
<div class="row">
  <?php $i = 1 ?>
	@foreach($products as $product)
	<div class="thumb-item-wrapper col-md-3 col-xs-6">
		<div class="thumb-item-container">
			<a class="photo thumb-item-btn" href="{{$product->route}}">
                <div class="post-thumb">
				    <div class="item-photo" style="background-image:url({{ str_replace(' ','%20',$product->imageDefault('original')) }})"></div>
                </div>
				<h4>{{$product->nameFirst}}</h4>
				@if( $product->price != $product->sale_price)
					<span class="price onSale">฿ {{$product->price}}</span>&nbsp; 
				@endif
					<span class="price">฿ {{$product->sale_price}}</span>
			</a>
		</div>
	</div> 
  @if($i %2 == 0 ) 
  <div class="clearfix visible-xs"></div>
  @endif
  <?php $i++ ?>
	@endforeach
 
</div>
@stop