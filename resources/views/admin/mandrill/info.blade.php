@extends('layouts.admin')
@section('content') 
<div class="breadcrumbs">
  <ol class="breadcrumb">
    <li><a href="/"><i class="fa fa-home"></i>Admin</a></li> 
    <li class="active">Mandrill - info</li>
  </ol>
</div>  
<div class="innerLR">
  <div class="row manage-header">
    <div class="col-lg-7">
    	<h2>Mandrill - Info</h2>
    </div> 
  </div>
  <div class="widget">
    <div class="table-responsive">
      <table class="table table-striped">
        <tbody>
          <tr>
            <td class="col-md-2"><strong>Username</strong></td>
            <td class="col-md-4">{{ $info['username'] }}</td>
            <td class="col-md-2"><strong>Created</strong></td>
            <td class="col-md-4">{{ $info['created_at'] }}</td>
          </tr>
          <tr>
            <td class="col-md-2"><strong>Reputation</strong></td>
            <td class="col-md-4">{{ $info['reputation'] }}</td>
            <td class="col-md-2"><strong>Hourly Quota</strong></td>
            <td class="col-md-4">{{ $info['hourly_quota'] }}</td>
          </tr>
        </tbody>
      </table>
    </div>
  </div>
  <div class="widget">
    <div class="table-responsive">
      <table class="table table-striped">
        <thead>
          <tr>
            <th>ระยะเวลา</th>
            <th class="text-right">sent</th>
            <th class="text-right">hard bounces</th>
            <th class="text-right">soft bounces</th>
            <th class="text-right">rejects</th>
            <th class="text-right">complaints</th>
            <th class="text-right">unsubs</th>
            <th class="text-right">opens</th>
            <th class="text-right">unique opens</th>
            <th class="text-right">clicks</th>
            <th class="text-right">unique clicks</th>
          </tr>
        </thead>
        <tbody>
          @foreach($info['stats'] as $key => $stat)
          <tr>
            <td>{{ $key }}</td>
            <td class="text-right">{{ $stat['sent'] }}</td>
            <td class="text-right">{{ $stat['hard_bounces'] }}</td>
            <td class="text-right">{{ $stat['soft_bounces'] }}</td>
            <td class="text-right">{{ $stat['rejects'] }}</td>
            <td class="text-right">{{ $stat['complaints'] }}</td>
            <td class="text-right">{{ $stat['unsubs'] }}</td>
            <td class="text-right">{{ $stat['opens'] }}</td>
            <td class="text-right">{{ $stat['unique_opens'] }}</td>
            <td class="text-right">{{ $stat['clicks'] }}</td>
            <td class="text-right">{{ $stat['unique_clicks'] }}</td>
          </tr>
          @endforeach
        </tbody>
      </table>
    </div>
  </div>
</div>
@stop