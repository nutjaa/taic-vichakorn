@extends('layouts.admin')
@section('content') 
<div class="breadcrumbs">
    <ol class="breadcrumb">
        <li><a href="/"><i class="fa fa-home"></i>Home</a></li>
        <li><a href="/admin">Admin</a></li>
        <li class="active">Omise</li>
    </ol>
</div>
<div class="innerLR">
    <div class="row manage-header">
        <div class="col-lg-7">
        	<h2>Omise charges list</h2>
        </div> 
    </div>
    <div class="widget">
        {!! Notification::showAll() !!}
        <div class="table-responsive">
            <table class="table">
                <thead>
    			<tr>
    				<th width="45">ID</th>
					<th>Object</th> 
    				<th class="text-right">Amonth</th>
    				<th class="text-center">Mode</th>
                    <th>Description</th>
    				<th>Created</th> 
    				<!--
    				<th>Actions</th>
    				-->
    			</tr>
    		</thead>
    		<tbody>
    		@foreach($charges->offsetGet('data') as $charge)
    			<tr>
    				<td>{{ $charge['id'] }}</td>
    				<td>{{ $charge['object'] }} </td>
    				<td class="text-right"><strong>{{ $charge['amount']/100 }}</strong></td>
                    <td class="text-center">
						@if( $charge['livemode'] ) 
							<span class="label label-info">Live</span>
						@else
							<span class="label label-danger">Test</span>
						@endif
					</td>
                    <td>
                        <?php
                        	if( strpos( $charge['description'] , '#' ) === 0 ){
                        		$space = strpos( $charge['description'] , ' ' ) ; 
                        		$order = substr( $charge['description'] , 1 , $space - 1 ) ;
                        		$charge['description'] = '<a href="/orders/'.$order.'">#' . $order . '</a> ' . substr($charge['description'],$space , strlen($charge['description']    ) );
                        	} 
                        	echo $charge['description'];
						?>
                    </td> 
    				<td>{{ ThaiTime($charge['created']) }}</td> 
					<!--
    				<td>
    					<a class="btn btn-small btn-info" href="{{ URL::to('omises/' . $charge['id'] . '') }}">View</a>
    				</td>
    				-->
    			</tr>
    		@endforeach
    		</tbody>
    	</table>
        <div class="text-center">
    		 <nav>
			  <ul class="pagination">
			  	@if($page > 1 )
			    <li>
			      <a href="/omises?page={{ $page -1 }}" aria-label="Previous">
			        <span aria-hidden="true">&laquo;</span>
			      </a>
			    </li>
			    @endif
			    @for($i = 1 ; $i <= ceil($charges->offsetGet('total') / 15 ) ; $i++ )
			    <li><a href="/omises?page={{ $i }}">{{ $i }}</a></li> 
			    @endfor
			    @if($page < ceil($charges->offsetGet('total') / 15 ) )
			    <li>
			      <a href="/omises?page={{ $page + 1 }}" aria-label="Next">
			        <span aria-hidden="true">&raquo;</span>
			      </a>
			    </li>
			    @endif
			  </ul>
			</nav>
    	</div>
        </div>
    </div>
</div>   
@stop

@section('footerjs')  
    <script type="text/javascript" src="/js/emsModal.js"></script>
@stop