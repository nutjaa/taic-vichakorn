<?php namespace App;

use App\Termdata;
use LaravelBook\Ardent\Ardent;
use Carbon ; 
use Auth;

class Order extends Ardent{
    protected $table = 'orders'; 
    
    public static $relationsData = array(  
        'user' => array(self::BELONGS_TO, 'App\User', 'foreignKey' => 'user_id') , 
        'items' => array(self::HAS_MANY, 'App\OrderItem' , 'order_id') ,     
        'status' => array(self::BELONGS_TO, 'App\Termdata', 'foreignKey' => 'status_id') ,  
        #'statuses' => array(self::HAS_MANY, 'App\OrderStatus' , 'order_id') ,      
        #'confirmtransfers' => array(self::HAS_MANY , 'App\OrderConfirmtransfer', 'order_id') 
    ) ; 
    
    public function statuses() {
	    return $this->hasMany('App\OrderStatus','order_id')->orderBy('created_at', 'desc');
	}
	
	public function confirmtransfers(){
		return $this->hasMany('App\OrderConfirmtransfer','order_id')->orderBy('transfered_at', 'desc');
	}
    
    /*--- Scope ---*/
    
    public function scopeSearch($query,$q){
    	return $query ;   
    } 
    
    public function scopeByUser($query,$user_id){
    	return $query->where('user_id','=',$user_id);
    }
    
    public function scopeForTransferReport($query){
    	$t1 = Termdata::whereIn('name',['ordered','transfer-reported'])->lists('id') ;
    	return $query->whereIn('status_id',$t1) ; 
    }
    
    public function scopeHasStatus($query , $name){
    	if($name == '' || $name == 'all')
    		return $query ; 
   		$term = Termdata::name($name)->first() ; 
   		if(is_null($term))
   			return $query ; 
   		return $query->where('status_id','=',$term->id);
    }
    
    /*--- attribure ---*/
    public function getGrandTotalAttribute(){
    	return number_format( $this->grandTotalNumber  , 2 ); 
    }
    
    public function getGrandTotalNumberAttribute(){
      return $this->shipping_price + $this->total_price - $this->discount - $this->used_point ; 
    }
    
    public function getCreatedAtFormatAttribute(){ 
		$date = Carbon::createFromFormat('Y-m-d H:i:s', $this->created_at  );
		$date->setTimezone('Asia/Bangkok');
		return $date->format('d/m/Y H:i:s');
    }
    
    public function getUpdatedAtFormatAttribute(){ 
		$date = Carbon::createFromFormat('Y-m-d H:i:s', $this->updated_at  );
		$date->setTimezone('Asia/Bangkok');
		return $date->format('d/m/Y H:i:s');
    }
    
    public function getOrderNumberAttribute(){
    	return str_pad($this->id, 4, "0", STR_PAD_LEFT) ; 
    }
    
    public function getFullnameAttribute(){
    	return $this->firstname . ' ' . $this->lastname ; 
    }
    
    public function getPaymenttypeAttribute(){
    	switch ($this->payment_type_id){ 
			case 1:
				return 'Credit Card';
			break;
      case 3:
        return 'Paypal';
      break;
			default :
				return 'Bank Transfer' ; 
		}
    }
    
    public function getIsBankTransferAttribute(){
    	return $this->payment_type_id == 2;
    }
    
    public function getValidAttribute(){
    	return $this->user_id == Auth::user()->id ; 
    }
    
    public function getLabelStatusAttribute(){
    	switch ($this->status->name){ 
			case 'ordered':
				return 'label-default';
			break;
      case 'paypal-pending':
				return 'label-default';
			break;
			case 'transfer-reported' :
				return 'label-info';
			break ; 
      case 'canceled' : 
        return 'label-danger';
      break ; 
      case 'confirm' : 
        return 'label-primary';
      break ;
      case 'shipping' : 
        return 'label-success';
      break ;
			default :
				return 'label-warning' ; 
		}
    }
    
    public function getCanCanceledAttribute(){
        return $this->status->name == 'ordered' ; 
    }
    
    public function getCanConfirmAttribute(){
        return ($this->status->name == 'ordered') || ($this->status->name == 'transfer-reported') ; 
    }
    
    public function getCanShippingAttribute(){
        return $this->status->name == 'confirm' ; 
    }
    
    public function getIsShippingAttribute(){
        return $this->status->name == 'shipping' ; 
    }
    
    public function getHasShipNoAttribute(){
        return !empty($this->ship_no); 
    }
    
    public function getCurrentPointAttribute(){  
      return (int)( $this->grandTotalNumber / 25 ) ;  
    }
}