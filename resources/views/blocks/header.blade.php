<div id="header">
	<div class="container">
		<div class="row">
			<div class="col-md-5 col-sm-4 col-xs-4">
				<ul class="list-inline text-left">
					<li><a href="/aboutus">เกี่ยวกับเรา  </a></li>
					<li>/</li>
					<li><a href="/contactus">ติดต่อเรา</a></li>
				</ul>
			</div>
			<div class="col-md-2 col-sm-4 col-xs-4">
				<a href="/"><img src="/images/logo.png" class="img-responsive" /></a>
			</div> 
			<div class="col-md-5 col-sm-4 col-xs-4">
				<ul class="list-inline text-right">
					@if(Auth::guest())
					<li><a href="/auth/register">สมัครสมาชิก</a></li>
					<li>/</li>
					<li><a href="/auth/login">เข้าสู่ระบบ</a></li>
					@else
					<li><a href="/home">{{Auth::user()->email}}@if(Auth::user()->is_vip)<i class="fa fa-star"></i>@endif</a> ,<a href="/auth/logout">ออกจากระบบ</a></li>
					@endif
				</ul>
			</div>
		</div>
	</div>
</div>