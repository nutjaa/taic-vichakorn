<?php namespace App\Http\Controllers;

use App\Page;
use App\Pagecategory;
use App\Vocabulary ; 
use App\Termdata ; 

use SEOMeta;
use OpenGraph;


class HowtoController extends Controller { 
 	public $vocab  ; 
 	public $termdata;
 	
 	public function __construct()
	{
		$this->vocab = Vocabulary::Page()->first() ; 
		$this->termdata = Termdata::where('name' , '=' , 'howto')->where('vid' , '=' , $this->vocab->id )->first() ; 
	 
	}

	/**
	 * Show the application dashboard to the user.
	 *
	 * @return Response
	 */
	public function index()
	{ 
 
		$page = Page::join('page_categories',function($join){
			$join->on('page_categories.page_id','=','pages.id') ; 
		})->where('page_categories.term_id','=',$this->termdata->id)->active()->orderBy('pages.order', 'ASC')->first()  ; 
 
		return redirect('ordering-and-how-to-use/'.$page->id.'/'.$page->sefu) ;
	}
	
	public function view($id , $sefu){
		$page = Page::active()->find($id)  ; 
		if(! is_object($page)){
			return Redirect('howto') ; 
		}
    
    SEOMeta::setTitle('ลำดับและวิธีการใช้ผลิตภัณฑ์');
    SEOMeta::setDescription($page->title);
    
    OpenGraph::setDescription($page->title);
    OpenGraph::setTitle('ลำดับและวิธีการใช้ผลิตภัณฑ์');
    OpenGraph::setUrl(url('/ordering-and-how-to-use/'.$page->id.'/'.$sefu)); 
    
    
		$pages = Page::join('page_categories',function($join){
			$join->on('page_categories.page_id','=','pages.id') ; 
		})->where('page_categories.term_id','=',$this->termdata->id)->active()->orderBy('pages.order', 'ASC')->get()  ;  
		return view('howto')->with('page',$page)->with('pages',$pages) ; 
	}

}
