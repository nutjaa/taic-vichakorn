<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;

use App\Vocabulary ;
use App\Termdata ;  

class VocabularySeeder extends Seeder {

	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run()
	{
		DB::table('vocabularies')->truncate();
        DB::table('term_datas')->truncate();
        
        $vocab = Vocabulary::create(array('name' => 'role' , 'description' => 'Users role')) ; 
        
        Termdata::create(array('name' => 'admin'  , 'label' => 'Admin' , 'description' => '' , 'vid' => $vocab->id ));  
        Termdata::create(array('name' => 'user'  , 'label' => 'User' , 'description' => '' , 'vid' => $vocab->id )); 
        
        $vocab = Vocabulary::create(array('name' => 'lang' , 'description' => 'Language selector')) ;  
        Termdata::create(array('name' => 'th'  , 'label' => 'th' , 'description' => '' , 'vid' => $vocab->id )); 
        
        $vocab = Vocabulary::create(array('name' => 'page' , 'description' => 'Page Categories')) ; 
        Termdata::create(array('name' => 'frontpage'  , 'label' => 'Front page' , 'description' => '' , 'vid' => $vocab->id ));
        Termdata::create(array('name' => 'aboutus'  , 'label' => 'About US' , 'description' => '' , 'vid' => $vocab->id ));
        
        $vocab = Vocabulary::create(array('name' => 'product' , 'description' => 'Product Categories')) ; 
        Termdata::create(array('name' => 'none'  , 'label' => 'none' , 'description' => '' , 'vid' => $vocab->id ));
	}

}
