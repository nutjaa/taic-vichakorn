<?php namespace App;
 
use LaravelBook\Ardent\Ardent;
use Carbon ; 
use Auth;

class Comment extends Ardent{
  protected $table = 'comments'; 
  /*--- relations --- */
  public static $relationsData = array( 
    'user' => array(self::BELONGS_TO, 'App\User', 'foreignKey' => 'user_id') , 
  );
  public function commentable()
  {
    return $this->morphTo();
  }
  
  public function getPostedAttribute()
  {
      return $this->getTimeAgo($this->updated_at);
  }
  
  
  // Returns a human readable diff of the given timestamp
  protected function getTimeAgo($timestamp)
  {
      return Carbon::createFromTimeStamp(strtotime($timestamp))->diffForHumans();
  }
  
  /*--- Attribute ---*/
  public function getUpdatedAtFormatAttribute(){ 
		$date = Carbon::createFromFormat('Y-m-d H:i:s', $this->updated_at  );
		$date->setTimezone('Asia/Bangkok');
		return $date->format('d/m/Y H:i:s');
    }
}