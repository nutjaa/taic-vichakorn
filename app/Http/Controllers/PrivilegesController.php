<?php namespace App\Http\Controllers;

use App\Page;

use Krucas\Notification\Facades\Notification;

class PrivilegesController extends Controller {
	
	public function index(){ 
		$page = Page::active()->sefu('Privileges')->first() ; 
		return view('privileges')->with('page',$page) ; 
	}
 
}