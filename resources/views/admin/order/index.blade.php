@extends('layouts.admin')
@section('content') 
<div class="breadcrumbs">
    <ol class="breadcrumb">
        <li><a href="/"><i class="fa fa-home"></i>Home</a></li>
        <li><a href="/admin">Admin</a></li>
        <li class="active">Orders</li>
    </ol>
</div>
<div class="innerLR">
    <div class="row manage-header">
        <div class="col-lg-7">
        	<h2>Orders Management</h2>
        </div>
        {!! Form::open(array( 'method' => 'get')) !!}
        <div class="col-lg-2"> 
        	{!! Form::select('s',$statusOptions,$s, array('class' => 'form-control', 'onchange' => "this.form.submit()" ) ) !!}
        </div>
        <div class="col-lg-2"> 
        	<input type="text" name="q" placeholder="search" class="span3 pull-right form-control" value="{{ $q }}" /> 
        </div>
        {!! Form::close() !!}
    </div>
    <div class="widget">
        {!! Notification::showAll() !!}
        <div class="table-responsive">
            <table class="table">
                <thead>
    			<tr>
    				<th width="45">ID</th>
    				<th>Email</th>
    				<th>Amonth</th>
    				<th>Status</th>
                    <th>EMS</th>
    				<th>Created</th>
                    <th>Updated</th> 
    				<th>Actions</th>
    			</tr>
    		</thead>
    		<tbody>
    		@foreach($orders as $order)
    			<tr>
    				<td>{{ $order->orderNumber }}</td>
    				<td>{{ $order->email }} </td>
    				<td><strong>{{ $order->grandTotal }}</strong></td>
            <td>
						  <a  href="{{ URL::to('orders?s=' . $order->status->name .'&q=' . $q ) }}"><span class="label {{$order->labelStatus}}">{{ $order->status->label }}</span></a>
            </td>
            <td>
                <!--<a href="#" class="ems-checking" data-toggle="modal" data-target="#emsModal" data-id="{{$order->ship_no}}">{{$order->ship_no}}</a>--> 
                <a href="http://emsbot.com/#/?s={{$order->ship_no}}" target="_blank">{{$order->ship_no}}</a>
            </td>
            <td>
						  {{ $order->createdAtFormat }}
            </td> 
    				<td>{{ $order->updatedAtFormat }}</td> 
    				<td>
    					<a class="btn btn-small btn-info" href="{{ URL::to('orders/' . $order->id . '') }}">View</a>
    				</td>
    			</tr>
    		@endforeach
    		</tbody>
    	</table>
        <div class="text-center">
    		<?php echo $orders->appends(array('q' => $q))->render(); ?>
    	</div>
        </div>
    </div>
</div>  
@include('blocks.emsmodal')
@stop

@section('footerjs')  
    <script type="text/javascript" src="/js/emsModal.js"></script>
@stop