<?php namespace App;

use LaravelBook\Ardent\Ardent;
use Auth;

class Invoice extends Ardent{
  protected $table = 'invoices'; 
  
  public static $relationsData = array(  
    'order' => array(self::BELONGS_TO, 'App\Order', 'foreignKey' => 'order_id') 
  ) ; 
  
  public function setDataAttribute($value)
  {
    $this->attributes['data'] = json_encode($value);
  }
  
  public function getDataAttribute($value){
    return json_decode($value, true);
  }
}