@extends('layouts.master')

@section('content')
<div class="row">
	<ol class="breadcrumb">
	  <li><a href="/">Home</a></li> 
	  <li><a href="{{ url('order') }}">สั่งซื้อผลิตภัณฑ์</a></li>
	  <li class="active">สรุปค่าใช้จ่าย</li>
	</ol>
</div>
<div class="row"> 
	<div class="col-md-12">
    {!! Notification::showAll() !!} 
		<h2>สรุปค่าใช้จ่าย</h2>
		<table class="table table-striped">
	    	<thead>
	    		<tr> 
	    			<th></th>
	    			<th class="text-center">รหัสสินค้า</th>
	    			<th>ชื่อผลิตภัณฑ์</th>
	    			<th>ราคา</th>
	    			<th class="col-xs-2 text-right">จำนวนที่สั่งซื้อ</th>
	    			<th class="text-right"> รวมเป็นเงิน</th>
	    		</tr>
	    	</thead>
	    	<tbody>
				@foreach($products as $product)
          @if( $product->sessionQty > 0 )
					<tr>
						<td><img src="{{$product->imageDefault('thumb')}}" /></td>
						<td class="text-center">{{$product->sku}}</td>
						<td>{!!$product->name!!}</td>
						<td>{{$product->sale_price}}</td>
						<td class="text-right">{{$product->sessionQty}}</td>
						<td class="text-right">{{$product->sessionTotalPrice}} ฿</td>
					</tr>
          @endif
				@endforeach
				<tr>
					<td colspan="5" class="text-right"><strong>สินค้าทั้งหมด</strong></td>
					<td class="text-right">{{$totalPrice}} ฿</td>
				</tr>
        @if( $discount > 0 )
        <tr>
					<td colspan="5" class="text-right"><strong>ส่วนลด VIP 10%</strong></td>
					<td class="text-right">{{$discount}} ฿</td>
				</tr>
        @endif
				<tr>
					<td colspan="5" class="text-right"><strong>รวมจัดส่งสินค้า</strong></td>
					<td class="text-right">{{$shipping}} ฿</td>
				</tr>
        @if(!Auth::guest() && Auth::user()->point > 0 ) 
        <form class="form-horizontal" role="form" method="POST" action="{{ url('/order/comfirm-use-point') }}">
          <input type="hidden" name="_token" value="{{ csrf_token() }}"> 
        <tr>
          <td>ใช้คะแนนสะสม ({{Auth::user()->point}})</td>
          <td><input type="text" name="point" class="form-control" /></td>
          <td>
            <table>
              <tr>
                <td><input type="submit" value="ADD" class="btn btn-primary"/></td>
                <td style="padding-left: 5px;"><small>กรุณากรอกตัวเลขที่ต้องการใช้คะแนนสะสมมาเป็นส่วนลดในการสั่งซื้อครั้งนี้ แล้วคลิกที่ปุ่ม add เพื่อให้ระบบคำนวนราคาส่วนลดค่ะ โดย 1 คะแนนสะสมจะเป็นส่วนลด 1 บาทค่ะ</small></td>
              </tr>
            </table>
            
          </td>
					<td colspan="2" class="text-right"><strong>ใช้แต้มสะสม</strong></td>
					<td class="text-right">{{$point}} ฿</td>
				</tr> 
        </form>
        @endif
				<tr>
					<td colspan="5" class="text-right"><strong>รวมทั้งหมด</strong></td>
					<td class="text-right">{{$grandTotal}} ฿</td>
				</tr>
			</tbody>
 		</table>
	</div>
</div>
<div class="row">
    <div class="col-md-12">
        <h2>ชื่อและสถานที่ผู้รับ</h2>
    </div>
</div>
@if(Auth::guest())
<div class="row">
    <div class="col-md-4 col-md-offset-4">
        <a href="/auth/login?redirectTo=/order/comfirm-and-shipping" class="btn btn-primary form-control">กรุณา Login เพื่อทำรายการ</a>
        <br /><br />
    </div> 
</div>    
@else
<form class="form-horizontal" role="form" method="POST" action="{{ url('/order/comfirm-and-shipping') }}">
	<input type="hidden" name="_token" value="{{ csrf_token() }}"> 
        
    <div class="form-group"> 
        <label class="col-sm-2 control-label">ชื่อ</label>
    	<div class="col-sm-4">
	         <input class="form-control" name="name" value="{{Auth::user()->shipping('name')}}" />
    	</div>
        <label class="col-sm-1 control-label">นามสกุล</label>
    	<div class="col-sm-4">
	         <input class="form-control" name="lastname" value="{{Auth::user()->shipping('lastname')}}" />
    	</div> 
    </div>
    <div class="form-group"> 
        <label class="col-sm-2 control-label">อีเมล์</label>
    	<div class="col-sm-4">
	         <input class="form-control" name="email" value="{{Auth::user()->shipping('email')}}" />
    	</div>
        <label class="col-sm-1 control-label">โทร</label>
    	<div class="col-sm-4">
	         <input class="form-control" name="phone" value="{{Auth::user()->shipping('phone')}}" />
    	</div> 
    </div>
    <div class="form-group"> 
        <label class="col-sm-2 control-label">ที่อยู่จัดส่ง</label>
    	<div class="col-sm-4">
             <textarea class="form-control" name="address">{{Auth::user()->shipping('address')}}</textarea>
    	</div>
    </div>
    <div class="row">
        <div class="col-md-3 col-md-offset-2"><input type="submit" class="form-control btn btn-primary" value="ตกลงและไปยังหน้าชำระเงิน" /></div>
    </div>
    <br />
</form>
@endif

@stop