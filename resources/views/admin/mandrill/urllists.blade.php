@extends('layouts.admin')
@section('content') 
<div class="breadcrumbs">
  <ol class="breadcrumb">
    <li><a href="/"><i class="fa fa-home"></i>Admin</a></li> 
    <li><a href="/mandrill/info">Mandrill</a></li> 
    <li class="active">Url Lists</li>
  </ol>
</div>  

<div class="innerLR">
  <div class="row manage-header">
    <div class="col-lg-12">
    	<h2>Mandrill - Url Lists ( Get the 100 most clicked URLs )</h2>
    </div> 
  </div> 
  
  <div class="widget">
    <div class="table-responsive">
      <table class="table table-striped">
        <thead>
          <tr>
            <th>url</th>
            <th class="text-right">sent</th> 
            <th class="text-right">clicks</th>
            <th class="text-right">unique clicks</th>
          </tr>
        </thead>
        <tbody>
          @foreach($lists as $list)
          <tr>
            <td>{{ $list['url'] }}</td>
            <td class="text-right">{{ $list['sent'] }}</td>
            <td class="text-right">{{ $list['clicks'] }}</td>
            <td class="text-right">{{ $list['unique_clicks'] }}</td> 
          </tr>
          @endforeach
        </tbody>
      </table>
    </div>
  </div>
</div>
@stop