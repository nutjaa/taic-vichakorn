<?php namespace App\Http\Controllers;

use App\Page;
use App\Pagecategory;
use App\Vocabulary ; 
use App\Termdata ; 
use App\Order;
use App\OrderConfirmtransfer;
use App\OrderStatus;

use Illuminate\Http\Request;

use Carbon ; 
use Auth;
use Mail;
use Config ; 

use Krucas\Notification\Facades\Notification;

class TransferController extends Controller { 
 	public $vocab  ; 
 	public $termdata;
 	
 	public function __construct()
	{
	 
	 
	}

	/**
	 * Show the application dashboard to the user.
	 *
	 * @return Response
	 */
	public function index()
	{  
		$day_list = [] ; 
		for($i = 1 ; $i<32 ; $i++){
			$day_list[$i] =str_pad($i,2,"0", STR_PAD_LEFT) ; 
		}
		$month_list = [] ; 
		for($i = 1 ; $i<13 ; $i++){
			$month_list[$i] =str_pad($i,2,"0", STR_PAD_LEFT) ; 
		}
		$year_list = [] ; 
		$year = date('Y') ; 
		for($i = 0 ; $i < 50 ; $i ++ ){
			$year_list[$year-$i] = $year - $i ; 
 		}
 		$hour_list = [] ;   
		for($i = 0 ; $i < 24 ; $i ++ ){
			$hour_list[$i] =str_pad($i,2,"0", STR_PAD_LEFT) ; 
 		}
 		
 		$minute_list = [] ;   
		for($i = 0 ; $i < 60 ; $i ++ ){
			$minute_list[$i] =str_pad($i,2,"0", STR_PAD_LEFT) ; 
 		}
 		if(Auth::guest())
 			return view('transfer')->with('day_list',$day_list)->with('month_list',$month_list)->with('year_list',$year_list)->with('hour_list',$hour_list)->with('minute_list',$minute_list) ;
 		
         #Load order information
 		$orders = Order::ForTransferReport()->byUser(Auth::user()->id)->get() ; 
 		$orderList = [] ;
		 
 		foreach($orders as $order){
 			$orderList[$order->id] = 'เลขที่ '.$order->orderNumber . ' - ' . $order->grandTotal . ' ฿' ; 
 		}
		
		return view('transfer')->with('day_list',$day_list)->with('month_list',$month_list)->with('year_list',$year_list)->with('hour_list',$hour_list)->with('minute_list',$minute_list)->with('orders',$orders)->with('orderList',$orderList) ;
	} 
	
	public function doTransfer(Request $requests){ 
		$date = $requests->input('transferdate'); 
		$time = $requests->input('transfertime'); 
		
		#we receive as bangkok time.
		$date = Carbon::createFromFormat('d/m/Y g:i A', $date. ' ' . $time, 'Asia/Bangkok');
		$date->setTimezone('UTC');
		 
		
 
		
		$order_id = $requests->input('order_id') ;
		$order = Order::find($order_id) ;
		if(is_null($order)){
			Notification::error('ไม่พบหมายเลข order ที่อ้างอิง'); 
			return redirect('moneytransfer');
		}
		if(!$order->valid){
			Notification::error('ไม่พบหมายเลข order ที่อ้างอิง'); 
			return redirect('moneytransfer');
		}
		
		$order->firstname = $requests->input('firstname');
		$order->lastname = $requests->input('lastname');
		$order->phone = $requests->input('phone');
		$order->email = $requests->input('email');
		$order->address = $requests->input('address');
		
		$order_confirm = new OrderConfirmtransfer() ; 
		$order_confirm->order_id = $order_id ; 
		$order_confirm->method = $requests->input('transfer_by','-'); 
		$order_confirm->frombank = $requests->input('from_bank'); 
		$order_confirm->tobank = $requests->input('to_bank'); 
		$order_confirm->amount = $requests->input('amount'); 
		$order_confirm->remark = $requests->input('remark');
		$order_confirm->transfered_at =  $date->toDateTimeString() ; 
 
		if ($order_confirm->save()) {
			$term = Termdata::name('transfer-reported')->first() ; 
			#save new status ; 
			$order_status = new OrderStatus() ;
			$order_status->status_id = $term->id ; 
			$order_status->order_id = $order_id ; 
			$order_status->save() ; 
			#save order;
			$order->status_id = $term->id ;
			$order->save();
			#send mail;
			$sendTo = Config::get('mail.contact_email');  
			Mail::send('emails.transferconfirm', ['order_confirmtransfer' => $order_confirm ], function($message)use ($order,$sendTo)
			{ 
				$message->from($order->email, $order->email);
			    $message->to($sendTo,$sendTo)->subject('แจ้งการโอนเงิน #'.$order->orderNumber);
			});
			
			Notification::success('ทางเราได้รับทราบการแจ้งโอนเงินเป็นที่เรียบร้อย ทางทีมงานจะทำการตรวจสอบและแจ้งให้ทราบค่ะ');
			return redirect('orders/'.$order_id) ; 
		}else{  
			Notification::error( $order_confirm->errors()->All()); 
			return redirect('moneytransfer')->withInput() ; 
		}
	}

}
