<?php namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Vocabulary;
use App\Termdata;
use App\User;
use Auth;
use Validator;
use Hash;
use File;
use Carbon;

use Krucas\Notification\Facades\Notification;

class UsersController extends Controller {
	
	public function __construct()
	{
		$this->middleware('auth');
	}
	
	public function index(Request $requests){
    $q = $requests->input('q') ;  		
		$users = User::with('role')->search($q)->orderBy('name','ASC')->paginate(15)  ; 
    return view('admin.users.index')->with('users', $users)->with('q' , $q) ;  
  }
    
  public function create()
	{ 
    if(Auth::user()->isAdmin()){
      $role_options = Termdata::allrole()->lists('label','id') ;  
    }else{
      $role_options = Termdata::allrole()->where('name' , '!=' , 'admin')->lists('label','id') ;  
    }
       
    $role_options =  array(''=> 'Please select') + $role_options  ;   
    return view('admin.users.create')->with('role_options', $role_options );
	}
    
  public function edit($id)
	{
		$user = User::find($id); 
    if(Auth::user()->isAdmin()){
      $role_options = Termdata::allrole()->lists('label','id') ;  
    }else{
      $role_options = Termdata::allrole()->where('name' , '!=' , 'admin')->lists('label','id') ;  
    }  
    
		return view('admin.users.edit')
			->with('user', $user) 
            ->with('role_options' , $role_options) ; 
	}
    
  public function store(Request $requests)
	{
    $rules = array(
			'repassword'    => 'required|min:4|same:password', 
			'password' => 'required|min:4|same:repassword' 
		);
    $validator = Validator::make($requests->all(), $rules);  	
        
    if( $validator->fails() ){
      Notification::error( $validator->errors()->all() ); 
      return redirect('users/create')->withInput() ;
    }
       
		$user = new User() ;
		$user->name = $requests->input('name');
		$user->email = $requests->input('email');
		$user->phone = $requests->input('phone');
		$user->gender = $requests->input('gender'); 
    $user->user_role_id = $requests->input('user_role_id');
		$user->status = $requests->input('status',0); 
    $user->is_vip = $requests->input('is_vip',0); 
    $user->password =  Hash::make(\Input::get('password')) ;  
    $user->lastname = $requests->input('lastname');
    $user->username = $requests->input('username');
    $user->address = $requests->input('address');
    $user->is_subscribe = $requests->input('is_subscribe',0);
    $user->lineid = $requests->input('lineid');
    $user->facebookname = $requests->input('facebookname');
        
    $birthdate = Carbon::createFromFormat('d/m/Y', $requests->input('birthdate')) ; 
    $user->birthdate = $birthdate->toDateTimeString() ; 
        
		if ($requests->hasFile('avatar')){
			$file = $requests->file('avatar');  
		 	$user->avatar = $file->getClientOriginalName() ; 
		}
		 
		if ($user->save()  ) {
			if ($requests->hasFile('avatar')){
				$file = $requests->file('avatar'); 
		 		$file->move('images/avatar/' . $user->id . '/' , $file->getClientOriginalName()   ); 
		 		$user->processAvatar() ; 
			}
			Notification::success('Successfull create user <a href="'.url('users/' . $user->id . '/edit').'">'.$user->name.'</a>');
			return redirect('users') ; 
		}else{  
			Notification::error( $user->errors()->All()); 
			return redirect('users/create')->withInput() ; 
		}
	}
    
  public function update(Request $requests , $id)
	{
		//Check for password first ; 
    if( trim($requests->input('password')) != ''){
        $rules = array(
			'repassword'    => 'required|min:4|same:password', 
			'password' => 'required|min:4|same:repassword' 
		);
      $validator = Validator::make($requests->all(), $rules);  	
      
      if( $validator->fails() ){
        Notification::error( $validator->errors()->all() ); 
        return redirect('users/' . $id . '/edit')->withInput() ;
      }
    }
        
    $user = User::find($id);
		$user->name = $requests->input('name');
		$user->email = $requests->input('email');
		$user->phone = $requests->input('phone');
		$user->gender = $requests->input('gender'); 
    $user->user_role_id = $requests->input('user_role_id');
		$user->status = $requests->input('status',0); 
    $user->is_vip = $requests->input('is_vip',0); 
    $user->lastname = $requests->input('lastname');
    $user->username = $requests->input('username');
    $user->address = $requests->input('address');
    $user->is_subscribe = $requests->input('is_subscribe',0);
    $user->lineid = $requests->input('lineid');
    $user->facebookname = $requests->input('facebookname');
    $birthdate = Carbon::createFromFormat('d/m/Y', $requests->input('birthdate')) ; 
    $user->birthdate = $birthdate->toDateTimeString() ; 
        
    if( trim($requests->input('password')) != '')
      $user->password =  Hash::make($requests->input('password')) ;  
		if ($requests->hasFile('avatar')){
			$file = $requests->file('avatar'); 
		 	$file->move('images/avatar/' . $user->id . '/' , $file->getClientOriginalName()   ); 
		 	$user->avatar = $file->getClientOriginalName() ; 
		}
		 
		if ($user->updateUniques()  ) {
			if ($requests->hasFile('avatar')){
				$user->processAvatar() ; 
			}
			Notification::success('Successfull update user <a href="'.url('users/' . $user->id . '/edit').'">'.$user->name.'</a>');
			return redirect('users') ; 
		}else{  
			Notification::error( $user->errors()->All()); 
			return redirect('users/' . $user->id . '/edit')->withInput() ; 
		}
	}
 
}