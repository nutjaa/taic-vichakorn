@extends('layouts.admin')
@section('content') 
<div class="breadcrumbs">
    <ol class="breadcrumb">
        <li><a href="/"><i class="fa fa-home"></i>Home</a></li>
        <li><a href="/admin">Admin</a></li>
        <li><a href="/users">Users</a></li>
        <li class="active">Edit user - {{ $user->name }}</li>
    </ol>
</div>

<div class="innerLR">
    <div class="row manage-header">
        <div class="col-lg-7">
        	<h2>Edit user - {{ $user->name }}</h2>
        </div>
 	</div>
 	{!! Notification::showAll() !!}
    <div class="widget">
    {!! Form::model($user, array('url' => 'users/' . $user->id , 'method' => 'PUT'  , 'class'=>'form-horizontal' , 'role' => 'form'  , 'files'=>true )) !!}
        @include('admin/users/form')  
    {!! Form::close() !!}
    </div>
</div>

@stop