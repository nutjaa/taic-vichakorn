<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddNameToOrdersTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('orders', function(Blueprint $table)
		{
			$table->string('firstname');
			$table->string('lastname');
			$table->string('phone');
			$table->string('email');
			$table->text('address'); 
			
			$table->dropColumn('ordered_at');
			$table->dropColumn('getpaid_at');
			$table->dropColumn('shipping_at');
			$table->dropColumn('address_id');
			
			$table->decimal('shipping_price', 6, 2);
			$table->decimal('total_price', 6, 2); 
			
			$table->integer('status_id')->change();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('orders', function(Blueprint $table)
		{
			$table->dropColumn('firstname');
			$table->dropColumn('lastname');
			$table->dropColumn('phone');
			$table->dropColumn('email');
			$table->dropColumn('address');
			$table->dropColumn('shipping_price');
			$table->dropColumn('total_price');
			
			$table->dateTime('ordered_at');
			$table->dateTime('getpaid_at');
			$table->dateTime('shipping_at');
			$table->integer('address_id');
		});
	}

}
