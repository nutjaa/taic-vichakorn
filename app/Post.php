<?php namespace App;
use Carbon ; 

class Post extends \Riari\Forum\Models\Post {
  
  public function getUpdatedAtFormatAttribute(){ 
  	$date = Carbon::createFromFormat('Y-m-d H:i:s', $this->updated_at  );
  	$date->setTimezone('Asia/Bangkok');
  	return $date->format('d/m/Y H:i:s');
  }
}