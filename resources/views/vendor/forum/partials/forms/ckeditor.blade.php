{!! HTML::script('js/ckeditor/ckeditor.js'); !!}  
{!! HTML::script('js/ckeditor/adapters/jquery.js'); !!}  

<script type="text/javascript">
  CKEDITOR.config.toolbar = [
   ['Styles','Format','Font','FontSize'],
   '/',
   ['Bold','Italic','Underline','StrikeThrough','-','Undo','Redo','-','Cut','Copy','Paste','Find','Replace','-','Outdent','Indent','-','Print'],
   '/',
   ['NumberedList','BulletedList','-','JustifyLeft','JustifyCenter','JustifyRight','JustifyBlock'],
   ['Image','Table','-','Link','Youtube','Smiley','TextColor','BGColor']
] ;
  $( document ).ready(function() {
    $( 'textarea.editor' ).ckeditor();
  });
</script>