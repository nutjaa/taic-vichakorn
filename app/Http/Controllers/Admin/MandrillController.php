<?php namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Config ; 

class MandrillController extends Controller {
	public function __construct()
	{
		$this->middleware('auth');
	}
    
  public function info(){
    $key = Config::get('services.mandrill.secret');  
    $mandrill = new \Mandrill($key);
    
    $result = $mandrill->users->info(); 
    return view('admin.mandrill.info')->with('info',$result);
  }
  
  public function urlLists(){
    $key = Config::get('services.mandrill.secret');  
    $mandrill = new \Mandrill($key);    
    $result = $mandrill->urls->getList();
    return view('admin.mandrill.urllists')->with('lists',$result); ; 
  }
}