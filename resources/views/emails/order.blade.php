@extends('layouts.email') 

@section('content') 

<table width="100%" bgcolor="#e8eaed" cellpadding="0" cellspacing="0" border="0" id="backgroundTable">
   <tbody>
      <tr>
         <td>
            <table width="600" cellpadding="0" cellspacing="0" border="0" align="center" class="devicewidth">
               <tbody>
                  <tr>
                     <td width="100%">
                        <table bgcolor="#ffffff" width="600" cellpadding="0" cellspacing="0" border="0" align="center" class="devicewidth">
                           <tbody>
                              <!-- Spacing -->
                              <tr>
                                 <td width="100%" height="20"></td>
                              </tr>
                              <!-- Spacing -->
                              <tr>
                                 <td>
                                    <table width="560" align="center" cellpadding="0" cellspacing="0" border="0" class="devicewidthinner">
                                       <tbody>
                                          <!-- Title -->
                                          <tr><td align="left" style="font-family: Helvetica, arial, sans-serif; font-size: 14px;">สวัสดีค่ะ คุณ<strong style="color:#FFA70C;">{{$order->fullname}}</strong> ขอบคุณเป็นอย่างยิ่งที่ได้ซื้อสินค้าจากเรา</tr>
                                          <tr>
                                             <td height="15"></td>
                                          </tr> 
                                          <tr>
                             				<td align="left" style="background-color:#271E15;color:#FFF;font-size:13px;font-weight:bold;padding:0.5em 1em;">รายละเอียดการสั่งซื้อ</td>
										  </tr>
										  <tr>
                                             <td height="15"></td>
                                          </tr> 
										  <tr>
											<td align="left" style="font-family: Helvetica, arial, sans-serif; font-size: 14px;">
												รายการสั่งซื้อ: <strong><span style="color:#FFA70C;">#{{$order->orderNumber}}</span> วันที่ {{$order->createdAtFormat}}</strong>
												<br>วิธีการชำระเงิน: <strong>{{$order->paymenttype}}</strong>
											</td>
										  </tr>
										  <tr>
                                             <td height="15"></td>
                                          </tr> 
										  <tr>
											<td align="left">
												<table style="width:100%;font-family:Verdana,sans-serif;font-size:13px;color:#374953;">
													
													<tbody><tr style="background-color:#B9BABE;text-align:center;">
														<th style="width:15%;padding:0.6em 0;">อ้างอิง</th>
														<th style="width:35%;padding:0.6em 0;">สินค้า</th>
														<th style="width:15%;padding:0.6em 0;">ราคาต่อหน่วย</th>
														<th style="width:15%;padding:0.6em 0;">จำนวน</th>
														<th style="width:20%;padding:0.6em 0;">ราคารวม</th>
													</tr>
														@foreach($order->items as $item)
														<tr style="background-color:#EBECEE;">
															<td style="padding:0.6em 0.4em;">{{$item->product->sku}}</td>
															<td style="padding:0.6em 0.4em;"><strong>{!!$item->product->name!!}</strong></td>
															<td style="padding:0.6em 0.4em;text-align:right;">{{$item->price}} ฿</td>
															<td style="padding:0.6em 0.4em;text-align:center;">{{$item->quantity}}</td>
															<td style="padding:0.6em 0.4em;text-align:right;">{{$item->price * $item->quantity}} ฿</td>
														</tr>
														@endforeach
													
													<tr style="text-align:right;">
														<td>&nbsp;</td>
														<td style="background-color:#B9BABE;padding:0.6em 0.4em;" colspan="3">จำนวนสินค้า</td>
														<td style="background-color:#B9BABE;padding:0.6em 0.4em;">{{$order->total_price}} ฿</td>
													</tr> 
													@if($order->discount > 0 ) 
                          <tr style="text-align:right;">
														<td>&nbsp;</td>
														<td style="background-color:#B9BABE;padding:0.6em 0.4em;" colspan="3">ส่วนลด VIP 10%</td>
														<td style="background-color:#B9BABE;padding:0.6em 0.4em;">{{$order->discount}} ฿</td>
													</tr>  
                          @endif
													<tr style="text-align:right;">
														<td>&nbsp;</td>
														<td style="background-color:#DDE2E6;padding:0.6em 0.4em;" colspan="3">ค่าจัดส่ง</td>
														<td style="background-color:#DDE2E6;padding:0.6em 0.4em;">{{$order->shipping_price}} ฿</td>
													</tr>
                          @if($order->used_point > 0 ) 
                          <tr style="text-align:right;">
														<td>&nbsp;</td>
														<td style="background-color:#B9BABE;padding:0.6em 0.4em;" colspan="3">ใช้แต้มสะสม</td>
														<td style="background-color:#B9BABE;padding:0.6em 0.4em;">{{$order->used_point}} ฿</td>
													</tr>  
                          @endif
													<tr style="text-align:right;font-weight:bold;">
														<td>&nbsp;</td>
														<td style="background-color:#B9BABE;padding:0.6em 0.4em;" colspan="3">จำนวนเงินรวม</td>
														<td style="background-color:#B9BABE;padding:0.6em 0.4em;">{{$order->grandTotal}} ฿</td>
													</tr>
												</tbody></table>
											</td>
										  </tr>
										  <tr>
                                             <td height="15"></td>
                                          </tr>
										  <tr>
                             				<td align="left" style="background-color:#271E15;color:#FFF;font-size:13px;font-weight:bold;padding:0.5em 1em;">วิธีการจัดส่ง</td>
										  </tr>
										  <tr>
                                             <td height="15"></td>
                                          </tr>
                                          <tr>
											<td align="left" style="font-family: Helvetica, arial, sans-serif; font-size: 14px;">
												โดย: <strong>Thailand Post (ไปรษณีย์ไทย) - EMS (พัสดุด่วน)</strong>
											</td>
										  </tr>
										  <tr>
                                             <td height="15"></td>
                                          </tr>
                                          <tr>
											<td>
												<table style="width:100%;font-family:Verdana,sans-serif;font-size:13px;color:#374953;">
													<tbody><tr style="background-color:#B9BABE;text-transform:uppercase;">
														<th style="text-align:left;padding:0.3em 1em;">สถานที่จัดส่ง</th>
														<th style="text-align:left;padding:0.3em 1em;">สถานที่ใบกำกับสินค้า</th>
													</tr>
													<tr>
														<td style="padding:0.5em 0 0.5em 0.5em;background-color:#EBECEE;">
															<p>{{$order->fullname}}</p>
															{!!nl2br($order->address)!!}
															<p>{{$order->phone}}</p>
														</td>
														<td style="padding:0.5em 0 0.5em 0.5em;background-color:#EBECEE;">
															<p>{{$order->fullname}}</p>
															{!!nl2br($order->address)!!}
															<p>{{$order->phone}}</p>
														</td>
													</tr>
												</tbody></table>
											</td>
										  </tr>
										  <tr>
                                             <td height="15"></td>
                                          </tr>
                                          <tr>
											<td align="left" style="font-family: Helvetica, arial, sans-serif; font-size: 14px;">
												คุณสามารถตรวจสอบรายการสั่งซื้อ ได้ที่ <a target="_blank" style="color:#FFA70C;font-weight:bold;text-decoration:none;" href="{{Config::get('app.url')}}orders/{{ $order->id }}">"Order history"</a> บน Website
											</td>
										  </tr>
                                       </tbody>
                                    </table>
                                 </td>
                              </tr>
                              <tr>
                                 <td width="100%" height="20"></td>
                              </tr>
                           </tbody>
                        </table>
                     </td>
                  </tr>
               </tbody>
            </table>
         </td>
      </tr>
   </tbody>
</table>
@endsection