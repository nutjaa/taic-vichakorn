<?php namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\Comment;
use Auth ; 

use Krucas\Notification\Facades\Notification;

class CommentsController extends Controller {
  public function store(Request $requests){ 
    $url = $requests->input('url','') ;
    if(Auth::guest()){
      Notification::error("กรุณาล็อคอินเพื่อทำการ comment") ;
      return redirect($url) ; 
    }
    
    $body = $requests->input('body','');
    if(trim($body) == ''){
      Notification::error("กรุณาระบุข้อความ") ;
      return redirect($url . '#post-box') ; 
    }
    
    $comment = new Comment();
    $comment->user_id = Auth::user()->id ; 
    $comment->body = $body ; 
    $comment->parent_id = $requests->input('parent_id') ; 
    $comment->commentable_id = $requests->input('commentable_id') ; 
    $comment->commentable_type = $requests->input('commentable_type') ; 
    $comment->save() ; 
    
    return redirect($url . '#post-' . $comment->id) ; 
  }
  
  public function destroy($id){
    $ret = array('success' => false) ;
    $comment = Comment::find($id) ;  
    if(! Auth::guest() && $comment->user_id == Auth::user()->id){ 
      $comment->delete() ;   
      $ret['success'] = true ; 
    }
    return $ret ; 
  }
}