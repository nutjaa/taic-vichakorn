<?php

return [
    'meta'      => [
        /*
         * The default configurations to be used by the meta generator.
         */
        'defaults'       => [
            'title'       => "Luminance Cocktail", // set false to total remove
            'description' => 'ครีมลูมิแนนซ์ ค็อกเทล', // set false to total remove
            'separator'   => ' - ',
            'keywords'    => ['ครีม','บำนุงผิว','สวย'],
        ],

        /*
         * Webmaster tags are always added.
         */
        'webmaster_tags' => [
            'google'    => null,
            'bing'      => null,
            'alexa'     => null,
            'pinterest' => null,
            'yandex'    => null
        ]
    ],
    'opengraph' => [
        /*
         * The default configurations to be used by the opengraph generator.
         */
        'defaults' => [
            'title'       => 'Luminance Cocktail', // set false to total remove
            'description' => 'ครีมลูมิแนนซ์ ค็อกเทล', // set false to total remove
            'url'         => env('WEB_URL'),
            'type'        => 'articles',
            'site_name'   => 'Luminance Cocktail',
            'images'      => [env('WEB_URL').'images/logo.png'],
            'locale'    => 'th-TH'
        ]
    ],
    'twitter' => [
        /*
         * The default values to be used by the twitter cards generator.
         */
        'defaults' => [
          //'card'        => 'summary',
          //'site'        => '@LuizVinicius73',
        ]
    ]
];
