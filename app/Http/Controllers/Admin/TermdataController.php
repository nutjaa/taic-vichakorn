<?php namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Termdata;
use App\Vocabulary;

use Krucas\Notification\Facades\Notification;

class TermdataController extends Controller {
	/**
	 * Create a new controller instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		$this->middleware('auth');
	}
	
	public function index(Request $requests)
	{ 
		$q = $requests->input('q') ;  
		$terms = Termdata::search($q)->with('vocabulary')->orderBy('name','ASC')->paginate(15)  ; 
		
        return view('admin.termdata.index')->with('terms' , $terms)->with('q',$q) ; 
	}
    
    public function create(){
        $vocabulary_options = Vocabulary::lists('name','id');
        $vocabulary_options = array(''=> 'Please select') + $vocabulary_options  ;  
		return view('admin.termdata.create')->with('vocabulary_options' , $vocabulary_options);
	}
	
    public function store(Request $requests){
		$term = new Termdata() ;
		$term->name = $requests->input('name');
        $term->label = $requests->input('label');
        $term->vid = $requests->input('vid');
		$term->description = $requests->input('description'); 
        $term->pid = $requests->input('pid'); 
		 
		if ($term->save()) {
			Notification::success('Successfull create term <a href="'.url('termdata/' . $term->id . '/edit').'">'.$term->name.'</a>');
			return redirect('termdata') ; 
		}else{ 
			Notification::error( $term->errors()->All());
			return redirect('termdata/create')->withInput() ; 
		}
	}
	
	public function edit(Request $requests , $id)
	{ 
	    $vocabulary_options = Vocabulary::lists('name','id'); 
		$term = Termdata::find($id);
		
		$term_options = Termdata::GenerateOptionByVid($term->vid , $term->id) ; 
 
 
		return view('admin.termdata.edit')
			->with('term', $term)
			->with('term_options',$term_options) 
            ->with('vocabulary_options',$vocabulary_options);
	}
	
	public function update(Request $requests,$id)
	{
		$term = Termdata::find($id);
		$term->name = $requests->input('name');
        $term->label = $requests->input('label');
        $term->vid = $requests->input('vid');
		$term->description = $requests->input('description'); 
        $term->pid = $requests->input('pid'); 
		 
		if ($term->updateUniques()) {
			Notification::success('Successfull update term <a href="'.url('termdata/' . $term->id . '/edit').'">'.$term->name.'</a>');
			return redirect('termdata') ; 
		}else{ 
			Notification::error( $term->errors()->All());
			return redirect('termdata/' . $term->id . '/edit')->withInput() ; 
		} 
	}
	
	//custom
	public function options($vid,$tid){
		$term_options = Termdata::GenerateOptionByVid($vid ,$tid) ; 
		echo json_encode($term_options);
		die() ; 
	}
}
	