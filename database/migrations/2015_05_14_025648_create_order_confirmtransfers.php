<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOrderConfirmtransfers extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('order_confirmtransfers', function(Blueprint $table)
		{
			$table->increments('id');
			$table->timestamps();
			
			$table->integer('order_id');
			
			$table->string('method',50);
			$table->string('frombank',100);
			$table->string('tobank',100);
			$table->decimal('amount', 6, 2); 
			$table->dateTime('transfered_at');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('order_confirmtransfers');
	}

}
