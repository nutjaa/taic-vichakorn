<div class="table-responsive">
<table class="table">
  <thead>
		<tr>
			<th width="45">ID</th>
			<th>Image</th>
			<th>Name</th>
			<th>Message</th> 
			<th>Updated</th>  
			<th colspan="2">Actions</th>
		</tr>
	</thead>
	<tbody>
	@foreach($comments as $comment)
		<tr>
			<td>{{ $comment->id }}</td>
			<td><img src="/images/{{ $comment->user->avatarpath() }}" class="img-thumbnail" style="width: 50px;"/></td>
			<td>{{ $comment->user->username }}</td>
      <td>{!! nl2br($comment->body) !!}</td> 
			<td>{{ $comment->updatedAtFormat }}</td> 
			<td>
				<a class="btn btn-small btn-info" href="{{ URL::to($url.$comment->id ) }}">Edit</a>  
			</td>
      <td>
        {!! Form::open(array('route' => array('comments.destroy', $comment->id), 'method' => 'delete' , 'style'=>'padding:0px')) !!}
          <button type="submit" class="btn btn-danger btn-mini">Delete</button>
          <input type="hidden" name="redirect_url" value="{{$url}}" />
        {!! Form::close() !!}
      </td>
		</tr>
	@endforeach
	</tbody>
</table> 
</div>