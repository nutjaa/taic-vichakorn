<?php namespace App\Http\Controllers;

use App\Page;
use App\Pagecategory;
use App\Vocabulary ; 
use App\Termdata ; 

use SEOMeta;
use OpenGraph;

class AboutusController extends Controller { 
 	public $vocab  ; 
 	public $termdata;
 	
 	public function __construct()
	{
		$this->vocab = Vocabulary::Page()->first() ; 
		$this->termdata = Termdata::where('name' , '=' , 'aboutus')->where('vid' , '=' , $this->vocab->id )->first() ; 
	 
	}

	/**
	 * Show the application dashboard to the user.
	 *
	 * @return Response
	 */
	public function index()
	{ 
		#\DB::enableQueryLog();
		$page = Page::join('page_categories',function($join){
			$join->on('page_categories.page_id','=','pages.id') ; 
		})->where('page_categories.term_id','=',$this->termdata->id)->active()->orderBy('pages.order', 'ASC')->first()  ; 
		#print_r(\DB::getQueryLog());die() ; 
		return redirect('aboutus/'.$page->sefu) ;
	}
	
	public function view($sefu){
	  $page = Page::active()->sefu($sefu)->first() ; 
		if(! is_object($page)){
			return Redirect('aboutus') ; 
		}
    
    SEOMeta::setTitle('เกี่ยวกับเรา');
    SEOMeta::setDescription($page->title);
    
    OpenGraph::setDescription($page->title);
    OpenGraph::setTitle('เกี่ยวกับเรา');
    OpenGraph::setUrl(url('/aboutu/'.$sefu));   
    
		$pages = Page::join('page_categories',function($join){
			$join->on('page_categories.page_id','=','pages.id') ; 
		})->where('page_categories.term_id','=',$this->termdata->id)->active()->orderBy('pages.order', 'ASC')->get()  ;  
		return view('about')->with('page',$page)->with('pages',$pages) ; 
	}

}
