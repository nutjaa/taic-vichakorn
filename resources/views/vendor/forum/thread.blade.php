@extends('layouts.master')

@section('content')
@include('forum::partials.breadcrumbs')
<div class="webboard">
<h2>
    @if ($thread->locked)
        [{{ trans('forum::base.locked') }}]
    @endif
    @if ($thread->pinned)
        [{{ trans('forum::base.pinned') }}]
    @endif
    {{{ $thread->title }}}

</h2>

@if (!Auth::guest() && ($thread->canLock || $thread->canPin || $thread->canDelete) && Auth::user()->isAdmin() )
    <div class="thread-tools dropdown">
        <button class="btn btn-default dropdown-toggle" type="button" id="thread-actions" data-toggle="dropdown" aria-expanded="true">
            {{ trans('forum::base.actions') }}
            <span class="caret"></span>
        </button>
        <ul class="dropdown-menu" role="menu">
            @if ($thread->canLock)
                <li><a href="{{ $thread->lockRoute }}" data-method="post">{{ trans('forum::base.lock_thread') }}</a></li>
            @endif
            @if ($thread->canPin)
                <li><a href="{{ $thread->pinRoute }}" data-method="post">{{ trans('forum::base.pin_thread') }}</a></li>
            @endif
            @if ($thread->canDelete)
                <li><a href="{{ $thread->deleteRoute }}" data-confirm data-method="delete">{{ trans('forum::base.delete_thread') }}</a></li>
            @endif
        </ul>
    </div>
    <hr>
@endif

@if ($thread->canReply)
    <div class="btn-group" role="group">
        <a href="{{ $thread->replyRoute }}" class="btn btn-default">{{ trans('forum::base.new_reply') }}</a>
        <a href="#quick-reply" class="btn btn-default">{{ trans('forum::base.quick_reply') }}</a>
        <br /><br />
    </div>
@endif

@foreach ($thread->postsPaginated as $post)
	<div class="panel thread-panel">
		<div class="panel-body">
			@include('forum::partials.post', compact('post'))
		</div>
	</div>
    
@endforeach

{!! $thread->pageLinks !!}

@if ($thread->canReply)
    <h3>{{ trans('forum::base.quick_reply') }}</h3>
    <div id="quick-reply">
        @include(
            'forum::partials.forms.post',
            array(
                'form_url'            => $thread->replyRoute,
                'form_classes'        => '',
                'show_title_field'    => false,
                'post_content'        => '',
                'submit_label'        => trans('forum::base.reply'),
                'cancel_url'          => ''
            )
        )
    </div>
@else
  <a href="/auth/login?redirectTo={{ $thread->route }}" class="btn btn-primary">กรุณา Log In เพื่อตอบกระทู้</a><br /><br />
@endif
</div>
@overwrite

@section('footerjs')  
<script type="text/javascript">
$( document ).ready(function() {
  $('a[data-confirm]').click(function(event) {
      if (!confirm('{!! trans('forum::base.generic_confirm') !!}')) {
          event.stopImmediatePropagation();
          event.preventDefault();
      }
  });
  $('[data-method]:not(.disabled)').click(function(event) { 
      $('<form action="' + $(this).attr('href') + '" method="POST">' +
      '<input type="hidden" name="_method" value="' + $(this).data('method') + '">' +
      '<input type="hidden" name="_token" value="{!! Session::getToken() !!}"' + '>' +
      '</form>').appendTo(document.body).submit() ;
	
      event.preventDefault();
  });
});
</script>
@include('forum::partials.forms.ckeditor') 
@stop


