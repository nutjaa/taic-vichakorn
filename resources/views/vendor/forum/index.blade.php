@extends('layouts.master')

@section('content')
@include('forum::partials.breadcrumbs')
<div class="webboard">
<h2>{{ trans('forum/base.index') }}</h2> 
@foreach ($categories as $category)
	@if( $category->weight == 0 )
	<table class="table table-index">
	    <thead>
	        <tr>
	            <td colspan="4">
	                <p class="lead"><a href="{{ $category->Route }}">{{ $category->title }}</a></p> 
	            </td>
	        </tr>
	    </thead>
	    <tbody>
	        <tr class="blue-row">
	            <th class="col-md-6">{{ trans('forum/base.last_5_thread') }}</th>
	            <th class="col-md-4">{{ trans('forum/base.last_post') }}</th>
	            <th class="col-md-1 text-center">{{ trans('forum/base.total_posts') }}</th>
	            <th class="col-md-1 text-center">{{ trans('forum/base.total_thread_view') }}</th>
	        </tr> 
	        @foreach ($category->threadsTop5 as $thread)
	        <tr>
	            <td>
	                <a href="{{ $thread->route }}">{{ $thread->title }}</a>
             	</td>
             	<td>{{ str_limit(strip_tags($thread->lastPost->content),20) }} 
	                <h6>{{ trans('forum/base.by') . ' ' . $thread->lastPost->author->username}} <span class="text-muted">{{ $thread->lastPost->updated }}</span></h6>
              	</td>
	            <td class="text-center">{{ $thread->replyCount }}</td>
	            <td class="text-center">{{ $thread->views }}</td>
	        </tr>
	        @endforeach 
	    </tbody>
	</table>
	<table class="table table-index">
		<thead class="blue-row">
	        <tr>
	            <th class="col-md-10">{{ trans('forum/base.room') }}</th>
	            <th class="col-md-1 text-center">{{ trans('forum/base.threads') }}</th> 
	            <th class="col-md-1 text-center">{{ trans('forum/base.total_posts') }}</th>
	        </tr>
    	</thead>
		<tbody>
	@else
		<tr >
			<td><p class="lead"><a href="{{ $category->Route }}">{{ $category->title }}</a></p></td> 
    	<td class="text-center">{{ $category->threadCount }}</td>
    	<td class="text-center">{{ $category->postCount }}</td>
		</tr>
	@endif
@endforeach
		</tbody>
	</table>
</div>
@overwrite
