@extends('layouts.admin')
@section('content') 
<div class="breadcrumbs">
    <ol class="breadcrumb">
        <li><a href="/"><i class="fa fa-home"></i>Home</a></li>
        <li><a href="/admin">Admin</a></li>
        <li><a href="/admindocs">Documents</a></li>
        <li class="active">{{ $page->title }}</li>
    </ol>
</div>

<div class="innerLR">
  <div class="row manage-header">
    <div class="col-md-12">
      <h2>{{ $page->title }}</h2>
    </div>
  </div>
  <div class="widget">
    {!! $page->body !!}
  </div>
</div>

@stop