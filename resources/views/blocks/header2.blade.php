 
<nav class="navbar navbar-default navbar-fixed-top" id="header2">
    <div class="container">
    <div class="navbar-header">
      <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>
      <a class="navbar-brand" href="/">Luminance Cocktail</a>
    </div>
    <div id="navbar" class="navbar-collapse collapse">
      <ul class="nav navbar-nav"> 
        
        <li class="dropdown @if (Request::is('product/*') || Request::is('products') || Request::is('ordering-and-how-to-use/*') || Request::is('wonders-of-use/*') || Request::is('order/*') || Request::is('order') || Request::is('moneytransfer') ) active  @endif">
          <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">ผลิตภัณฑ์ <span class="caret"></span></a>
          <ul class="dropdown-menu" role="menu">
            <li @if (Request::is('product/*') || Request::is('products')) class="active" @endif><a href="/products">รายละเอียดผลิตภัณฑ์</a></li>
			<li @if (Request::is('ordering-and-how-to-use/*')) class="active" @endif><a href="/ordering-and-how-to-use">ลำดับและวิธีการใช้ผลิตภัณฑ์</a></li>
			<li @if (Request::is('wonders-of-use/*')) class="active" @endif><a href="/wonders-of-use">มหัศจรรย์แห่งการใช้ผลิตภัณฑ์  Luminance Cocktail ครบเซ็ต</a></li>
            <li class="divider"></li> 
            <li @if (Request::is('order/*') || Request::is('order')) class="active" @endif><a href="/order">สั่งซื้อผลิตภัณฑ์</a></li>
            <li @if (Request::is('moneytransfer')) class="active" @endif><a href="/moneytransfer">แจ้งโอนเงิน</a></li>
          </ul>
          
        </li>
        <li @if (Request::is('aboutus/*') || Request::is('aboutus')) class="active" @endif><a href="/aboutus"  >เกี่ยวกับเรา  </a></li>
        <li @if (Request::is('contactus/*') || Request::is('contactus')) class="active" @endif><a href="/contactus">ติดต่อเรา</a></li>
      </ul>
      <ul class="nav navbar-nav navbar-right">
      	@if( Auth::guest() )
        <li @if (Request::is('auth/register') || Request::is('auth/register')) class="active" @endif><a href="/auth/register">สมัครสมาชิก</a></li>
        <li @if (Request::is('auth/login') || Request::is('auth/login')) class="active" @endif><a href="/auth/login">เข้าสู่ระบบ</a></li> 
        @else
        <li @if (Request::is('home')) class="active" @endif><a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">{{ Auth::user()->email }}@if(Auth::user()->is_vip)<i class="fa fa-star"></i>@endif [{{ Auth::user()->point }}] <span class="caret"></span></a>
          <ul class="dropdown-menu" role="menu">
            <li @if (Request::is('home')) class="active" @endif><a href="/home">ข้อมูล</a></li>
            <li @if (Request::is('orders/*') || Request::is('orders')) class="active" @endif><a href="/orders">ประวัติการสั่งซื้อ</a></li> 
            <li class="divider"></li>  
            <li><a href="/auth/logout">ออกจากระบบ</a></li>
          </ul>
	  	</li>
        @endif
      </ul>
    </div><!--/.nav-collapse -->
    </div>
    </nav>