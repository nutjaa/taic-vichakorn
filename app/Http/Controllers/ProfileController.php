<?php namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;

use Auth;
use Validator;
use Hash;
use File;
use Carbon;

use Krucas\Notification\Facades\Notification;

class ProfileController extends Controller {

	/*
	|--------------------------------------------------------------------------
	| Home Controller
	|--------------------------------------------------------------------------
	|
	| This controller renders your application's "dashboard" for users that
	| are authenticated. Of course, you are free to change or remove the
	| controller as you wish. It is just here to get your app started!
	|
	*/

	/**
	 * Create a new controller instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		$this->middleware('auth');
	}
    
  public function edit(){
    return view('profile.edit') ; 
  }
    
  public function update(Request $requests){
    if( trim($requests->input('password')) != ''){
      $rules = array(
			 'repassword'    => 'required|min:4|same:password', 
			 'password' => 'required|min:4|same:repassword' 
		  );
      $validator = Validator::make($requests->all(), $rules);  	
          
      if( $validator->fails() ){
        Notification::error( $validator->errors()->all() ); 
        return redirect('profile/edit')->withInput() ;
      }
    }
        
    $user = User::find(Auth::user()->id);
		$user->name = $requests->input('name');
		$user->email = $requests->input('email');
		$user->phone = $requests->input('phone');
		$user->gender = $requests->input('gender'); 
    #$user->user_role_id = $requests->input('user_role_id');
		#$user->status = $requests->input('status',0); 
    $user->lastname = $requests->input('lastname');
    $user->username = $requests->input('username');
    $user->address = $requests->input('address');
    $user->is_subscribe = $requests->input('is_subscribe',0);
    $user->lineid = $requests->input('lineid');
    $user->facebookname = $requests->input('facebookname');
    $birthdate = Carbon::createFromFormat('d/m/Y', $requests->input('birthdate')) ; 
    $user->birthdate = $birthdate->toDateTimeString() ; 
        
    if( trim($requests->input('password')) != '')
      $user->password =  Hash::make($requests->input('password')) ;  
    
       
    if ($requests->hasFile('avatar')){
			$file = $requests->file('avatar'); 
		 	$file->move('images/avatar/' . $user->id . '/' , $file->getClientOriginalName()   ); 
		 	$user->avatar = $file->getClientOriginalName() ; 
		}
		 
		if ($user->updateUniques()  ) {
			if ($requests->hasFile('avatar')){ 
				$user->processAvatar() ; 
			}
			Notification::success('ระบบได้ทำการแก้ไขข้อมูลของท่านเรียบร้อย');
			return redirect('home') ; 
		}else{  
			Notification::error( $user->errors()->All()); 
			return redirect('profile/edit')->withInput() ; 
		}
  }
}
