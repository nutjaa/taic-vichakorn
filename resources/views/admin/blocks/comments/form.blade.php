{!! Form::model($comment, array('url' => 'comments/'. $comment->id , 'method' => 'PUT'  , 'class'=>'form-horizontal' , 'role' => 'form'  , 'files'=>true )) !!}
  <div class="form-group">
  	{!! Form::label('email', 'Email' , array('class'=>'col-sm-2 control-label')) !!}
  	<div class="col-sm-8">
  		<span class="form-control">{{ $comment->user->email }}</span>
  	</div>
  </div>	
  <div class="form-group">
  	{!! Form::label('username', 'Usernmae' , array('class'=>'col-sm-2 control-label')) !!}
  	<div class="col-sm-8">
  		<span class="form-control">{{ $comment->user->username }}</span>
  	</div>
  </div>	
  <div class="form-group">
  	{!! Form::label('body', 'Message' , array('class'=>'col-sm-2 control-label')) !!}
  	<div class="col-sm-8">
  		{!! Form::textarea('body', Input::old('body'), array('class' => 'form-control')) !!}
  	</div>
  </div>	
  <div class="form-group">
  	{!! Form::label('updated_at', 'Updated' , array('class'=>'col-sm-2 control-label')) !!}
  	<div class="col-sm-8">
  		<input type="text" class="form-control" name="updated_at" value="{{ $comment->updatedAtFormat }}" />
  	</div>
  </div>
  
  <div class="form-group">
      <div class="col-sm-offset-2 col-sm-8">
  		{!! Form::submit('Save', array('class' => 'btn btn-primary')) !!}
  		<a href="{{ URL::to($redirect_url) }}">&nbsp;Cancel</a>
      <input type="hidden" name="redirect_url" value="{{ $redirect_url }}" />
  	</div>
  </div>
{!! Form::close() !!}