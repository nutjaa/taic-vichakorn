<div class="form-group">
	{!! Form::label('title', 'Title' , array('class'=>'col-sm-2 control-label')) !!}
	<div class="col-sm-8">
		{!! Form::text('title', Input::old('title'), array('class' => 'form-control')) !!}
	</div>
</div>	
<div class="form-group">
	{!! Form::label('sefu', 'Sefu' , array('class'=>'col-sm-2 control-label')) !!}
	<div class="col-sm-8">
		{!! Form::text('sefu', Input::old('sefu'), array('class' => 'form-control')) !!}
	</div>
</div> 
<div class="form-group">
	{!! Form::label('body', 'Body' , array('class'=>'col-sm-2 control-label')) !!}
	<div class="col-sm-8">
		{!! Form::textarea('body', Input::old('body'), array('class' => 'form-control ckeditor')) !!}
	</div>
</div>
<div class="form-group">
    <div class="col-sm-offset-2 col-sm-8">
      <div class="checkbox">
        <label>
          {!! Form::checkbox('status' , 1) !!} Status
        </label>
      </div>
    </div>
</div> 
<div class="form-group">
    <div class="col-sm-offset-2 col-sm-8">
      <div class="checkbox">
        <label>
          {!! Form::checkbox('has_comments' , 1) !!} Has comments
        </label>
      </div>
    </div>
</div> 
<div class="form-group">
	{!! Form::label('order', 'Order' , array('class'=>'col-sm-2 control-label')) !!}
	<div class="col-sm-8">
		{!! Form::text('order', Input::old('order'), array('class' => 'form-control')) !!}
	</div>
</div> 
<div class="form-group">
	{!! Form::label('lang', 'Lang' , array('class'=>'col-sm-2 control-label')) !!}
	<div class="col-sm-8">
		{!! Form::select('lang',$lang_options,Input::old('lang'), array('class' => 'form-control')) !!}
	</div>
</div>
<div class="form-group">
	{!! Form::label('category_id', 'Category' , array('class'=>'col-sm-2 control-label')) !!}
	<div class="col-sm-8">
		@if( isset($page) && is_object($page))
			@foreach($page->categories as $category)
        		{!! Form::select('category['.$category->id.']',$category_options,$category->term_id, array('class' => 'form-control')) !!}
        	@endforeach
        	<hr />
        	@for ($i = 0; $i < 2; $i++)
        		{!! Form::select('category[N'.$i.']',$category_options,Input::old('category[N'.$i.']'), array('class' => 'form-control')) !!}
        	@endfor 
		@else
		{!! Form::select('category[0]',$category_options,Input::old('category[0]'), array('class' => 'form-control')) !!}
        {!! Form::select('category[1]',$category_options,Input::old('category[1]'), array('class' => 'form-control')) !!}
        {!! Form::select('category[2]',$category_options,Input::old('category[2]'), array('class' => 'form-control')) !!}
        @endif
	</div>
</div>
<div class="form-group">
    <div class="col-sm-offset-2 col-sm-8">
		{!! Form::submit('Save', array('class' => 'btn btn-primary')) !!}
		<a href="{{ URL::to('pages/') }}">&nbsp;Cancel</a>
	</div>
</div>