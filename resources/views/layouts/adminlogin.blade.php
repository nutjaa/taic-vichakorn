<!doctype html>
<html lang="en">
<head>
	<meta charset="UTF-8"/>
	<meta name="viewport" content="width=device-width, initial-scale=1"/>
	<title>TAIC CMS - Vichakorn</title>  
	<link href="{{ asset('/css/bootstrap.css') }}" rel="stylesheet">
	<link href="{{ asset('/css/font-awesome.min.css') }}" rel="stylesheet">
	<link href="{{ asset('/css/admin.css') }}" rel="stylesheet"> 
	<link href='http://fonts.googleapis.com/css?family=Lato|Lobster+Two&subset=latin&v2' rel='stylesheet' type='text/css'/>
	<link type="text/css" rel="stylesheet" href="//fonts.googleapis.com/css?family=Oswald:300normal,400normal,700normal|Open+Sans:400normal|Droid+Sans:400normal|Pacifico:400normal|Lato:400normal|Josefin+Slab:400normal|Roboto:400normal|Open+Sans+Condensed:300normal|PT+Sans+Narrow:400normal|Allura:400normal|Merriweather:400normal&amp;subset=all"/>
	@yield('headercss')
</head>
<body class="login">
	@yield('content')  
    <script src="/js/jquery-1.11.2.min.js"></script>
    <script src="/js/bootstrap.min.js"></script>
    @yield('footerjs')
</body>
</html>