<?php namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Order;
use App\Termdata; 
use App\Vocabulary;
use App\OrderStatus;

use Krucas\Notification\Facades\Notification; 

class OmisesController extends Controller {
	public function __construct()
	{
		$this->middleware('auth');
	}
	
	public function index(Request $requests){ 
		$page = $requests->input('page' , 1 ); 
		$id = '?offset='.(( $page -1 ) *15).'&limit=15' ;
		$charges = \OmiseCharge::retrieve($id) ;  
        return view('admin.omise.index')->with('charges' , $charges)->with('page',$page) ; 
	}
	
	public function show(Request $requests , $id){
        $charge = \OmiseCharge::retrieve($id) ;  
        print_r($charge) ; die() ; 
        if(is_null($charge)){
            Notification::error('Not found.');
            return redirect('charges') ; 
        }
        return view('admin.omise.show')->with('charge',$charge);
    }
}