<div class="form-group">
	{!! Form::label('username', 'Username *' , array('class'=>'col-sm-2 control-label')) !!}
	<div class="col-sm-8">
		{!! Form::text('username', Input::old('username'), array('class' => 'form-control')) !!}
	</div>
</div>
<div class="form-group">
	{!! Form::label('name', 'Firstname *' , array('class'=>'col-sm-2 control-label')) !!}
	<div class="col-sm-8">
		{!! Form::text('name', Input::old('name'), array('class' => 'form-control')) !!}
	</div>
</div>
<div class="form-group">
	{!! Form::label('lastname', 'Lastname *' , array('class'=>'col-sm-2 control-label')) !!}
	<div class="col-sm-8">
		{!! Form::text('lastname', Input::old('lastname'), array('class' => 'form-control')) !!}
	</div>
</div>
<div class="form-group">
	{!! Form::label('email', 'Email *' , array('class'=>'col-sm-2 control-label')) !!}
	<div class="col-sm-8">
		{!! Form::text('email', Input::old('email'), array('class' => 'form-control')) !!}
	</div>
</div>
<div class="form-group">
	{!! Form::label('password', 'Password' , array('class'=>'col-sm-2 control-label')) !!}
	<div class="col-sm-8">
		{!! Form::password('password' , array('class' => 'form-control')) !!}
	</div>
</div>
<div class="form-group">
	{!! Form::label('repassword', 'Re-Password' , array('class'=>'col-sm-2 control-label')) !!}
	<div class="col-sm-8">
		{!! Form::password('repassword' , array('class' => 'form-control')) !!}
	</div>
</div>
<div class="form-group">
	{!! Form::label('phone', 'Phone' , array('class'=>'col-sm-2 control-label')) !!}
	<div class="col-sm-8">
		{!! Form::text('phone', Input::old('phone'), array('class' => 'form-control')) !!}
	</div>
</div>
<div class="form-group">
	{!! Form::label('gender', 'Gender' , array('class'=>'col-sm-2 control-label')) !!}
	<div class="col-sm-8">
		{!! Form::select('gender', array('M' => 'Male','F' => 'Female'),Input::old('gender' , 'M'), array('class' => 'form-control')) !!}
	</div>
</div> 

<div class="form-group">
	{!! Form::label('user_role_id', 'Role *' , array('class'=>'col-sm-2 control-label')) !!}
	<div class="col-sm-8">
		{!! Form::select('user_role_id',$role_options,Input::old('user_role_id'), array('class' => 'form-control')) !!}
	</div> 
</div>

<div class="form-group">
	{!! Form::label('avatar', 'Avatar' , array('class'=>'col-sm-2 control-label')) !!}
	<div class="col-sm-8">
		<div style="position:relative;">
	        <a class='btn btn-primary' href='javascript:;'>
	            Choose File...
	            <input type="file" style='position:absolute;z-index:2;top:0;left:0;filter: alpha(opacity=0);-ms-filter:"progid:DXImageTransform.Microsoft.Alpha(Opacity=0)";opacity:0;background-color:transparent;color:transparent;' name="avatar" size="40"  onchange='$("#upload-file-info").html($(this).val());'>
	        </a>
	        &nbsp;
	        <span class='label label-info' id="upload-file-info"></span>
		</div> 
	</div> 
</div>

@if ( isset($user) && $user->avatar )
<div class="form-group">
	<div class="col-sm-2"></div>
	<div class="col-sm-8">
		<img src="/images/avatar/{{ $user->id }}/{{ $user->avatar }}"  />
	</div>
</div>
@endif

<div class="form-group">
    <div class="col-sm-offset-2 col-sm-8">
      <div class="checkbox">
        <label>
          {!! Form::checkbox('status' , 1) !!} Status
        </label>
      </div>
    </div>
</div>

<div class="form-group">
    <div class="col-sm-offset-2 col-sm-8">
      <div class="checkbox">
        <label>
          {!! Form::checkbox('is_vip' , 1) !!} V.I.P
        </label>
      </div>
    </div>
</div>

<div class="form-group">
	{!! Form::label('birthdate', 'Birth date' , array('class'=>'col-sm-2 control-label')) !!}
	<div class="col-md-3"> 
        <div class='input-group date' id='birthdate'>
            <input type='text' class="form-control" name="birthdate" required value="@if(isset($user)){{ $user->birthDateFormatted }} @endif" /> 
            <span class="input-group-addon">
                <span class="glyphicon glyphicon-calendar"></span>
            </span>
        </div> 
	</div>
</div>

<div class="form-group">
	{!! Form::label('address', 'Address' , array('class'=>'col-sm-2 control-label')) !!}
	<div class="col-sm-8">
		{!! Form::textarea('address', Input::old('address'), array('class' => 'form-control' )) !!}
	</div>
</div>

<div class="form-group">
    <div class="col-sm-offset-2 col-sm-8">
      <div class="checkbox">
        <label>
          {!! Form::checkbox('is_subscribe' , 1) !!} Subscribe
        </label>
      </div>
    </div>
</div>

<div class="form-group">
	{!! Form::label('lineid', 'Line ID' , array('class'=>'col-sm-2 control-label')) !!}
	<div class="col-sm-8">
		{!! Form::text('lineid', Input::old('lineid'), array('class' => 'form-control')) !!}
	</div>
</div>

<div class="form-group">
	{!! Form::label('facebookname', 'Facebook *' , array('class'=>'col-sm-2 control-label')) !!}
	<div class="col-sm-8">
		{!! Form::text('facebookname', Input::old('facebookname'), array('class' => 'form-control')) !!}
	</div>
</div>

<div class="form-group">
    <div class="col-sm-offset-2 col-sm-8">
		{!! Form::submit('Save', array('class' => 'btn btn-primary')) !!}
		<a href="{{ URL::to('users/') }}">&nbsp;Cancel</a>
	</div>
</div>
@section('headercss')
    <link href="/css/bootstrap-datetimepicker.css" rel="stylesheet" type="text/css" />  
@stop

@section('footerjs')
    <script type="text/javascript" src="/js/moment-with-locales.js"></script>
    <script type="text/javascript" src="/js/bootstrap-datetimepicker.js"></script>
    <script type="text/javascript" src="/js/page/admin/user.form.js"></script>
@stop