@extends('layouts.master')

@section('content')
<div class="row">
	<ol class="breadcrumb">
	  <li><a href="/">Home</a></li> 
	  <li class="active">ประวัติการสั่งซื้อ</li>
	</ol>
</div>
<div class="row">
    <div class="col-md-2">
        @include('orders/blocks/leftmenu') 
    </div>
    <div class="col-md-10">
    	<table class="table table-striped table-responsive">
    		<thead>
				<th>หมายเลข</th>
				<th>สถานะ</th>
                <th>EMS</th>
				<th class="text-right">จำนวนเงิน</th>
				<th class="text-center">วันที่</th>
			</thead>
			<tbody>
				@foreach( $orders as $order ) 
				<tr>
					<td><a href="/orders/{{$order->id}}">#{{$order->orderNumber}}</a></td>
					<td><span class="label {{$order->labelStatus}}">{{$order->status->label}}</span></td>
          <td>
            <a href="http://emsbot.com/#/?s={{$order->ship_no}}" target="_blank">{{$order->ship_no}}</a> 
          </td>
					<td class="text-right">{{$order->grandTotal}}</td>
					<td class="text-center">{{$order->createdAtFormat}}</td>
				</tr>
				@endforeach
			</tbody>
    	</table>
    	
    </div>
</div>
@include('blocks.emsmodal')
@stop

@section('footerjs')  
    <script type="text/javascript" src="/js/emsModal.js"></script>
@stop