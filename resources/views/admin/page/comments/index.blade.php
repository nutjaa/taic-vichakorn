@extends('layouts.admin')
@section('content') 
<div class="breadcrumbs">
    <ol class="breadcrumb">
        <li><a href="/"><i class="fa fa-home"></i>Home</a></li>
        <li><a href="/admin">Admin</a></li>
        <li><a href="/pages">Pages</a></li>
        <li><a href="/pages/{{$page->id}}/edit">{{$page->title}}</a></li>
         <li class="active">Comments</li>
    </ol>
</div>
<div class="innerLR">
    <div class="row manage-header">
        <div class="col-lg-6">
        	<h2>Pages Comments</h2>
        </div> 
    </div>
    <div class="widget">
    	{!! Notification::showAll() !!}
      @include('admin.blocks.comments.list', array('comments'=>$page->comments,'url'=>'/pages/'.$page->id.'/comments/'))
    </div>
</div>
@stop