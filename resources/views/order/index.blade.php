@extends('layouts.master')

@section('content')
<div class="row">
	<ol class="breadcrumb">
	  <li><a href="/">Home</a></li> 
	  <li class="active">สั่งซื้อผลิตภัณฑ์</li>
	</ol>
</div>
<div ng-app="vichakorn">
	<div ng-controller="vichakornController" > 
		<form class="form-horizontal" role="form" method="POST" action="{{ url('/order') }}">
		<div class="row">
			<div class="col-md-9"> 
				{!!$page1->body!!}
			</div>	
			<div class="col-md-3">
				<br />
				<div class="panel panel-primary" ng-if="total > 0">
					<div class="panel-heading">
						ตระกร้าสินค้า
					</div>
					<div class="panel-body">
						<table class="table table-borderless">
							<thead>
								<th>รหัส</th> 
								<th class="text-right">ราคา</th>
							</thead>
							<tbody>
								<tr ng-repeat="product in products | filter:{selected_product:true}" ng-if="product.selected_quantity > 0">
									<td>@{{product.sku}} x @{{product.selected_quantity}}</td>
									<td class="text-right">@{{product.sale_price*product.selected_quantity}} ฿</td>
								</tr>
                <tr ng-show="is_vip">
									<td class="text-right"><strong>ส่วนลด VIP 10%</strong></td>
		              <td class="text-right"><strong>@{{discount}} ฿</strong></td>
								</tr>
								<tr>
									<td class="text-right"><strong>รวมทั้งสิ้น</strong></td>
		              <td class="text-right"><strong>@{{total}} ฿</strong></td>
								</tr>
							</tbody>
						</table>
						<input type="submit" value="ไปขั้นตอนสรุปจำนวนค่าใช้จ่าย" class="btn btn-primary form-control" />
					</div>
				</div>
			</div>	
		</div>			
		<div ng-init='products={!!$products->toJson();!!}'></div> 
    @if(Auth::guest())
      <div ng-init='is_vip=0'></div> 
    @else
      <div ng-init='is_vip={{ Auth::user()->is_vip }}'></div> 
    @endif
	    <br />
		
			<input type="hidden" name="_token" value="{{ csrf_token() }}">
		    <table class="table table-striped">
		    	<thead>
		    		<tr>
		    			<th>สั่งซื้อ</th>
		    			<th></th>
		    			<th class="text-center">รหัสสินค้า</th>
		    			<th>ชื่อผลิตภัณฑ์</th>
		    			<th>ราคา</th>
		    			<th class="col-xs-2">จำนวนที่สั่งซื้อ</th>
		    			<th>รวมเป็นเงิน</th>
		    		</tr>
		    	</thead>
		    	<tbody>
		        <tr ng-repeat="product in products ">
		          <td><div ng-if="!product.out_of_stock" ><input name="products[@{{product.id}}][selected_product]" type="checkbox" class="form-control" ng-model="product.selected_product" /></div></td>
		    			<td><img src="@{{product.image_default_thumb}}" title="@{{product.name}}" /></td>
		    			<td class="text-center">@{{product.sku}}</td>
		    			<td>@{{product.nameFirst}}</td>
		    			<td>
		            <div ng-if="product.price == product.sale_price" >
		              @{{product.price}}
		            </div>
		            <div ng-if="product.price != product.sale_price">
		              <span class="price onSale">@{{product.price}}</span><br />
		    					พิเศษเพียง<br />
		    					@{{product.sale_price}}
		            </div>
		          </td>
		    			<td>
                <div ng-if="!product.out_of_stock">
                  <input type="number" class="form-control" ng-model="product.selected_quantity" name="products[@{{product.id}}][selected_quantity]" min="0" max="999"/>
                </div>
                <div ng-if="product.out_of_stock">
                  <label class="label label-danger">สินค้าหมด</label>
                </div>
              </td>
	            <td class="text-right">
	              <div ng-if="product.price == product.sale_price" >
	                @{{product.price*product.selected_quantity}} ฿
	              </div>
	              <div ng-if="product.price != product.sale_price">
	                @{{product.sale_price*product.selected_quantity}} ฿
	              </div>
	            </td>
		        </tr>
		        <tr>
		          <td colspan="6" class="text-right"><strong>สินค้าทั้งหมด</strong></td>
		          <td class="text-right"><strong>@{{total}} ฿</strong></td>
		        </tr>
		    	</tbody>
		    </table>
			    
			<div class="row">
				<div class="col-md-4 col-md-offset-4"><input type="submit" value="ไปขั้นตอนสรุปจำนวนค่าใช้จ่าย" class="btn btn-primary form-control" /></div>
			</div>
		</form>
			
		
		<hr/>
		<div class="row">
			<div class="col-md-12">
				{!!$page2->body!!}
			</div>
		</div>
		<div class="row">
			<div class="col-md-12">
				{!!$page3->body!!}
			</div>
		</div>
	</div>
</div>
@stop

@section('headerjs')
    <link href="/css/bootstrap-datetimepicker.css" rel="stylesheet" type="text/css" /> 
    <script type="text/javascript" src="/js/angular.min.js"></script>
@stop

@section('footerjs') 
    <script type="text/javascript" src="/js/page/order.index.js"></script>
@stop