<?php namespace App;

use LaravelBook\Ardent\Ardent;
use App\Vocabulary;

class Termdata extends Ardent {

    protected $table = 'term_datas';
    protected $fillable = array('name' , 'label' , 'description' , 'vid');
    
    
    
    public static $relationsData = array(
        'vocabulary' => array(self::BELONGS_TO, 'App\Vocabulary', 'foreignKey' => 'vid') ,  
    );
    
    public static $rules = array(
    	'name' => 'required|unique:term_datas',
    	'label' => 'required|unique:term_datas', 
    	'vid' => 'required', 
	);
	
	/*------ Scope ------ */
    
    public function scopeSearch($query,$q){
    	if(trim($q) == '')
    		return $query ;   
   		return $query->where(function($query) use ($q){ 
   			$query->where('name', 'LIKE' , '%'.$q.'%')
   				->orWhere('label', 'LIKE' , '%'.$q.'%') 
                ->orWhere('description', 'LIKE' , '%'.$q.'%') ; 
   		}) ; 
    }
    
    public function scopeAllrole($query)
    {
        return $query->where('vid', '=', 1);
    }
    
    public function scopeVid($query,$vid){
    	return $query->where('vid', '=',$vid);
    }
    
    public function scopeName($query,$q){
    	return $query->where('name','=',$q) ; 
    }
    
    #Generate term option 
    
    public static function GenerateOptionByVid($vid , $meid){
    	$terms = self::Vid($vid)->where('id','!=',$meid)->where('pid','=',0 )->lists('label','id');
        $terms = self::GenerateOptionSub($terms,$meid , 0);
    	$terms = array(0 => 'None') + $terms  ;  
    	return $terms ; 
    }
    
    public static function GenerateOptionSub($terms , $meid , $level ){ 
        $ret = array() ;
        $nextLevel = $level + 1 ; 
        foreach($terms as $k => $term){
            $t = self::where('id','!=',$meid)->where('pid','=',$k )->lists('label','id');
            $ret += array($k => self::GenerateOptionLevel( $term,$level )) +  self::GenerateOptionSub($t , $meid , $nextLevel ) ; 
        }
        return $ret ; 
    }
    
    public static function GenerateOptionLevel($term,$level){
        for($i = 0 ; $i < $level ; $i++ ){
            $term = '--' . $term ; 
        }
        return $term ;
    }
    
    public static function NameValueOption($vid){
     	return self::Vid($vid)->lists('label','name');
    }
}