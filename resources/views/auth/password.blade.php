@extends('layouts.master')
@section('content') 
<br />
<div class="row">
	<div class="col-md-8 col-md-offset-2">
		<div class="panel panel-default">
			<div class="panel-heading">ลืมรหัสผ่าน</div>
			<div class="panel-body">
				@if (session('status'))
					<div class="alert alert-success">
						{{ session('status') }}
					</div>
				@endif

				@if (count($errors) > 0)
					<div class="alert alert-danger">
						<strong>ขออภัย</strong> เกิดปัญหาบางอย่างสำหรับข้อมูลที่ส่งมา<br><br>
						<ul>
							@foreach ($errors->all() as $error)
								<li>{{ $error }}</li>
							@endforeach
						</ul>
					</div>
				@endif

				<form class="form-horizontal" role="form" method="POST" action="{{ url('/password/email') }}">
					<input type="hidden" name="_token" value="{{ csrf_token() }}">

					<div class="form-group">
						<label class="col-md-4 control-label">อีเมล์เพื่อเข้าสู่ระบบ</label>
						<div class="col-md-6">
							<input type="email" class="form-control" name="email" value="{{ old('email') }}">
						</div>
					</div>

					<div class="form-group">
						<div class="col-md-6 col-md-offset-4">
							<button type="submit" class="btn btn-primary">
								ส่งลิงค์ สำหรับแก้ไขพาสเวิร์ด
							</button>
						</div>
					</div>
				</form>
			</div>
		</div>
	</div>
</div> 
@endsection
