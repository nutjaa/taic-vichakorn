<div class="navbar hidden-print main navbar-default" role="navigation">
    <div class="user-action user-action-btn-navbar pull-right">
		<button class="btn btn-sm btn-navbar btn-inverse btn-stroke hidden-lg hidden-md"><i class="fa fa-bars fa-2x"></i></button>
	</div>
    <a class="logo" href="/">
		<img width="32" alt="SMART" src="/images/logo.png"/>
		<span class="hidden-xs hidden-sm inline-block">Vichakorn</span>
	</a>
    <ul class="main pull-left hidden-xs ">
        <li class="notifications">
            <a href="/booking/kastrup">
                <i class="fa fa-clock-o"></i>
                <span class="badge badge-warning"></span>
            </a>
        </li>
        <!--
        <li class="notifications">
			<a href="user.html?v=v1.0.0-rc1"><i class="fa fa-user"></i> <span class="badge badge-info">2</span></a>
		</li>
		-->
    </ul>
    <ul class="main pull-right">
        <li class="hidden-xs hidden-sm"><a data-toggle="modal" class="btn btn-info" href="/pages/create">Create New Page  <i class="fa fa-fw icon-compose"></i></a>
        </li>
        <li class="hidden-xs">เตรียมจัดส่งสินค้า:  <a href="{{  URL::to('orders?s=confirm') }}"><strong class="text-primary">{{ Auth::user()->totalConfirm }}</strong></a></li>
        <li class="hidden-xs">แจ้งชำระเงิน:  <a href="{{  URL::to('orders?s=transfer-reported') }}"><strong class="text-primary">{{ Auth::user()->totalConfirmTransfer }}</strong></a></li>
        <li class="dropdown username hidden-xs">
			<a data-toggle="dropdown" class="dropdown-toggle" href=""><img alt="Profile" class="img-circle" src="/images/{{ Auth::user()->avatarpath() }}" width="35" /> {{ Auth::user()->username }}@if(Auth::user()->is_vip)<i class="fa fa-star"></i>@endif<span class="caret"></span></a>

			<ul class="dropdown-menu pull-right">
				<li><a class="glyphicon glyphicon-user  " href="#"><i></i> Account</a></li>
				<li><a class="glyphicon glyphicon-envelope " href="#"><i></i> Messages</a></li> 
				<li><a class="glyphicon glyphicon-lock " href="/auth/logout"><i></i> Logout</a></li>
		    </ul>
		</li>
    </ul>
</div>