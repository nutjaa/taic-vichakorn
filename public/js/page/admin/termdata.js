$(document).ready(function(){ 
	$('select[name="vid"]').change(function(){
		//Load new parent data ; 
		$.get('/termdata/options/'+$(this).val()+'/'+$('input[name="tid"]').val(), function( data ) {
 			var $el = $('select[name="pid"]') ; 
 			$el.empty(); // remove old options
 			
			$.each(data, function(key, value) { 
			  $el.append($("<option></option>")
			     .attr("value", key).text(value));
			});
		}, "json" );
	}) ; 
	
}) ; 