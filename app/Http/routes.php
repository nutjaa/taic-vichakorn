<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/
define('OMISE_PUBLIC_KEY', env('OMISE_PUBLIC', 'pkey_51jhypljyehiwle2wo4'));
define('OMISE_SECRET_KEY', env('OMISE_SECRET', 'skey_51jhyqi3ta12wrs5sn5'));
#Admin
$adminRoutes = function()
{
	Route::get('/','Admin\IndexController@index'); 
	Route::controllers([
		'auth' => 'Admin\Auth\AuthController', 
    'ga' => 'Admin\GaController' , 
	]);
	# global
	Route::resource('users', 'Admin\UsersController'); 
	Route::resource('vocabulary', 'Admin\VocabularyController'); 
	Route::get('/termdata/options/{vid}/{tid}', 'Admin\TermdataController@options');
  Route::resource('termdata', 'Admin\TermdataController');
  Route::get('filemanager', 'Admin\FilemanagerController@showIndex');   
  # 
  Route::resource('orders', 'Admin\OrdersController');
  Route::put('orders/{id}/changestatus','Admin\OrdersController@changestatus');
  
  Route::resource('products', 'Admin\ProductsController');
  Route::get('products/{id}/comments', 'Admin\ProductsController@comments');
  Route::get('products/{id}/comments/{cid}', 'Admin\ProductsController@commentedit');
  
  Route::resource('pages', 'Admin\PagesController');
  Route::get('pages/{id}/comments', 'Admin\PagesController@comments');
  Route::get('pages/{id}/comments/{cid}', 'Admin\PagesController@commentedit');  
  
  Route::resource('productimages','Admin\ProductImageController') ; 
  Route::resource('omises','Admin\OmisesController');
  Route::get('mandrill/info','Admin\MandrillController@info');
  Route::get('mandrill/url-lists','Admin\MandrillController@urlLists');
  
  Route::resource('comments', 'Admin\CommentsController'); 
  
  Route::get('ems/{ems}','Admin\EmsController@index') ; 
  
  Route::get('admindocs','Admin\AdmindocsController@index');
  Route::get('admindocs/{id}/{sefu}', 'Admin\AdmindocsController@view'); 
 
};

Route::group(array('domain' => 'admin.vichakorn.com'),$adminRoutes); 
Route::group(array('domain' => 'admin.vch.aecstar.com'),$adminRoutes);
Route::group(array('domain' => 'admin.luminancecocktail.com'),$adminRoutes);

#Page
Route::get('/', 'WelcomeController@index');  
Route::get('/home', 'HomeController@index');  
#Page - Abount US
Route::get('aboutus', 'AboutusController@index'); 
Route::get('aboutus/{sefu}', 'AboutusController@view'); 
#Page - Contact US
Route::get('contactus', 'ContactController@index'); 
Route::post('contactus','ContactController@submit');
#Page - Privileges
Route::get('privileges','PrivilegesController@index');
#Page - Order
Route::get('order','OrderController@index');
Route::post('order','OrderController@doOrder');
Route::get('order/comfirm-and-shipping','OrderController@confirm');
Route::post('order/comfirm-use-point' , 'OrderController@addPoint');
Route::post('order/comfirm-and-shipping','OrderController@doConfirm');
Route::get('order/payment','OrderController@payment');
Route::post('order/payment','OrderController@doPayment');
Route::get('order/cancel_paypal','OrderController@cancel_paypal');
Route::get('order/complete_paypal','OrderController@complete_paypal');
Route::post('order/ipn_paypal','OrderController@ipn_paypal');

Route::get('orders','OrdersController@index');
Route::get('orders/{id}','OrdersController@view');
#Page - Products 
Route::get('products','ProductsController@index');
Route::get('product/{sefu}', 'ProductsController@view'); 
#Page - Ordering and how to use
Route::get('ordering-and-how-to-use','HowtoController@index');
Route::get('ordering-and-how-to-use/{id}/{sefu}', 'HowtoController@view'); 
#Page - Wonders of use
Route::get('wonders-of-use','WonderController@index');
Route::get('wonders-of-use/{id}/{sefu}', 'WonderController@view'); 
#Page - Tricks and data inside 
Route::get('tricks-and-data-insight','TricksController@index');
Route::get('tricks-and-data-insight/{id}/{sefu}', 'TricksController@view'); 
#Page - Money Transfer ; 
Route::get('moneytransfer','TransferController@index') ; 
Route::post('moneytransfer','TransferController@doTransfer');
#HELPER - get ems ; 
Route::get('ems/{ems}','EmsController@index') ; 

Route::get('profile/edit', 'ProfileController@edit'); 
Route::post('profile/edit','ProfileController@update');

Route::resource('comments', 'CommentsController'); 

Route::get('historical-point', 'HistoricalPointController@index'); 

Route::controllers([
	'auth' => 'Auth\AuthController',
	'password' => 'Auth\PasswordController',
]);
