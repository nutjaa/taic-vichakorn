<?php namespace App\Http\Controllers;

use Illuminate\Http\Request; 
use App\Order;  
use Auth;

use Krucas\Notification\Facades\Notification;

class OrdersController extends Controller {
	public function __construct()
	{
		$this->middleware('auth');
	}
	
	public function index(){   
		$orders = Order::byUser(Auth::user()->id)->orderBy('created_at','DESC')->get() ;  
		return view('orders.list')->with('orders',$orders) ; 
	}
	
	public function view($id){
		$order = Order::find($id) ; 
		if($order->user_id != Auth::user()->id)
			return redirect('orders') ; 
		return view('orders.view')->with('order',$order) ; 
	}
}