<?php namespace App\Http\Controllers;

use Auth ; 
use App\HistoricalPoint ; 

class HistoricalPointController extends Controller {
 
	/**
	 * Create a new controller instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		$this->middleware('auth');
	}

	/**
	 * Show the application dashboard to the user.
	 *
	 * @return Response
	 */
	public function index()
	{
	  $records = HistoricalPoint::where('user_id',Auth::user()->id)->orderBy('created_at','DESC')->paginate(15)  ;   
		return view('point')->with('records',$records) ; 
    
	}

}
