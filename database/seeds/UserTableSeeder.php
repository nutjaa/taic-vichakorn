<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;

use App\User ;
use App\Termdata ;  

class UserTableSeeder extends Seeder {

	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run()
	{
		$role =  Termdata::where('name' , '=' , 'admin')->first() ; 
        
		DB::table('users')->truncate();
		
		User::create(array('email' => 'tworkstudio@gmail.com' , 'password' => Hash::make('p@ssword') , 'name' => 'Nut' , 'lastname' => 'Chanyong' , 'username' => 'tworkstudio', 'status' => 1 ,'user_role_id' => $role->id ));
	}

}
