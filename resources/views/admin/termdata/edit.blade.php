@extends('layouts.admin')
@section('content') 
<div class="breadcrumbs">
    <ol class="breadcrumb">
        <li><a href="/"><i class="fa fa-home"></i>Home</a></li>
        <li><a href="/admin">Admin</a></li>
        <li><a href="/termdata">Terms</a></li>
        <li class="active">Edit {{ $term->name }}</li>
    </ol>
</div>

<div class="innerLR">
    <div class="row manage-header">
        <div class="col-lg-7">
        	<h2>Edit term - {{ $term->name }}</h2>
        </div>
 	</div>
    
    <div class="widget">
        {!! Notification::showAll() !!}
 	
     	{!! Form::model($term, array('url' => 'termdata/'. $term->id , 'method' => 'PUT'  , 'class'=>'form-horizontal' , 'role' => 'form' )) !!}
     		<div class="form-group">
    			{!! Form::label('name', 'Name' , array('class'=>'col-sm-2 control-label')) !!}
    			<div class="col-sm-8">
    				{!! Form::text('name', Input::old('name'), array('class' => 'form-control')) !!}
    			</div>
    		</div>
            <div class="form-group">
    			{!! Form::label('label', 'label' , array('class'=>'col-sm-2 control-label')) !!}
    			<div class="col-sm-8">
    				{!! Form::text('label', Input::old('label'), array('class' => 'form-control')) !!}
    			</div>
    		</div>
            <div class="form-group">
            	{!! Form::label('vid', 'Vocabulary *' , array('class'=>'col-sm-2 control-label')) !!}
            	<div class="col-sm-8">
            		{!! Form::select('vid',$vocabulary_options,Input::old('vid'), array('class' => 'form-control')) !!}
            	</div>
            </div>
    		<div class="form-group">
    			{!! Form::label('description', 'Description' , array('class'=>'col-sm-2 control-label')) !!}
    			<div class="col-sm-8">
    				{!! Form::textarea('description', Input::old('description'), array('class' => 'form-control')) !!}
    			</div>
    		</div> 
    		 <div class="form-group">
            	{!! Form::label('pid', 'Parent' , array('class'=>'col-sm-2 control-label')) !!}
            	<div class="col-sm-8">
            	 	{!! Form::select('pid',$term_options,Input::old('pid'), array('class' => 'form-control')) !!}
            	</div>
            </div>
    		<div class="form-group">
    		    <div class="col-sm-offset-2 col-sm-8">
    				{!! Form::submit('Save', array('class' => 'btn btn-primary')) !!}
    				<a href="{{ URL::to('termdata/') }}">&nbsp;Cancel</a>
    			</div>
    		</div>
     	{!! Form::close() !!}
     	<input type="hidden" name="tid" value="{{$term->id}}" />
    </div>
</div>

@stop

@section('footerjs')  
    {!! HTML::script('js/page/admin/termdata.js'); !!}  
@stop