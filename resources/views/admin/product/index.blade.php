@extends('layouts.admin')
@section('content') 
<div class="breadcrumbs">
    <ol class="breadcrumb">
        <li><a href="/"><i class="fa fa-home"></i>Home</a></li>
        <li><a href="/admin">Admin</a></li>
        <li class="active">Products</li>
    </ol>
</div>
<div class="innerLR">
    <div class="row manage-header">
        <div class="col-lg-6">
        	<h2>Products Management</h2>
        </div>
        <div class="col-lg-2">
        	<a class="btn btn-small btn-primary" href="{{ URL::to('products/create') }}">Create new product</a>
        </div>
        {!! Form::open(array( 'method' => 'get')) !!}
        <div class="col-lg-2"> 
        {!! Form::select('c',$categoryOptions,$c, array('class' => 'form-control', 'onchange' => "this.form.submit()" ) ) !!}
        </div>
        <div class="col-lg-2">
          <input type="text" name="q" placeholder="search" class="span3 pull-right form-control" value="{{ $q }}" />
        </div>
        {!! Form::close() !!}
    </div>
    <div class="widget">
    	{!! Notification::showAll() !!}
        <div class="table-responsive">
            <table class="table">
                <thead>
	    			<tr>
	    				<th width="45">ID</th>
	    				<th>Image</th>
	    				<th>Name</th>
	    				<th>Weight</th>
	                    <th>Category</th>
	    				<th>Updated</th>  
	    				<th>Actions</th>
	    			</tr>
	    		</thead>
	    		<tbody>
	    		@foreach($products as $product)
	    			<tr>
	    				<td>{{ $product->id }}</td>
	    				<td><img src="{{ $product->imageDefault('thumb') }}" class="img-thumbnail" style="width: 50px;"/></td>
	    				<td>{{ $product->name }}
	                    @if($product->status == 0 )
	                        <span class="label label-danger">&nbsp;</span>
	                    @else
	                        <span class="label label-success">&nbsp;</span>
	                    @endif
                      @if($product->out_of_stock )
                        <span class="label label-danger">Out of stock</span>
                      @endif
	                    </td>
	                    <td>{{$product->weight}}</td>
	                    <td>
	                    	<?php if( is_object($product->category)){ ?>
							<span class="badge">{{ $product->category->label }}</span>
							<?php } ?>
				 
						</td> 
	    				<td>{{ $product->updated_at }}</td> 
	    				<td>
	    					<a class="btn btn-small btn-info" href="{{ URL::to('products/' . $product->id . '/edit') }}">Edit</a>
                &nbsp;
                <a class="btn btn-small btn-info" href="{{ URL::to('products/' . $product->id . '/comments') }}">Comments({{$product->comments->count()}})</a>
	    				</td>
	    			</tr>
	    		@endforeach
	    		</tbody>
	    	</table>
	        <div class="text-center">
	    		<?php echo $products->appends(array('q' => $q))->render(); ?>
	    	</div>
        </div>
    </div>
</div>
@stop