@extends('layouts.admin')
@section('content') 
<div class="breadcrumbs">
    <ol class="breadcrumb">
        <li><a href="/"><i class="fa fa-home"></i>Home</a></li> 
        <li class="active">Dashboard</li>
    </ol>
</div>
<div class="innerLR">
    <div class="row">
    	<div class="col-md-6">
			<div class="widget">
				<h4>Lastest Order Status (<a href="{{ url('orders') }}">All</a>)</h4>
				<table class="table">
          <thead>
	    			<tr>
	    				<th width="80">ID</th>
	    				<th>Email</th>
	    				<th>Status</th> 
	            <th>Date</th>
	    			</tr>
    			</thead>
		      <tbody>
            @foreach($orders as $order)
            <tr>
              <td><a href="{{ url('orders/' . $order->id ) }}">{{ $order->orderNumber }}</a></td>
              <td>{{ $order->email }}</td>
              <td><span class="label {{$order->labelStatus}}">{{ $order->status->label }}</span></td>
              <td>{{ $order->updatedAtFormat }}</td>
            </tr>
            @endforeach
					</tbody>
        </table>
			</div>
		</div>
		<div class="col-md-6">
			<div class="widget">
				<h4>Customer Login Status</h4>
        <table class="table">
          <thead>
            <tr>
              <th>Email</th>
              <th>Name</th>
              <th>Date</th>
            </tr>
          </thead>
          <tbody>
            @foreach($users as $user)
            <tr>
              <td>{{ $user->email }}</td>
              <td>{{ $user->fullName }}</td>
              <td>{{ $user->updatedAtFormat }}</td>
            </tr>
            @endforeach
          </tbody>
        </table>
			</div>
      <div class="widget">
        <h4>Webboard Activities</h4>
        <table class="table">
          <thead>
            <tr>
              <th>กระทู้</th>
              <th>โพส</th>
              <td>Date</td>
            </tr>
          </thead>
          <tbody>
            @foreach($posts as $post)
            <tr>
              <td>{{ str_limit($post->thread->title,30,'..') }}</td>
              <td><a target="_blank" href="{{ config('app.url'). 'forum/' . $post->thread->category->id . '-' . $post->thread->category->title . '/' . $post->thread->id . '-' . $post->thread->title}}">{{ str_limit($post->content,60,'..') }}</a></td>
              <td>{{ $post->updatedAtFormat }}</td>
            </tr>
            @endforeach
          </tbody>
        </table>
      </div>
		</div>
    </div>
</div>
@stop
 