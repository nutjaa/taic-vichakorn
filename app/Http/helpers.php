<?php
 

function ThaiTime($times){
	$date = new Carbon( $times  );
	$date->setTimezone('Asia/Bangkok');
	return $date->format('d/m/Y H:i:s');
}