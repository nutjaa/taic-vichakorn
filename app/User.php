<?php namespace App;

use Illuminate\Auth\Authenticatable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Auth\Passwords\CanResetPassword;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\CanResetPassword as CanResetPasswordContract;
use LaravelBook\Ardent\Ardent;
use File;
use Image;
use Carbon ; 
use Session ; 

use App\Order;

class User extends Ardent implements AuthenticatableContract, CanResetPasswordContract {

	use Authenticatable, CanResetPassword;

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'users';

	/**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	 */
	protected $fillable = ['name' , 'lastname' , 'username' , 'address', 'email', 'password','user_role_id'];

	/**
	 * The attributes excluded from the model's JSON form.
	 *
	 * @var array
	 */
	protected $hidden = ['password', 'remember_token'];
	
	protected $attachment = array( 
    'thumb' => array('w' => 100 , 'h' => 100 )
  ) ; 
    
  public static $rules = array(
  	'username' => 'required|unique:users',
  	'email' => 'required|email|unique:users',  
    'user_role_id' => 'required',
	);
	
	public static $relationsData = array(
        'role' => array(self::BELONGS_TO, 'App\Termdata', 'foreignKey' => 'user_role_id')  , 
        'orders' => array(self::HAS_MANY, 'App\Order' , 'user_id') ,     
    );
    
  /* --- Scope --- */
  
  public function scopeSearch($query,$q){
  	if(trim($q) == '')
  		return $query ;   
 		return $query->where(function($query) use ($q){ 
 			$query->where('name', 'LIKE' , '%'.$q.'%')
 				->orWhere('email', 'LIKE' , '%'.$q.'%')
 				->orWhere('phone', 'LIKE' , '%'.$q.'%')  ; 
 		}) ; 
  }
    
  public function scopeToken($query,$token){
  	return $query->where('token','=',$token);
  }
    
    /* ---- Attribute --- */
    
    
    public function isAdmin(){
        return $this->role->name == 'admin' ; 
    }
    
    public function shortname(){ 
        return ucfirst($this->name) . ' ' . strtoupper( substr($this->lastname,0,1)) . '.' ; 
    }
    
    public function avatarpath(){
    	if( $this->avatar == '')
    		return 'noavatar.png' ; 
    	return 'avatar/' . $this->id . '/thumb/' .  $this->avatar 	 ;
    }
    
    public function processAvatar(){
    	foreach($this->attachment as $key => $attach){ 
    		if(! File::exists(public_path() . '/images/avatar/' . $this->id . '/' . $key . '/'))
            	File::makeDirectory(public_path() . '/images/avatar/' . $this->id . '/' . $key . '/');
            $image = Image::make( public_path() . '/images/avatar/' . $this->id . '/' . $this->avatar )->fit($attach['w'],$attach['h'])->save( public_path() . '/images/avatar/' . $this->id . '/' . $key . '/' . $this->avatar);
        }
    }
    
    public function shipping($field){ 
    	$shipping = Session::get('shipping') ; 
    	if(isset($shipping[$field]))
    		return $shipping[$field] ; 
   		return $this->$field  ; 
    } 
    
    /*--- Attribute ---*/
    public function getShippingJsonAttribute(){
      $ret = array() ; 
      $ret['id'] = $this->id ; 
      $shipping = Session::get('shipping') ; 
      foreach($shipping as $k => $v){
        $ret[$k] = $v ; 
      }
      return json_encode($ret) ; 
    }
    
    public function getFullnameAttribute(){
        return $this->name . ' ' . $this->lastname ; 
    }
    
    public function getBirthDateFormattedAttribute(){
        return Carbon::createFromFormat('Y-m-d', $this->birthdate )->format('d/m/Y');
    }
    
    public function getUpdatedAtFormatAttribute(){ 
		$date = Carbon::createFromFormat('Y-m-d H:i:s', $this->updated_at  );
		$date->setTimezone('Asia/Bangkok');
		return $date->format('d/m/Y H:i:s');
    }
    
    public function getAddressHtmlAttribute(){
        return nl2br($this->address);
    }
    
    public function getTotalConfirmTransferAttribute(){
    	if(!$this->isAdmin())
    		return 0 ;
		return Order::hasStatus('transfer-reported')->count() ; 
    }
    
    public function getTotalConfirmAttribute(){
        if(!$this->isAdmin())
    		return 0 ;
		return Order::hasStatus('confirm')->count() ; 
    }
  

}
