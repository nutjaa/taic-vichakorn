$( document ).ready(function() {
     $("#register-form").validate({
	  rules: {
 		password: {
 			required : true , 
 			minlength: 6
		},
	    password_confirmation: {
	      equalTo: "#password" , 
	      minlength: 6
	    }
  	  },
	  submitHandler: function(form) {
	    form.submit();
	  }
	 });
});