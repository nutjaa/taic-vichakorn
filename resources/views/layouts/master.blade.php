<!DOCTYPE html>
<html lang="en-US" class="no-js">
	<head> 
    <meta name="viewport" content="width=device-width, initial-scale=1"/>
    {!! SEOMeta::generate() !!}
    {!! OpenGraph::generate() !!} 
    {!! Twitter::generate() !!}
		<link href='//fonts.googleapis.com/css?family=PT+Sans:400,700,700italic,400italic' rel='stylesheet' type='text/css' />
		<link href="/css/main.css" rel="stylesheet" type="text/css" /> 
		<link href="/css/bootstrap.css" rel="stylesheet" type="text/css" /> 
    <link href="{{ asset('/css/font-awesome.css') }}" rel="stylesheet" />
		<script type="text/javascript" src="/js/jquery-1.11.2.min.js"></script>
    <script src="/js/bootstrap.min.js"></script>
		@yield('headerjs') 
    <script>
      (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
      (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
      m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
      })(window,document,'script','//www.google-analytics.com/analytics.js','ga');
    
      ga('create', '{{ config('services.ga.ua')}}', 'auto');
      ga('send', 'pageview');
    
    </script>
	</head>
	<body>
		<div id="fb-root"></div>
		<script>(function(d, s, id) {
		  var js, fjs = d.getElementsByTagName(s)[0];
		  if (d.getElementById(id)) return;
		  js = d.createElement(s); js.id = id;
		  js.src = "//connect.facebook.net/en_US/sdk.js#xfbml=1&version=v2.3&appId=314864935272687";
		  fjs.parentNode.insertBefore(js, fjs);
		}(document, 'script', 'facebook-jssdk'));</script>
		@include('blocks/header2') 
		@include('blocks/subheader') 
		
		
		<div class="content container"> 
			@yield('content') 
			
		</div>
		<div class="container">
			@include('blocks/footer') 
		</div>
		<script type="text/javascript" src="/js/jquery.backstretch.min.js"></script>
		<script type="text/javascript">
			$( document ).ready(function() {
			    $.backstretch("/images/body_bg_img.gif");
			});
		</script>
		@yield('footerjs') 
    
	</body>
</html>
