{!! Form::open(['url' => $form_url, 'class' => $form_classes , 'files'=>true ]) !!}

@if ( $show_title_field )
<div class="form-group">
    <label for="title">{{ trans('forum/base.title') }}</label>
    {!! Form::text('title', Input::old('title'), ['class' => 'form-control']) !!}
</div>
@endif

<div class="form-group">
    {!! Form::textarea('content', $post_content, ['class' => 'form-control editor']) !!}
</div>

<div style="position:relative;">
  <a class='btn btn-primary' href='javascript:;'>
      Upload Image...
      <input type="file" style='position:absolute;z-index:2;top:0;left:0;filter: alpha(opacity=0);-ms-filter:"progid:DXImageTransform.Microsoft.Alpha(Opacity=0)";opacity:0;background-color:transparent;color:transparent;' name="upload" size="40"  onchange='$("#upload-file-info").html($(this).val());'>
  </a>
  &nbsp;
 <span class='label label-info' id="upload-file-info"></span>
</div> 
    
<hr />
<button type="submit" class="btn btn-primary">{{ $submit_label }}</button> 
@if ( $cancel_url )
<a href="{{ $cancel_url }}" class="btn btn-default">{{ trans('forum/base.cancel') }}</a>
@endif
<br /><br />
{!! Form::close() !!}
