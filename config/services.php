<?php

return [

	/*
	|--------------------------------------------------------------------------
	| Third Party Services
	|--------------------------------------------------------------------------
	|
	| This file is for storing the credentials for third party services such
	| as Stripe, Mailgun, Mandrill, and others. This file provides a sane
	| default location for this type of information, allowing packages
	| to have a conventional place to find your various credentials.
	|
	*/

	'mailgun' => [
		'domain' => '',
		'secret' => '',
	],

	'mandrill' => [
		'secret' => 'xjgufjtzxN17_0kcHlfBYg',
	],

	'ses' => [
		'key' => '',
		'secret' => '',
		'region' => 'us-east-1',
	],

	'stripe' => [
		'model'  => 'App\User',
		'secret' => '',
	],

	'omise' => [
		'public_key' => env('OMISE_PUBLIC', 'pkey_51jhypljyehiwle2wo4') , 
		'secret_key' => env('OMISE_SECRET', 'skey_51jhyqi3ta12wrs5sn5'),
 	],
  'ga' => [
    'ua' => env('GA_UA') 
  ],
  'paypal' => [
    'email' => env('PAYPAL_EMAIL') , 
    'sandbox' => env('PAYPAL_SANDBOX')
  ]
];
