

$(document).ready(function(){ 
    $.ajaxPrefilter(function(options, originalOptions, xhr) {
      var token = $('meta[name="csrf_token"]').attr('content'); 
      if (token) {
            return xhr.setRequestHeader('X-XSRF-TOKEN', token);
      }
    });

    $('.product-image-widget').click(function(){
        $('.product-image-widget').removeClass('default') ;
        $(this).addClass('default') ;
        $('#product_image_id').val($(this).data('id')) ; 
    }) ; 
    
    $('.product-image-widget .delete').click(function(){
        var id = $(this).parent().data('id') ; 
        $.ajax({
            url: '/productimages/'+id,
            method: 'DELETE',
            data: {
                _token : $('input[name="_token"]').val() 
            } , 
            
            success: function(response) {
                if(response.success){
                    $(".product-image-widget[data-id='" + response.id +"']").fadeOut();
                }
            }
        });
        return false ; 
    }) ; 
}) ; 