@extends('layouts.email_plus_top')

@section('subject', 'สมัครเปิดบัญชี luminancecocktail.com')

@section('content')

<!-- start textbox-with-title -->
<table width="100%" bgcolor="#e8eaed" cellpadding="0" cellspacing="0" border="0" id="backgroundTable">
   <tbody>
      <tr>
         <td>
            <table width="600" cellpadding="0" cellspacing="0" border="0" align="center" class="devicewidth">
               <tbody>
                  <tr>
                     <td width="100%">
                        <table bgcolor="#ffffff" width="600" cellpadding="0" cellspacing="0" border="0" align="center" class="devicewidth">
                           <tbody>
                              <!-- Spacing -->
                              <tr>
                                 <td width="100%" height="20"></td>
                              </tr>
                              <!-- Spacing -->
                              <tr>
                                 <td>
                                    <table width="560" align="center" cellpadding="0" cellspacing="0" border="0" class="devicewidthinner">
                                       <tbody>
                                          <!-- Title -->
                                          <tr>
                                             <td style="font-family: Helvetica, arial, sans-serif; font-size: 14px; font-weight:bold; color: #333333; text-align:left;line-height: 24px;">
                                                สวัสดีค่ะ คุณ {{ $user->fullname }}
                                             </td>
                                          </tr>
                                          <!-- End of Title -->
                                          <!-- spacing -->
                                          <tr>
                                             <td height="5"></td>
                                          </tr>
                                          <!-- End of spacing -->
                                          <!-- content -->
                                          <tr>
                                             <td style="font-family: Helvetica, arial, sans-serif; font-size: 13px; color: #333333; text-align:left;line-height: 24px;">
                                                ทางเว็บไซต์ luminancecocktail.com ขอแจ้งให้ทราบว่า ขณะนี้ ขั้นตอนการสมัครเปิดบัญชี ของคุณเรียบร้อยแล้ว<br />
อีเมล์ฉบับนี้ส่งมาเพื่อเป็นการยืนยันที่อยู่อีเมล์ที่คุณได้ให้ไว้ในขั้นตอนการสมัครเปิดบัญชี<br />
ซึ่งทางเราขอรบกวนให้ท่าน <a href="{{ url('auth/confirm/'.$user->token) }}" target="_blank">คลิกที่นี่</a> เพื่อทำการยืนยันความถูกต้องของอีเมล์นี้อีกครั้ง 
                                             </td>
                                          </tr>
                                          <!-- End of content -->
                                          <!-- Spacing -->
                                          <tr>
                                             <td width="100%" height="10"></td>
                                          </tr>
                                          <!-- Spacing -->
                                          <!-- button -->
                                          <tr>
                                             <td style="font-family: Helvetica, arial, sans-serif; font-size: 14px; font-weight:bold; color: #333333; text-align:left;line-height: 24px;">
                                              	หากลิงค์ดังกล่าวไม่ทำงาน กรุณาคัดลอก URL ต่อไปนี้ไปวางในเว็บเบราเซอร์ของคุณ<br />
                                              	<a href="{{ url('auth/confirm/'.$user->token) }}" target="_blank">{{ url('auth/confirm/'.$user->token) }}</a>
                                             </td>
                                          </tr>
                                          <tr>
                                             <td width="100%" height="20"></td>
                                          </tr>
                                          <tr>
                                             <td style="font-family: Helvetica, arial, sans-serif; font-size: 13px; color: #333333; text-align:left;line-height: 24px;">
                                                หลังจากทำการยืนยันเรียบร้อยแล้ว คุณสามารถใช้บัญชีดังกล่าวได้ทันที
                                             </td>
                                          </tr>
                                          @include('emails.block.ending')
                                       </tbody>
                                    </table>
                                 </td>
                              </tr>
                           </tbody>
                        </table>
                     </td>
                  </tr>
               </tbody>
            </table>
         </td>
      </tr>
   </tbody>
</table>
<!-- end of textbox-with-title -->

@endsection