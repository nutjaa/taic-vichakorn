@extends('layouts.master')

@section('content')
<div class="row">
	<ol class="breadcrumb">
	  <li><a href="/">Home</a></li> 
	  <li class="active">ประวัติแต้มสะสม</li>
	</ol>
</div>
<div class="row">
    <div class="col-md-2">
        <aside class="widget">
            <div class="widget-inner">
                <h3 class="widget-title" style="background-position: 0px -11px;">
                    <span>ข้อมูลส่วนตัว</span>
                </h3> 
                <img class="img-responsive img-rounded" src="/images/{{ Auth::user()->avatarpath() }}" />
                <ul class="left-menu list-unstyled">
                    <li><a href="/home">ข้อมูลส่วนตัว</a></li>
                    <li><a href="/orders">ประวัติการสั่งซื้อ</a></li>
                    <li class="active"><a href="/historical-point">ประวัติแต้มสะสม</a></li>
                </ul>
            </div>
        </aside>
    </div>
    <div class="col-md-10"> 
      <h3>คุณมีคะแนนคงเหลือทั้งสิ้น  {{Auth::user()->point}} แต้ม</h3> 
      <table class="table-striped table">
        <thead>
          <th>วันที่</th>
          <th>หมายเลข Order</th>
          <th>รายละเอียด</th>
          <th class="text-right">แต้ม</th>
        </thead>
        <tbody>
          @foreach($records as $record )
          <tr>
            <td>{{$record->createdAtFormat }}</td>
            <td>
              @if(is_null($record->order) == false )
                <a href="{{ url('/orders/' . $record->order->id) }}">#{{ $record->order->orderNumber }}</a>
              @endif  
            </td>
            <td>{{$record->remark}}</td>
            <td class="text-right">{{$record->point}}</td>
          </tr>
          @endforeach
          <tr>
            <td colspan="3"><strong>คะแนนคงเหลือ</strong></td>
            <td class="text-right"><strong>{{Auth::user()->point}}</strong></td>
          </tr>
        </tbody>
      </table>
      <div class="text-center">
    		<?php echo $records->render(); ?>
    	</div>
    </div>
</div>
@stop