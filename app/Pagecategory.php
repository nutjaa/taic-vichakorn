<?php namespace App;

use LaravelBook\Ardent\Ardent;

class Pagecategory extends Ardent  {
    
    protected $table = 'page_categories';
    
    public static $rules = array(
    	'page_id' => 'exists:pages,id',
    	'term_id' => 'exists:term_datas,id',  
	);
    
    public static $relationsData = array(
        'page' => array(self::BELONGS_TO, 'App\Page', 'foreignKey' => 'page_id') , 
        'term' => array(self::BELONGS_TO, 'App\Termdata', 'foreignKey' => 'term_id')  
    );
    
    public function isAdded(){
        $data = self::where('page_id' , '=' , $this->page_id )->where('term_id' , '=' , $this->term_id)->lists('id') ; 
    	if(empty($data))
    		return false ; 
   		return true ; 
    }
    
}