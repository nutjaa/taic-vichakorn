@extends('layouts.admin')
@section('content') 
<div class="breadcrumbs">
    <ol class="breadcrumb">
        <li><a href="/"><i class="fa fa-home"></i>Home</a></li> 
        <li class="active">GA - Info ( {{$activeUsers}} Active users )</li>
    </ol>
</div>
<div class="innerLR">
    <div class="row">
    	<div class="col-md-6">
			<div class="widget">
				<h4>Last 7 days pages and visitors</h4>
				<table class="table table-striped">
          <thead>
	    			<tr> 
	    				<th>Date</th>
	    				<th>Visitors</th> 
	            <th>Page Views</th>
	    			</tr>
    			</thead>
		      <tbody>
            @while(! $lastWeekPageViews->isEmpty() )
            <?php $row = $lastWeekPageViews->pop() ?>
            <tr>
              <td>{{ $row['date']->setTimezone('Asia/Bangkok')->format('d/m/Y H:i:s') }}</td>
              <td>{{ $row['visitors'] }}</td>
              <td>{{ $row['pageViews'] }}</td> 
            </tr>
            @endwhile
					</tbody>
        </table>
			</div>
      <div class="widget"> 
        <h4>Top Referrers</h4>
        <table class="table table-striped">
          <thead>
            <tr>
              <th>URL</th>
              <th>Page Views</th>
            </tr>
          </thead>
          <tbody>
            @foreach($topReferrers as $row)
            <tr>
              <td>{{ $row['url'] }}</td>
              <td>{{ $row['pageViews'] }}</td>
            </tr>
            @endforeach
          </tbody>
        </table>
			</div>
		</div>
		<div class="col-md-6">
			<div class="widget"> 
        <h4>Most Visited Pages</h4>
        <table class="table table-striped">
          <thead>
            <tr>
              <th>URL</th>
              <th>Page Views</th>
            </tr>
          </thead>
          <tbody>
            @foreach($mostVisitPages as $row)
            <tr>
              <td>{{ $row['url'] }}</td>
              <td>{{ $row['pageViews'] }}</td>
            </tr>
            @endforeach
          </tbody>
        </table>
			</div>
      <div class="widget"> 
        <h4>Top Keyword</h4>
        <table class="table table-striped">
          <thead>
            <tr>
              <th>Keyword</th>
              <th>Sessions</th>
            </tr>
          </thead>
          <tbody>
            @foreach($topKeyword as $row)
            <tr>
              <td>{{ $row['keyword'] }}</td>
              <td>{{ $row['sessions'] }}</td>
            </tr>
            @endforeach
          </tbody>
        </table>
      </div>
		</div>
    </div>
</div>
@stop
 