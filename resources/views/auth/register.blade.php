@extends('layouts.master')

@section('content') 
	<br />
	<div class="row">
		<div class="col-md-12">
			<div class="panel panel-default">
				<div class="panel-heading">สมัครสมาชิก</div>
				<div class="panel-body">
					 {!! Notification::showAll() !!} 
					<form class="form-horizontal" role="form" id="register-form" method="POST" action="{{ url('/auth/register') }}">
						<input type="hidden" name="_token" value="{{ csrf_token() }}">
						
						<div class="form-group">
							<label class="col-md-3 control-label"><span class="require">*</span>เพศ</label>
							<div class="col-md-9">
								<label class="radio-inline">
									<input type="radio"  name="gender" value="M" required id="gender-m" />ชาย
								</label> 
								<label class="radio-inline">
									<input type="radio"  name="gender" value="F" id="gender-f" />หญิง
								</label> 
								<label class="radio-inline">
									<input type="radio"  name="gender" value="N" id="gender-n" />ไม่ประสงค์จะระบุ
								</label>
								<label for="gender" class="error radio-inline"></label>
							</div>
						</div>

						<div class="form-group">
							<label class="col-md-3 control-label"><span class="require">*</span>ชื่อ</label>
							<div class="col-md-4">
								<input type="text" class="form-control" required name="name" value="{{ old('name') }}">
							</div>
							<div class="col-md-4">
								<label for="name" class="error inline"></label>
							</div>
						</div>
						
						<div class="form-group">
							<label class="col-md-3 control-label"><span class="require">*</span>นามสกุล</label>
							<div class="col-md-4">
								<input type="text" class="form-control" required name="lastname" value="{{ old('lastname') }}">
							</div>
							<div class="col-md-4">
								<label for="lastname" class="error inline"></label>
							</div>
						</div>

						<div class="form-group">
							<label class="col-md-3 control-label"><span class="require">*</span>วันเดือนปีเกิด </label>
							<div class="col-md-2">
								<div class="row">
									<div class="col-md-4">
										<label class="control-label">วันที</label>่
									</div>
									<div class="col-md-8"> 
										{!! Form::select('day', $day_list,Input::old('day'), array('class' => 'form-control')) !!}
									</div>
								</div> 
							</div>
							<div class="col-md-2">
								<div class="row">
									<div class="col-md-4">
										<label class="control-label">เดือน</label>่
									</div>
									<div class="col-md-8">
										{!! Form::select('month', $month_list,Input::old('month'), array('class' => 'form-control')) !!}
									</div>
								</div> 
							</div>
							<div class="col-md-2">
								<div class="row">
									<div class="col-md-2">
										<label class="control-label">ปี</label>่
									</div>
									<div class="col-md-8">
										{!! Form::select('year', $year_list,Input::old('year'), array('class' => 'form-control')) !!}
									</div>
								</div> 
							</div>
						</div>
						
						<div class="form-group">
							<label class="col-md-3 control-label"><span class="require">*</span>ที่อยู่ที่สามารถจัดส่งของได้จริง</label>
							<div class="col-md-8">
								<textarea required name="address" class="form-control">{{ old('address') }}</textarea>
							</div>
						</div>
						
						<div class="form-group">
							<label class="col-md-3 control-label">เบอร์โทร</label>
							<div class="col-md-4">
								<input type="text" class="form-control" name="phone" value="{{ old('phone') }}">
							</div>
						</div>
						
						<div class="form-group">
							<label class="col-md-3 control-label">ไอดีไลน์ (line ID)</label>
							<div class="col-md-4">
								<input type="text" class="form-control" name="lineid" value="{{ old('lineid') }}">
							</div>
						</div>
						
						<div class="form-group">
							<label class="col-md-3 control-label">ชื่อที่ใช้ในเฟสบุ๊ค (facebook name)</label>
							<div class="col-md-4">
								<input type="text" class="form-control" name="facebookname" value="{{ old('facebookname') }}">
							</div>
						</div>
						
						<div class="form-group">
							<label class="col-md-3 control-label"><span class="require">*</span>อีเมล์เพื่อเข้าสู่ระบบ</label>
							<div class="col-md-4">
								<input class="form-control" type="email" required name="email" value="{{ old('email') }}">
							</div>
							<div class="col-md-4">
								<label for="email" class="error inline"></label>
							</div>
						</div>

						<div class="form-group">
							<label class="col-md-3 control-label"><span class="require">*</span>รหัสผ่านเพื่อเข้าสู่ระบบ</label>
							<div class="col-md-4">
								<input type="password" id="password" class="form-control" name="password" />
							</div>
							<div class="col-md-4">
								<label for="password" class="error inline"></label>
							</div>
						</div>
						
						<div class="form-group">
							<label class="col-md-3 control-label"><span class="require">*</span>ยืนยันรหัสผ่านข้างต้นอีกครั้ง</label>
							<div class="col-md-4">
								<input type="password" id="password_confirmation" class="form-control" name="password_confirmation"/>
							</div>
							<div class="col-md-4">
								<label for="password_confirmation" class="error inline"></label>
							</div>
						</div>
						
						<div class="form-group">
							<label class="col-md-3 control-label"><span class="require">*</span>ตั้งชื่อผู้ใช้ที่จะปรากฏในเว็บนี้</label>
							<div class="col-md-4">
								<input type="text" class="form-control" name="username" required value="{{ old('username') }}">
							</div>
							<div class="col-md-4">
								<label for="username" class="error inline"></label>
							</div>
						</div>
						
						<div class="form-group">
							<label class="col-md-3 control-label"><span class="require">*</span>ต้องการรับข่าวสารทางอีเมล์หรือไม่</label>
							<div class="col-md-6">
								<label class="radio-inline">
									<input type="radio"  name="is_subscribe" value="1" required />ต้องการ
								</label> 
								<label class="radio-inline">
									<input type="radio"  name="is_subscribe" value="0" />ไม่ต้องการ
								</label>
								<label for="is_subscribe" class="error radio-inline"></label>
							</div> 
						</div>

						<div class="form-group">
							<label class="col-md-6 control-label">กรุณาใส่ข้อมูลให้ครบทุกช่องที่มีเครื่องหมาย*</label>
						</div>

						<div class="form-group">
							<div class="col-md-6 col-md-offset-4">
								<button type="submit" class="btn btn-primary">
									สมัครสมาชิก
								</button>
							</div>
						</div>
					</form>
				</div>
			</div>
		</div>
	</div> 
@endsection

@section('footerjs')
	<script type="text/javascript" src="/js/jquery.validate.min.js"></script>
	<script type="text/javascript" src="/js/messages_th.js"></script>
	<script type="text/javascript" src="/js/page/register.js"></script>
@endsection
