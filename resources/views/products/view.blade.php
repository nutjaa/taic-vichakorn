@extends('layouts.master')

@section('content')
<div class="row">
	<ol class="breadcrumb">
	  <li><a href="/">Home</a></li> 
	  <li><a href="{{ url('products') }}">รายละเอียดผลิตภัณฑ์</a></li> 
	  <li class="active">{{$product->nameFirst}}</li>
	</ol>
</div>
<div class="row">
	<div class="col-md-4">
    <aside class="widget">
      <div class="widget-inner">
        <h3 class="widget-title" style="background-position: 0px -11px;">
          <span>ผลิตภัณฑ์</span>
        </h3>
  		<ul class="left-menu list-unstyled">
        <?php $i = 1 ; ?>
  			@foreach($products as $p)
  			<li @if($p->id == $product->id) class="active" @endif><a href="{{$p->route}}">{{$i}}. {{$p->nameFirst}}</a></li>
        <?php $i++ ?>
  			@endforeach
  		</ul>
      </div>
    </aside>
	</div>
	<div class="col-md-8">
		<h3>{{$product->nameFirst}}</h3>
		{!!$product->description!!}
    <hr />
    @include('blocks.comments',['model' => $product , 'type' => 'App\Product' ])   
	</div>
</div>    
@stop