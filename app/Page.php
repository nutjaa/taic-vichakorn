<?php namespace App;

use LaravelBook\Ardent\Ardent;

class Page extends Ardent  {

  protected $table = 'pages';
  protected $fillable = array('title', 'sefu', 'body' );
    
  public static $rules = array(
  	'title' => 'required',
  	'sefu' => 'required|unique_with:pages,lang',  
  	'lang' => 'required' , 
	);
    
  public static $relationsData = array(  
    'user' => array(self::BELONGS_TO, 'App\User', 'foreignKey' => 'createby') , 
    'categories' => array(self::HAS_MANY, 'App\Pagecategory' , 'page_id') ,     
  ) ; 
    
  public function comments()
  {
    return $this->morphMany('\App\Comment', 'commentable')->orderBy('updated_at', 'desc');
  }
    
  public function scopeActive($query)
  {
    return $query->where('status', '=', 1);
  }
  
  public function scopeSefu($query , $q)
  {
  	return $query->where('sefu', '=', $q);
  }
  
  public function scopeLang($query , $q)
  {
  	return $query->where('lang','=',$q); 
  }
  
  public function scopeSearch($query,$q){
  	if(trim($q) == '')
  		return $query ;   
 		return $query->where(function($query) use ($q){ 
 			$query->where('title', 'LIKE' , '%'.$q.'%')
 				->orWhere('body', 'LIKE' , '%'.$q.'%')  ; 
 		}) ; 
  }  
  
  public function scopeHasCategory($query , $id){
  	if($id == '' || $id == 0)
  		return $query ; 
 		$term = Termdata::find($id);
 		if(is_null($term))
 			return $query ; 
 		return $query->whereHas('categories',function($query) use ($id){
      $query->where('term_id','=',$id) ; 
 		});
  }
}