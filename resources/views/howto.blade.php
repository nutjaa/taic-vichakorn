@extends('layouts.master')

@section('content')
<div class="row">
	<ol class="breadcrumb">
	  <li><a href="/">Home</a></li> 
	  <li><a href="{{ url('ordering-and-how-to-use') }}">ลำดับและวิธีการใช้ผลิตภัณฑ์</a></li> 
	  <li class="active">{{$page->title}}</li>
	</ol>
</div>
<div class="row">
	<div class="col-md-4">
        <aside class="widget">
            <div class="widget-inner">
                <h3 class="widget-title" style="background-position: 0px -11px;">
                    <span>ลำดับและวิธีการใช้ผลิตภัณฑ์</span>
                </h3>
        		<ul class="left-menu list-unstyled">
        			@foreach($pages as $p)
        			<li @if($p->id == $page->id) class="active" @endif><a href="{{ URL::to('ordering-and-how-to-use/'.$p->id.'/'.$p->sefu) }}">{{$p->order}}. {{$p->title}}</a></li>
        			@endforeach
        		</ul>
            </div>
        </aside>
	</div>
	<div class="col-md-8">
		<h3>{{$page->order}}. {{$page->title}}</h3>
		{!!$page->body!!}
    @if($page->has_comments)
      <hr />
      @include('blocks.comments',['model' => $page , 'type' => 'App\Page' ])   
    @endif
	</div>
</div>
@stop