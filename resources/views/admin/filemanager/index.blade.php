@extends('layouts.admin')
@section('headercss') 
    {!! HTML::style($dir.'/css/elfinder.min.css'); !!}
    {!! HTML::style($dir.'/css/theme.css'); !!}
    {!! HTML::style('//ajax.googleapis.com/ajax/libs/jqueryui/1.10.4/themes/smoothness/jquery-ui.css'); !!}
    
@stop
@section('content') 
<div class="breadcrumbs">
    <ol class="breadcrumb">
        <li><a href="/"><i class="fa fa-home"></i>Home</a></li>
        <li><a href="/admin">Admin</a></li>
        <li class="active">File Manager</li>
    </ol>
</div>

<div class="innerLR">
    <div class="row manage-header">
        <div class="col-lg-7">
        	<h2>File Manager</h2>
        </div> 
    </div>
    <div class="widget">
         <div id="elfinder"></div>
    </div>
</div>  

@stop

@section('footerjs') 
    {!! HTML::script('js/jquery-ui.min.js'); !!}  
    {!! HTML::script($dir.'/js/elfinder.min.js'); !!}
<script type="text/javascript">
    $().ready(function() {
        $('#elfinder').elfinder({ 
        	customData: {
		     _token:  '<?php echo csrf_token(); ?>'
		    },
            url : '<?= URL::action('\Barryvdh\Elfinder\ElfinderController@showConnector') ?>'
        });
    });
</script>
@stop