

@if(Auth::guest())
  <div class="row">
    <div class="col-md-4 col-md-offset-4">
        <a href="/auth/login?redirectTo={{ urlencode(Request::url()) }}" class="btn btn-primary form-control">กรุณา Login เพื่อทำการ comment</a>
        <br /><br />
    </div> 
  </div>   
@endif
<div class="comments" id="comments">
  {!! Notification::showAll() !!} 
  @if(!Auth::guest())
  <div class="row comment-box" id="post-box">
    <div class="col-sm-12">
      <div class="avatar">
        <img src="/images/{{ Auth::user()->avatarpath() }}" class="img-rounded" />
      </div>
      <div class="post-body">
        <form method="post" action="/comments">
          <input type="hidden" name="_token" value="{{ csrf_token() }}"/>
          <input type="hidden" name="url" value="{{ Request::url() }}"/>
          <input type="hidden" name="commentable_id" value="{{ $model->id }}"/>
          <input type="hidden" name="commentable_type" value="{{ $type }}"/>
          <textarea name="body" class="form-control" placeholder="Join the discussion"></textarea>
          <input class="btn btn-primary pull-right" type="submit" value="SEND" />
        </form>
      </div>
    </div>
  </div>
  @endif
  @foreach( $model->comments as $comment ) 
  <div class="row comment-box" id="post-{{ $comment->id }}">
    <div class="col-sm-12">
      <div class="avatar">
        <img src="/images/{{ $comment->user->avatarpath() }}" class="img-rounded" />
      </div>
      @if(!Auth::guest() && $comment->user_id == Auth::user()->id )
      <ul class="post-menu list-inline">
        <li><a title="Remove" href="#post-{{ $comment->id }}" class="remove-comment" data-id="{{ $comment->id }}"><i class="fa fa-trash"></a></i></li>
      </ul>
      @endif
      <div class="post-body">
        <header>
          <span class="author publisher-anchor-color">{{ $comment->user->username }}</span> 
          <span class="post-meta">
            <span class="bullet time-ago-bullet">•</span>
            <span class="time-ago">{{ $comment->posted }}</span>
          </span>
        </header>
        <div class="post-body-inner">
          {!! nl2br($comment->body) !!}
        </div>
      </div> 
    </div>
  </div> 
  @endforeach
</div>

<script type="text/javascript">
  $( document ).ready(function() {
    $('.remove-comment').click(function(){
      if(confirm("ยืนยันเพื่อทำการลบความคิดเห็นนี้!") == true){
        var box_id = '#post-' + $(this).data('id') ; 
        $.post('/comments/'+ $(this).data('id') , { 
          '_method' : 'DELETE' , 
          '_token' : $('input[name="_token"]').val()
        } , function(response){
          if( response['success'] == true ){
            $(box_id ).fadeOut(1000) ; 
          }
        }) ; 
      }
    });
  });
</script>