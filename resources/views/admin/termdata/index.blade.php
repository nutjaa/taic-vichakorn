@extends('layouts.admin')
@section('content') 
<div class="breadcrumbs">
    <ol class="breadcrumb">
        <li><a href="/"><i class="fa fa-home"></i>Home</a></li>
        <li><a href="/admin">Admin</a></li>
        <li class="active">Term data</li>
    </ol>
</div>
<div class="innerLR">
    <div class="row manage-header">
        <div class="col-lg-7">
        	<h2>Term Management</h2>
        </div>
        <div class="col-lg-2">
        	<a class="btn btn-small btn-primary" href="{{ URL::to('termdata/create') }}">Create new term</a>
        </div>
        <div class="col-lg-3">
            {!! Form::open(array( 'method' => 'get')) !!}
                <input type="text" name="q" placeholder="search" class="span3 pull-right form-control" value="{{ $q }}" />
            {!! Form::close() !!}
        </div>
    </div>
    <div class="widget">
        {!! Notification::showAll() !!}
        <div class="table-responsive">
            <table class="table">
                <thead>
    			<tr>
    				<th width="45">ID</th>
    				<th>Name</th>
                    <th>Label (en)</th>
                    <th>Label (da)</th>
                    <th>Vocab</th>
    				<th>Description</th> 
                    <th>Actions</th>
    			</tr>
    		</thead>
    		<tbody>
    		@foreach($terms as $term)
    			<tr>
    				<td>{{ $term->id }}</td>
    				<td>{{ $term->name }}</td>
                    <td>{{ $term->label }}</td>
                    <td>{{ $term->label_da }}</td>
                    <td>{{ $term->vocabulary->name }}</td>
    				<td>{{ $term->description }}</td> 
                    <td>
    					<a href="{{ URL::to('termdata/' . $term->id . '/edit') }}">Edit</a>
    				</td>
    			</tr>
    		@endforeach
    		</tbody>
    	</table>
    	<div class="text-center">
    		<?php echo $terms->appends(array('q' => $q))->render(); ?>
    	</div>
        </div>
    </div>
</div>  
@stop