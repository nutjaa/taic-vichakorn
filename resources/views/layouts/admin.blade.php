<!doctype html>
<html lang="en">
<head>
	<meta charset="UTF-8"/>
	<meta name="viewport" content="width=device-width, initial-scale=1"/>
	<title>TAIC CMS</title>  
 	<link href="{{ asset('/css/bootstrap.css') }}" rel="stylesheet">
	<link href="{{ asset('/css/font-awesome.css') }}" rel="stylesheet">
	<link href="{{ asset('/css/admin.css') }}" rel="stylesheet"> 
	<link href='http://fonts.googleapis.com/css?family=Lato|Lobster+Two&subset=latin&v2' rel='stylesheet' type='text/css'/>
	<link type="text/css" rel="stylesheet" href="//fonts.googleapis.com/css?family=Oswald:300normal,400normal,700normal|Open+Sans:400normal|Droid+Sans:400normal|Pacifico:400normal|Lato:400normal|Josefin+Slab:400normal|Roboto:400normal|Open+Sans+Condensed:300normal|PT+Sans+Narrow:400normal|Allura:400normal|Merriweather:400normal&amp;subset=all"/>
	@yield('headercss')
</head>
<body class="admin">
    <div class="container-fluid">
        @include('admin/blocks/leftmenu',array('items' =>array(
			array('path' => '/' , 'href'=>'/','title'=>'Overview','icon'=>'fa-home') , 
			array('href'=>'shop','title'=>'Shop','icon'=>'fa-cart-plus','sub'=>array( 
				array('path' => 'products*','href'=>'/products' , 'title'=>'Products','icon' => 'fa-cart-plus' ) ,
                array('path' => 'orders*','href'=>'/orders' , 'title'=>'Orders','icon' => 'fa-sellsy' ) ,
                array('path' => 'omise*','href'=>'/omises' , 'title'=>'Omise','icon' => 'fa-usd' ) ,
			)),
			array('href'=>'page-menu','title'=>'Pages','icon'=>'fa-file-text-o','sub'=>array(
				#array('path' => 'pages/frontpage*','href'=>'/pages/frontpage' , 'title'=>'Front Page','icon' => 'fa-file-text-o' ) ,
				array('path' => 'pages*','href'=>'/pages' , 'title'=>'All','icon' => 'fa-file-text-o' ) ,
			)), 
			array('href'=>'contents-menu','title'=>'Emails','icon'=>'fa-envelope','sub'=>array( 
				array('path' => 'mandrill/info','href'=>'/mandrill/info' , 'title'=>'Info','icon' => 'fa-info-circle' ) ,
        array('path' => 'mandrill/url-lists','href'=>'/mandrill/url-lists' , 'title'=>'Url Lists','icon' => 'fa-eye' ) ,
			)), 
      
      array('href'=>'marketing-menu','title'=>'Marketing','icon'=>'fa-sellsy','sub'=>array( 
				array('path' => '/ga/info','href'=>'/ga/info' , 'title'=>'Google Analytics','icon' => 'fa-google-plus-square' ) , 
			)), 
			 
			 
			array('href'=>'admin-menu' , 'title'=>'Admin','icon'=>'fa-lock','sub'=>array(
				array('path' => 'users*' , 'href'=>'/users','title'=>'Users','icon'=>'fa-users') ,  
				array('path' => 'configurations*' , 'href'=>'/configurations','title'=>'Configurations','icon'=>'fa-cog') ,  
				array('path' => 'vocabulary*' , 'href'=>'/vocabulary','title'=>'Vocabulary','icon'=>'fa-tasks') , 
				array('path' => 'termdata*' , 'href'=>'/termdata','title'=>'Term data','icon'=>'fa-tasks') , 
        array('path' => 'filemanager*' , 'href'=>'/filemanager','title'=>'File Manager','icon'=>'fa-file') , 
        array('path' => 'admindocs*' , 'href'=>'/admindocs','title'=>'Documents','icon'=>'fa-book') , 
			)),
		)))  
        <div id="content">
            @include('admin/blocks/navbar') 
            @yield('content') 
        </div> 
        
    </div>
    <script src="/js/jquery-1.11.2.min.js"></script>
    <script src="/js/bootstrap.min.js"></script>
    <script src="/js/pace.min.js"></script>
    <script src="/js/admin.js"></script> 
    @yield('footerjs')
</body>
</html>