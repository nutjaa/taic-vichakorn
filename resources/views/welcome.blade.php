@extends('layouts.master')

@section('content')
<div class="row">
	<div class="col-md-12">
		<div class="slider-wrapper theme-default">    
            <div id="slider" class="nivoSlider">
                <a href="/products"><img src="/images/main-image-1-1.jpg" class="img-responsive" /></a> 
                <a href="/products"><img src="/images/main-image-2-1.jpg" class="img-responsive" /></a>
            </div>
  		</div>
	</div>
</div> 
<div class="row">
	<div class="col-md-12">
		<br /> 
		<p class="text-center">Evidence-Based Medicine Always Brings Quality , Honesty and Sincerity to Medical Product<br />
		หลักฐานเชิงประจักษ์ทางการแพทย์ย่อมนำมาซึ่งคุณภาพ ความซื่อตรง และความจริงใจของผลิตภัณฑ์ทางการแพทย์<br /><br />
		By Luminance Cocktail<br />
		โดยแพทย์หญิงวศินี แพทยศาสตร์จุฬาลงกรณ์มหาวิทยาลัยรุ่น 57<br /><br />
		ติดตามข่าวสารจากช่องทางอื่นๆได้จากเฟสบุ๊ค <a href="www.facebook.com/luminancecocktail">www.facebook.com/luminancecocktail</a><br />
		และไลน์ ไอดี (ID line)  :  luminancecocktail
		</p>
		<hr />
	</div>
</div>

<div class="row animatedParent animateOnce"  data-sequence='800' > 
	<div class="col-lg-3 col-md-4 col-sm-6 col-xs-6  animated bounceIn" data-id='1'  >
		<a href="/order">
            <div class="post-thumb">
			     <img src="/images/main-image-2.jpg" class="img-responsive " />
            </div>
			<p class="text-center">สั่งซื้อผลิตภัณฑ์</p>
		</a>
	</div>
	<div class="col-lg-3 col-md-4 col-sm-6 col-xs-6  animated bounceIn" data-id='1'>
		<a href="/products">
            <div class="post-thumb">
			 <img src="/images/all.jpg" class="img-responsive" />
            </div>
			<p class="text-center">รายละเอียดผลิตภัณฑ์</p>
		</a>
	</div>
	<div class="clearfix visible-sm visible-xs"></div>
	<div class="col-lg-3 col-md-4 col-sm-6 col-xs-6 animated bounceIn" data-id='1'>
		<a href="/ordering-and-how-to-use">
            <div class="post-thumb">
                <img src="/images/online_ordering.jpg" class="img-responsive" />
            </div>
			<p class="text-center">ลำดับและวิธีการใช้ผลิตภัณฑ์</p>
		</a>
	</div>
	<div class="col-lg-3 col-md-4 col-sm-6 col-xs-6 animated bounceIn" data-id='1'>
		<a href="/wonders-of-use">
            <div class="post-thumb">
                <img src="/images/set.jpg" class="img-responsive" />
            </div>
			<p class="text-center">มหัศจรรย์แห่งการใช้ผลิตภัณฑ์  Luminance Cocktail ครบเซ็ต</p>
		</a>
	</div>
	<div class="clearfix visible-sm visible-xs "  ></div>
	<div class="col-lg-3 col-md-4 col-sm-6 col-xs-6 animated bounceIn" data-id='2'>
		<a href="/privileges">
            <div class="post-thumb">
                <img src="/images/privieleges.jpg" class="img-responsive" />
            </div>
			<p class="text-center">สิทธิพิเศษสมาชิก</p>
		</a>
	</div>
	<div class="col-lg-3 col-md-4 col-sm-6 col-xs-6 animated bounceIn" data-id='2'>
		<a href="/aboutus">
            <div class="post-thumb">
                <img src="/images/aboutus.jpg" class="img-responsive" />
            </div>
			<p class="text-center">เกี่ยวกับเรา  </p>
		</a>
	</div>
	<div class="clearfix visible-md visible-sm visible-xs"></div>
	<div class="col-lg-3 col-md-4 col-sm-6 col-xs-6 animated bounceIn" data-id='2'>
		<a href="/contactus">
            <div class="post-thumb">
                <img src="/images/contact.jpg" class="img-responsive" />
            </div>
			<p class="text-center">ติดต่อเรา</p>
		</a>
	</div>
	<div class="col-lg-3 col-md-4 col-sm-6 col-xs-6 animated bounceIn" data-id='2'>
		<a href="/tricks-and-data-insight">
            <div class="post-thumb">
			     <img src="/images/sara.jpg" class="img-responsive" />
            </div>
			<p class="text-center">นานาสาระและข้อมูลความรู้เชิงลึก</p>
		</a>
	</div>
	<div class="clearfix visible-sm visible-xs"></div>
	<div class="col-lg-3 col-md-4 col-sm-6 col-xs-6 animated bounceIn" data-id='3'>
		<a href="/forum">
            <div class="post-thumb">
                <img src="/images/webboardLogo.png" class="img-responsive" />
            </div>
			<p class="text-center">เว็บบอร์ดแจ้งข่าว พูดคุยและสอบถาม</p>
		</a>
	</div>
	<div class="clearfix visible-md "></div>
	<div class="col-lg-3 col-md-4 col-sm-6 col-xs-6 animated bounceIn" data-id='3'>
		<a href="/auth/login">
            <div class="post-thumb">
                <img src="/images/login.jpg" class="img-responsive" />
            </div>
			<p class="text-center">เข้าสู่ระบบ</p>
		</a>
	</div>
	<div class="clearfix visible-sm visible-xs"></div>
	<div class="col-lg-3 col-md-4 col-sm-6 col-xs-6 animated bounceIn" data-id='3'>
		<a href="/auth/register">
            <div class="post-thumb">
                <img src="/images/register.jpg" class="img-responsive" />
            </div>
			<p class="text-center">สมัครสมาชิก</p>
		</a>
	</div>
	<div class="col-lg-3 col-md-4 col-sm-6 col-xs-6 animated bounceIn" data-id='3'>
		<a href="/moneytransfer">
            <div class="post-thumb">
                <img src="/images/transfer.jpg" class="img-responsive" />
            </div>
			<p class="text-center">แจ้งโอนเงิน </p>
		</a>
	</div>
</div>
<hr />
<!--
<div class="row">
	<div class="col-md-9">
		<br />
		<p class="text-center">
			ลูมิแนนซ์ ค็อกเทล (Luminance Cocktail) ได้จดทะเบียนพาณิชย์ ตามพระราชบัญญัติทะเบียนพาณิชย์<br />
อย่างถูกต้องตามกฎหมายแล้ว เมื่อวันที่ 16 มิถุนายน 2557 <br />
ทะเบียนเลขที่ 1101400472480 คำขอที่ 1000757000100

		</p>
	</div>
	<div class="col-md-3">
		<img src="/images/DBD-Verified.jpg" class="img-responsive" />
	</div>
</div>
<hr />
-->
<div class="row">
	<div class="col-md-9">
		<br />
		<p class="text-center">
			และทางทีมงานลูมิแนนซ์ ค็อกเทล (Luminance Cocktail) ได้ทำการจดทะเบียนพาณิชย์อิเล็กทรอนิคส์<br />
ของกรมพัฒนาธุรกิจการค้า อย่างถูกต้องตามกฎหมาย ด้วยความซื่อสัตย์สุจริตทางการค้า พร้อมและยินดีให้ตรวจสอบ


		</p>
	</div>
	<div class="col-md-3">
		<img src="/images/TrackTrace.jpg" class="img-responsive" />
	</div>
</div>
<hr />
<div class="row">
	<div class="col-md-12 text-center">
		<p>ขอสงวนลิขสิทธิ์ ไม่อนุญาตให้มีการคัดลอกบทความก่อนได้รับอนุญาต<br />
		Copyright 2015 All Rights Reserved
		</p>
	</div>
</div>
<hr />
<div class="row">
	<div class="col-md-12">
		<h4 class="text-center">เว็บไซต์สำหรับสืบค้นข้อมูลทางการแพทย์</h4>
	</div>
</div>
<div class="row">
	<div class="col-md-3 text-center">
		<a href="http://www.ncbi.nlm.nih.gov/pubmed" target="_blank"><img src="/images/pubmedimage1.jpg" class="img-responsive img-rounded" /></a>
	</div>
	<div class="col-md-3 text-center">
		<a href="http://www.cochrane.org/" target="_blank"><img src="/images/cochrane.jpg" class="img-responsive img-rounded" /></a>
	</div>
	<div class="col-md-3 text-center">
		<a href="http://journals.bmj.com/" target="_blank"><img src="/images/BMJ.jpg" class="img-responsive img-rounded" /></a>
	</div>
	<div class="col-md-3 ">
		<a href="http://www.uptodate.com/home" target="_blank"><img src="/images/uptodate.png" class="img-responsive img-rounded" /></a>
	</div>
</div>	
<hr />
<div class="row" id="hightlight">
	<div class="col-md-12">
    {!! $highlights->body !!}
  </div>
</div>
@stop

@section('headerjs')
    <link href="{{ asset('/css/nivo-slider.css') }}" rel="stylesheet">
    <link href="{{ asset('/css/animations.css') }}" rel="stylesheet">
@stop

@section('footerjs')
    <script type="text/javascript" src="/js/jquery.nivo.slider.pack.js"></script> 
    <script type="text/javascript" src="/js/page/welcome.js"></script> 
    <script type="text/javascript" src="/js/css3-animate-it.js"></script> 
@stop