<div id="main-menu">
	<div class="container">
		<div class="row">
			<div class="col-md-12"> 
				<ul class="list-inline">
					<li @if (Request::is('order*')) class="active" @endif><a href="/order">สั่งซื้อผลิตภัณฑ์</a></li>
					<!--<li><a href="/products">รายละเอียดผลิตภัณฑ์</a></li>
					<li><a href="/ordering-and-how-to-use">ลำดับและวิธีการใช้ผลิตภัณฑ์</a></li>
					<li><a href="/wonders-of-use">มหัศจรรย์แห่งการใช้ผลิตภัณฑ์  Luminance Cocktail ครบเซ็ต</a></li>-->
					<li @if (Request::is('privileges*')) class="active" @endif><a href="/privileges">สิทธิพิเศษสมาชิก</a></li>
					<li @if (Request::is('tricks*'))  class="active" @endif><a href="/tricks-and-data-insight">นานาสาระและข้อมูลความรู้เชิงลึก</a></li> 
					<li @if (Request::is('moneytransfer*'))  class="active" @endif><a href="/moneytransfer">แจ้งโอนเงิน</a></li>
          <li @if (Request::is('forum*') || Request::is('forum')) class="active" @endif ><a href="/forum" >	เว็บบอร์ดแจ้งข่าว พูดคุยและสอบถาม</a></li>
				</ul>
			</div>
		</div>
	</div>
</div>