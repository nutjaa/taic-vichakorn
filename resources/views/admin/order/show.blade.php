@extends('layouts.admin')
@section('content')
<div class="breadcrumbs">
    <ol class="breadcrumb">
        <li><a href="/"><i class="fa fa-home"></i>Home</a></li>
        <li><a href="/admin">Admin</a></li>
        <li><a href="/orders">Orders</a></li>
        <li class="active">Order - #{{ $order->orderNumber }}</li>
    </ol>
</div>

<div class="innerLR">
    <div class="row manage-header">
        <div class="col-lg-12">
        	<h2>หมายเลข #{{$order->orderNumber}} - {{$order->status->label}}</h2>
        </div>
 	</div>
    <div class="widget">
        {!! Notification::showAll() !!}
        {!! Form::model($order, array('url' => 'orders/'. $order->id , 'method' => 'PUT'  , 'class'=>'form-horizontal' , 'role' => 'form' )) !!}
        	 <div class="row">
                <div class="col-md-8">
                    <h4>รายละเอียด</h4>
                    <table class="table table-striped">
                        <tr>
                            <td class="col-md-4">สถานะ</td>
                            <td class="col-md-8"><span class="label {{$order->labelStatus}}">{{$order->status->label}}</span></td>
                        </tr> 
                        <tr>
                            <td>ชื่อ</td>
                            <td>{{$order->fullName}} ({{$order->email}}) Tel.({{$order->phone}})</td>
                        </tr>
                        <tr>
                            <td>ที่อยู่</td>
                            <td>{!! nl2br($order->address) !!}</td>
                        </tr>
                    </table>
                </div>
                <div class="col-md-4">
                    <div class="panel panel-success">
                        <div class="panel-heading">
                            Admin   
                        </div>
                        <div class="panel-body">
                            @if( $order->canConfirm ) 
                            <button type="button" class="btn btn-primary form-control" data-toggle="modal" data-target="#confirmModal">ยืนยันการชำระเงิน</button>
                            @elseif( $order->canShipping ) 
                            <button type="button" class="btn btn-success form-control" data-toggle="modal" data-target="#shippingModal">ทำการจัดส่งสินค้า</button>
                            @elseif( $order->isShipping  )
                            <strong>จัดส่งสินค้าเรียบร้อย</strong> : {{$order->ship_no}}<br />
                            <!--<button type="button" class="btn btn-success form-control ems-checking" data-toggle="modal" data-target="#emsModal" data-id="{{$order->ship_no}}">ตรวจสอบ EMS</button>-->
                            <a  class="btn btn-success form-control ems-checking" href="http://emsbot.com/#/?s={{$order->ship_no}}" target="_blank">ตรวจสอบ EMS</a><br /><br />
                            <button type="button" class="btn btn-warning form-control" data-toggle="modal" data-target="#shippingModal">แก้ไขหมายเลข EMS</button>
                            @endif
                            @if( $order->canCanceled ) 
                            <hr /> 
                            <button type="button" class="btn btn-danger form-control" data-toggle="modal" data-target="#canceledModal">ยกเลิกรายการ</button>
                            
                            @endif
                        </div>
                    </div>
                </div>
             </div> 
             <div class="row">
                <div class="col-md-12">
                    <h4>รายละเอียดรายการสั่งซื้อ</h4>
                    <table class="table table-striped">
                        <thead>
                            <tr>
                                <th></th>
                                <th>รหัส</th>
                                <th>ชื่อผลิตภัณฑ์</th>
                                <th class="text-right">ราคา</th>
                                <th class="text-right">จำนวน</th>
                                <th class="text-right">รวมเป็นเงิน</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($order->items as $item)
                            <tr>
                                <td>{{$item->product->sku}}</td>
                                <td><img src="{{$item->product->imageDefault('thumb')}}" /></td>
                                <td>{!!$item->product->name!!}</td>
                                <td class="text-right">{{$item->price}}</td>
                                <td class="text-right">{{$item->quantity}}</td>
                                <td class="text-right">{{$item->total}}</td>
                            </tr>
                            @endforeach
                            <tr>
                                <td colspan="5" class="text-right"><strong>รวม</strong></td>
                                <td class="text-right">{{$order->total_price}}</td>
                            </tr>
                            @if($order->discount > 0 )
                            <tr>
                                <td colspan="5" class="text-right"><strong>ส่วนลด VIP 10%</strong></td>
                                <td class="text-right">{{$order->discount}}</td>
                            </tr>
                            @endif
                            <tr>
                                <td colspan="5" class="text-right"><strong>ค่าจัดส่ง</strong></td>
                                <td class="text-right">{{$order->shipping_price}}</td>
                            </tr>
                            <tr>
                                <td colspan="5" class="text-right"><strong>รวมทั้งสิ้น</strong></td>
                                <td class="text-right">{{$order->grandTotal}}</td>
                            </tr>
                        </tbody>
                    </table>
                </div>
             </div>
        {!! Form::close() !!}
    </div>
    <div class="row">
        <div class="col-md-6 ">
            <div class="row widget">
                <h4>ประวัติ</h4>
                <table class="table table-striped">
                    <thead>
                        <tr>
                            <th>สถานะ</th>
                            <th>วันที่</th>
                            <th>โดย</th>
                            <th>i</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($order->statuses as $status)
                        <tr>
                            <td>{{$status->status->label}}</td>
                            <td>{{$status->createdAtFormat}}</td>
                            <td>{{$status->user->username}}</td>
                            <td>
                            @if($status->hasDescription)
                            <button type="button" class="btn btn-default" data-toggle="tooltip" data-placement="top" title="{!! nl2br($status->description) !!}">i</button>
                            @endif
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div> 
        </div>
        <div class="col-md-6">
             <div class="row widget">
                <div class="col-md-12">
                <h4>ยืนยันการโอนเงิน</h4>
                @foreach($order->confirmtransfers as $confirm)
                    <strong>วันที่&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</strong> : {{$confirm->transferedAtFormat}}<br />
                    <strong>จำนวนเงิน&nbsp;&nbsp;</strong> : {{$confirm->amount}}<br />
                    <strong>โอนโดย&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</strong> : {{$confirm->method}}<br />
                    <strong>จากธนาคาร</strong> : {{$confirm->frombank}}<br />
                    <strong>เข้าบัญชีธนาคาร</strong> : {{$confirm->tobank}}<br />
                    <strong>หมายเหตุ&nbsp;&nbsp;&nbsp;&nbsp;</strong> : {!!nl2br($confirm->remark)!!}<br />
                    <hr />
                @endforeach
                </div>
            </div>
        </div>
    </div> 
</div>

<div class="modal fade" id="canceledModal" tabindex="-1" role="dialog" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      {!! Form::model($order, array('url' => 'orders/'. $order->id . '/changestatus' , 'method' => 'PUT'  , 'class'=>'form-horizontal' , 'role' => 'form' )) !!}
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" >ยกเลิกการจอง</h4>
      </div>
      <div class="modal-body">
        <span>รายละเอียด</span>
        <input name="status" type="hidden" value="canceled" />
        <textarea class="form-control" name="description"></textarea>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <button type="submit" class="btn btn-primary">ยืนยันการยกเลิก</button>
      </div>
      {!! Form::close() !!}
    </div>
  </div>
</div>

<div class="modal fade" id="confirmModal" tabindex="-1" role="dialog" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      {!! Form::model($order, array('url' => 'orders/'. $order->id . '/changestatus' , 'method' => 'PUT'  , 'class'=>'form-horizontal' , 'role' => 'form' )) !!}
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" >ยืนยันการชำระเงิน</h4>
      </div>
      <div class="modal-body">
        <span>รายละเอียด</span>
        <input name="status" type="hidden" value="confirm" />
        <textarea class="form-control" name="description"></textarea>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <button type="submit" class="btn btn-primary">ยืนยันการชำระเงิน</button>
      </div>
      {!! Form::close() !!}
    </div>
  </div>
</div>

<div class="modal fade" id="shippingModal" tabindex="-1" role="dialog" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      {!! Form::model($order, array('url' => 'orders/'. $order->id . '/changestatus' , 'method' => 'PUT'  , 'class'=>'form-horizontal' , 'role' => 'form' )) !!}
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" >ยืนยันการจัดส่ง</h4>
      </div>
      <div class="modal-body">
        <span>หมายเลย EMS</span>
        <input name="ship_no" type="text" class="form-control"  required="" value="{{$order->ship_no}}" />
        <span>รายละเอียด</span>
        <input name="status" type="hidden" value="shipping" />
        <textarea class="form-control" name="description"></textarea>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <button type="submit" class="btn btn-primary">ยืนยันการจัดส่ง</button>
      </div>
      {!! Form::close() !!}
    </div>
  </div>
</div>

@include('blocks.emsmodal')

@stop

@section('footerjs') 
    <script type="text/javascript" src="/js/page/admin/order.show.js"></script>
    <script type="text/javascript" src="/js/emsModal.js"></script>
@stop