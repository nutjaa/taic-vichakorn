@extends('layouts.master')

@section('content')
<div class="row">
	<ol class="breadcrumb">
	  <li><a href="/">Home</a></li> 
	  <li class="active">ติดต่อเรา</li>
	</ol>
</div>
<div class="row"> 
	<div class="col-md-9">
		{!! $page->body !!} 
	</div>
	<div class="col-md-3">
		<br /><br />
		<div class="fb-page" data-href="https://www.facebook.com/luminancecocktail" data-width="100%" data-hide-cover="false" data-show-facepile="true" data-show-posts="false"><div class="fb-xfbml-parse-ignore"><blockquote cite="https://www.facebook.com/luminancecocktail"><a href="https://www.facebook.com/luminancecocktail">Luminance Cocktail</a></blockquote></div></div>
		<div class="row">
			<div class="col-lg-12">
				<h4>ติดต่อเรา</h4>
				{!! Notification::showAll() !!}
				@if (count($errors) > 0)
					<div class="alert alert-danger">
						<strong>ขออภัย</strong> เกิดปัญหาบางอย่างสำหรับข้อมูลที่ส่งมา<br><br>
						<ul>
							@foreach ($errors->all() as $error)
								<li>{{ $error }}</li>
							@endforeach
						</ul>
					</div>
				@endif
				{!! Form::open(array('url' => 'contactus' , 'role' => 'form' )) !!}
					<input type="hidden" name="_token" value="{{ csrf_token() }}">
					<div class="form-group">
					    <label for="email">Email address</label>
						<input type="email" class="form-control" id="email" name="email" placeholder="Email address" value="{{ old('email') }}"/>
					</div>
					<div class="form-group">
					    <label for="description">รายละเอียด</label>
					    <textarea class="form-control" id="description" name="description" placeholder="Decription" rows="8">{{ old('description') }}</textarea>
					</div> 
					<div class="form-group">
						{!! captcha_img() !!}
						<input type="text" class="form-control" id="captcha" name="captcha" placeholder="Captcha" value=""/>
					</div>
					<button type="submit" class="btn btn-primary">ส่งข้อความ</button>
					
				{!! Form::close() !!}
				<br /> 
			</div>
		</div>
		
	</div>
</div>
@stop