<?php namespace App;

use LaravelBook\Ardent\Ardent;
use Carbon ; 
use Auth;

class OrderStatus extends Ardent{
    protected $table = 'order_statuses'; 
    
    public static $relationsData = array(  
        'order' => array(self::BELONGS_TO, 'App\Order', 'foreignKey' => 'order_id') ,  
        'status' => array(self::BELONGS_TO, 'App\Termdata', 'foreignKey' => 'status_id') , 
        'user' => array(self::BELONGS_TO,'App\User', 'foreignKey' => 'user_id')
    ) ; 
    
    /*--- Hook ---*/
    public function beforeSave(){
      if(!Auth::guest())
        $this->user_id = Auth::user()->id ; 
    }
    
    /*--- Atribute ---*/
    
    public function getCreatedAtFormatAttribute(){ 
		$date = Carbon::createFromFormat('Y-m-d H:i:s', $this->created_at );
		$date->setTimezone('Asia/Bangkok');
		return $date->format('d/m/Y H:i');
    }
    
    public function getHasDescriptionAttribute(){
        if(trim($this->description) == '')
            return false ; 
        return true ; 
    }
}