@extends('layouts.admin')
@section('content') 
<div class="breadcrumbs">
    <ol class="breadcrumb">
        <li><a href="/"><i class="fa fa-home"></i>Home</a></li>
        <li><a href="/admin">Admin</a></li>
        <li><a href="/pages">Pages</a></li>
        <li class="active">Create new page</li>
    </ol>
</div>

<div class="innerLR">
    <div class="row manage-header">
        <div class="col-lg-7">
        	<h2>Create new page</h2>
        </div>
 	</div>
 	
 	<div class="widget">
        {!! Notification::showAll() !!}
 	
     	{!! Form::open(array('url' => 'pages' , 'class'=>'form-horizontal' , 'role' => 'form' )) !!}
            @include('admin/page/form')  
     	{!! Form::close() !!}
 	</div>
</div>

@stop

@section('footerjs') 
	{!! HTML::script('js/ckeditor/ckeditor.js'); !!}  
@stop