@extends('layouts.adminlogin')
@section('content') 
<div class="wrapper">
    <div class="container">
        <div class="row">
            <div class="col-md-4"></div>
            <div class="col-md-4">
                <div class="panel login-panel"> 
    				<div class="panel-body">
    					<h2>Please sign in</h2>  
    					{!! Notification::showAll() !!}
    					<form role="form" action="{{ url('/auth/login') }}" method="POST">
    						<input type="hidden" name="_token" value="{{ csrf_token() }}">
    						<div class="form-group">
    						    <label for="email">Email address</label>
    							<input type="email" class="form-control" id="email" name="email" placeholder="Email address" value="{{ old('email') }}"/>
    						</div>
    						<div class="form-group">
    						    <label for="passowrd1">Password</label>
    						    <input type="password" class="form-control" id="passowrd1" name="password" placeholder="Password" />
    						</div>
    						<div class="checkbox">
    						    <label>
    								<input type="checkbox" name="remember" /> Remember me
    						    </label>
    						</div>
    						<button type="submit" class="btn btn-primary">Sign in</button>
    					</form>
    				</div>
    			</div>
            </div>
            <div class="col-md-4"></div>
        </div>
    </div>
</div>
@stop

@section('footerjs')  
@stop

@section('headercss')   
@stop