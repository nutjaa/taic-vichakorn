<?php namespace App\Http\Controllers\Auth;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Contracts\Auth\Guard;
use Illuminate\Contracts\Auth\Registrar;
use Illuminate\Foundation\Auth\AuthenticatesAndRegistersUsers;

use App\User;
use App\Termdata;
use Hash;
use Session;
use Mail;

use Krucas\Notification\Facades\Notification;

use SEOMeta;
use OpenGraph;

class AuthController extends Controller {

	/*
	|--------------------------------------------------------------------------
	| Registration & Login Controller
	|--------------------------------------------------------------------------
	|
	| This controller handles the registration of new users, as well as the
	| authentication of existing users. By default, this controller uses
	| a simple trait to add these behaviors. Why don't you explore it?
	|
	*/

	use AuthenticatesAndRegistersUsers;

	/**
	 * Create a new authentication controller instance.
	 *
	 * @param  \Illuminate\Contracts\Auth\Guard  $auth
	 * @param  \Illuminate\Contracts\Auth\Registrar  $registrar
	 * @return void
	 */
	public function __construct(Guard $auth, Registrar $registrar)
	{
		$this->auth = $auth;
		$this->registrar = $registrar;

		$this->middleware('guest', ['except' => ['getLogout','getConfirm']]);
	} 
    
  public function getLogin(Request $request)
	{ 
    SEOMeta::setTitle('เข้าสู่ระบบ');
    SEOMeta::setDescription('เข้าสู่ระบบ Luminance Cocktail');
    
    OpenGraph::setDescription('เข้าสู่ระบบ Luminance Cocktail');
    OpenGraph::setTitle('เข้าสู่ระบบ');
    OpenGraph::setUrl(url('/auth/login')); 
    
    $redirectTo = $request->input('redirectTo') ; 
    if(trim($redirectTo) != '')
      Session::put('redirectTo',$redirectTo);
		return view('auth.login');
	}
	
	public function postLogin(Request $request)
	{
		$this->validate($request, [
			'email' => 'required|email', 'password' => 'required',
		]);

		$credentials = $request->only('email', 'password');

		if ($this->auth->attempt($credentials, $request->has('remember')))
		{
			if($this->auth->user()->status == 0){
				Notification::error('กรุณาเช็คอีเมล์เพื่อทำการยืนยันความถูกต้องของอีเมล์อีกครั้งค่ะ (ต้องยืนยันการสมัครทางอีเมล์ก่อน ถึงจะล๊อกอินเข้าสู่ระบบได้)'); 
				$this->auth->logout();
				return redirect($this->loginPath())
					->withInput($request->only('email', 'remember'));
				
			}
			return redirect()->intended($this->redirectPath());
		}
		
		Notification::error('อีเมล์ หรือ รหัสผ่านไม่ถูกค้อง'); 

		return redirect($this->loginPath())
					->withInput($request->only('email', 'remember'))
					->withErrors([
						'email' => $this->getFailedLoginMessage(),
					]);
	}
	
	public function getRegister()
	{
    SEOMeta::setTitle('สมัครสมาชิก');
    SEOMeta::setDescription('สมัครสมาชิก Luminance Cocktail');
    
    OpenGraph::setDescription('สมัครสมาชิก Luminance Cocktail');
    OpenGraph::setTitle('สมัครสมาชิก');
    OpenGraph::setUrl(url('/auth/register')); 
    
		$day_list = [] ; 
		for($i = 1 ; $i<32 ; $i++){
			$day_list[$i] =str_pad($i,2,"0", STR_PAD_LEFT) ; 
		}
		$month_list = [] ; 
		for($i = 1 ; $i<13 ; $i++){
			$month_list[$i] =str_pad($i,2,"0", STR_PAD_LEFT) ; 
		}
		$year_list = [] ; 
		$year = date('Y') ; 
		for($i = 0 ; $i < 50 ; $i ++ ){
			$year_list[$year-$i] = $year - $i ; 
 		}
		return view('auth.register')->with('day_list',$day_list)->with('month_list',$month_list)->with('year_list',$year_list);
	}
	
	public function postRegister(Request $request)
	{ 
 		$user = new User() ; 
 		$user->name = $request->input('name') ; 
 		$user->email = $request->input('email') ; 
 		$user->password = Hash::make($request->input('password')) ;  
 		$user->phone = $request->input('phone') ; 
 		$user->gender = $request->input('gender') ; 
 		$user->lastname = $request->input('lastname') ; 
 		$user->username = $request->input('username') ; 
 		$user->address = $request->input('address') ; 
 		$user->user_role_id = Termdata::name('user')->first()->id;
 		$user->status = 0 ; 
 		$user->lineid = $request->input('lineid') ; 
 		$user->facebookname = $request->input('facebookname') ; 
 		$user->is_subscribe = $request->input('is_subscribe') ; 
 		$user->birthdate = $request->input('year') . '-' . $request->input('month')  . '-' . $request->input('day')  ;
		$user->token =  hash_hmac('sha256', str_random(40),'TAIC') ; 
		
		if ($user->save()  ) {
			# send confirmation email ; 
			//$this->auth->login($user);
			//return redirect($this->redirectPath());
			
			Notification::success('คุณได้ลงทะเบียนเรียบร้อยแล้ว'); 
			Mail::send('emails.confirmlogin', ['user' => $user ], function($message)use ($user)
			{ 
			    $message->to($user->email,$user->fullname)->subject('สมัครเปิดบัญชี luminancecocktail.com');
			});
			return redirect('auth/successsignup');
		}else{
			Notification::error( $user->errors()->All()); 
			return redirect('auth/register')->withInput() ; 
		}
 		 
	}
    
    public function redirectPath()
	{
        if (Session::has('redirectTo'))
        {
            return Session::pull('redirectTo');
        }   
       
		if (property_exists($this, 'redirectPath'))
		{
			return $this->redirectPath;
		}

		return property_exists($this, 'redirectTo') ? $this->redirectTo : '/home';
	}
	
	public function getConfirm($token = null){
	   /*
		Mail::send('emails.confirmlogin', ['user' => $this->auth->user()], function($message)
		{ 
		    $message->to('tworkstudio@gmail.com', 'Nut Chanyong')->subject('สมัครเปิดบัญชี luminancecocktail.com');
		});
        */  
        $user = User::token($token)->first() ;  
        if(!is_null($user)){
        	$user->token = '' ; 
        	$user->status = 1 ; 
        	$user->updateUniques() ; 
        }
        
		return view ('auth.confirm')->with('user' ,$user);
	}
	
	public function getSuccesssignup(){ 
		return view('auth.success_signup') ; 
	}

}
