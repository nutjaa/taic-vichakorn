@if ($post->canEdit)
	<a href="{{ $post->editRoute }}" class="edit">{{ trans('forum::base.edit')}}</a>
@endif
@if (!Auth::guest() && $post->canDelete && Auth::user()->isAdmin() )
	<a href="{{ $post->deleteRoute }}" class="delete" data-confirm data-method="delete">{{ trans('forum::base.delete') }}</a>
@endif   

<div id="post-{{ $post->id }}">
  <div class="row">
    <div class="col-md-1">
      <a href="#post-{{ $post->id }}"><img  class="img-circle" width="50" title="โพสต์" alt="โพสต์" src="/images/{{ $post->author->avatarpath() }}"></a>
    </div>
    <div class="col-md-11">
      <strong>{!! $post->author->username !!}</strong> » {{ trans('forum/base.posted_at') }} {{ $post->posted }}
      @if ($post->updated_at != null && $post->created_at != $post->updated_at)
  			» {{ trans('forum/base.last_update') }} {{ $post->updated }}
  		@endif
      <br /> 
      {!! $post->content !!}
    </div>
  </div>  
</div>