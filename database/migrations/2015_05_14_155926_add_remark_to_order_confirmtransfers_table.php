<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddRemarkToOrderConfirmtransfersTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('order_confirmtransfers', function(Blueprint $table)
		{
			$table->text('remark') ; 
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('order_confirmtransfers', function(Blueprint $table)
		{
			$table->dropColumn('remark') ; 
		});
	}

}
